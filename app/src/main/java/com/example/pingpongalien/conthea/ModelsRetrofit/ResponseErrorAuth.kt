package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

data class ResponseErrorAuth(
        @SerializedName("token")
        val campos: ResponseErrorAuthOrder? = null
)
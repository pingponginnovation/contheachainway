package com.example.pingpongalien.conthea.Activities.Login.Login.Core

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.TextView
import com.example.pingpongalien.conthea.Activities.Core.Core
import com.example.pingpongalien.conthea.Activities.Core.CorePresenter
import com.example.pingpongalien.conthea.Activities.Core.HomeScreenView
import com.example.pingpongalien.conthea.Activities.Core.TypeBusquedasView
import com.example.pingpongalien.conthea.Activities.DetailOrder.DetailOrderView
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyLocalView
import com.example.pingpongalien.conthea.Activities.Login.Login.LoginView
import com.example.pingpongalien.conthea.Fragments.DetailTag.DetailTagView
import com.example.pingpongalien.conthea.Fragments.InitialAdjustment.AltaTagView
import com.example.pingpongalien.conthea.Fragments.InitialAdjustment.InitialAdjustmentView
import com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta.SearchByEtiquetaView
import com.example.pingpongalien.conthea.Fragments.SearchByUbication.SearchByUbicationView
import com.example.pingpongalien.conthea.Fragments.SearchCatalogue.SearchCatalogueView
import com.example.pingpongalien.conthea.Fragments.SearchOrder.SearchOrderView
import com.example.pingpongalien.conthea.Fragments.SetPlace.SetPlaceView
import com.example.pingpongalien.conthea.Fragments.Settings.SettingsView
import com.example.pingpongalien.conthea.Fragments.StartInventory.StartInventoryView
import com.example.pingpongalien.conthea.Fragments.Unsuscribe.UnsuscribeView
import com.example.pingpongalien.conthea.Fragments.VerifyInventory.VerifyInventoryView
import com.example.pingpongalien.conthea.Models.ProductChild
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderItem
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import com.example.pingpongalien.conthea.VersionApp
import kotlinx.android.synthetic.main.activity_core.*
import kotlinx.android.synthetic.main.app_bar_menu.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import java.util.*

class CoreView : AppCompatActivity(), Core.View,NavigationView.OnNavigationItemSelectedListener {

    val TAG = "CoreView"

    private var presenter: Core.Presenter? = null
    private var preferences: PreferencesController? = null
    private var controlFragment = 0
    private var fragment: Fragment? = null
    private var responseOrderItem: ResponseOrderItem? = null
    private var typeSearch: Boolean? = null

    companion object{
        var core = CoreView()
        var nameFragment = ""
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_core)

        configurateNagiationView()
        setToolbar()
        setGestureNavigationDrawer()
        preferences = PreferencesController(applicationContext)
        presenter = CorePresenter(this, preferences!!)
        presenter!!.getUser()
        SetMenu()
        CheckNotification()

        button_home.setOnClickListener{
            setFirstFragment()
        }
    }

    fun CheckNotification(){
        val fromNotification = intent.getBooleanExtra("NOTIFICATION", false)
        if(fromNotification){
            fragment = SearchOrderView(true, this)
            changeTitleToolbar(getString(R.string.buscar_orden_salida))
            controlFragment = 3
            val manager = supportFragmentManager
            manager.beginTransaction().replace(R.id.main_content, fragment).commit()
        }
        else setFirstFragment()
    }

    fun SetMenu(){

        val menuArray = arrayListOf("Asignar lugar", "Buscar en catálogo", "Buscar orden entrada",
        "Buscar orden salida", "Dar de alta", "Dar de baja", "Detalle de etiqueta", "Inventario cíclico")
        val menuIcons = arrayListOf(R.drawable.item_place, R.drawable.item_search_catalog,
        R.drawable.item_search_order, R.drawable.item_search_order, R.drawable.item_cancel_product,
        R.drawable.item_cancel_product, R.drawable.item_place, R.drawable.item_inventory)

        val menu: Menu = navigation_view.menu
        //menu.add(0, 8, Menu.FIRST, "Configuración")
        //menu.getItem(0).setIcon(R.drawable.item_settings)
        menu.add(0, 10, Menu.FIRST, "Versión")
        menu.getItem(0).setIcon(R.drawable.item_settings)
        /*menu.add(0, 11, Menu.FIRST, "Enviar tags")
        menu.getItem(1).setIcon(R.drawable.item_settings)*/
        menu.add(0, 9, Menu.FIRST, "Salir")
        menu.getItem(1).setIcon(R.drawable.item_exit)

        val permisosHashSet = preferences!!.GetRoles()
        val subMenu = menu.addSubMenu("CONTHEA")

        var activate = false
        var aux = 0
        menuArray.forEachIndexed{i, it->

            activate = false

            if(i == 2 && permisosHashSet!!.contains("ver_entrada_almacen")){
                subMenu.add(0, i, Menu.FIRST, it)
                subMenu.getItem(aux).setIcon(menuIcons[i])
                aux++
                activate = true
            }
            else if(i == 3 && permisosHashSet!!.contains("ver_salida_almacen")){
                subMenu.add(0, i, Menu.FIRST, it)
                subMenu.getItem(aux).setIcon(menuIcons[i])
                aux++
                activate = true
            }
            else if(i == 5 && permisosHashSet!!.contains("eliminar_tags")){
                subMenu.add(0, i, Menu.FIRST, it)
                subMenu.getItem(aux).setIcon(menuIcons[i])
                aux++
                activate = true
            }
            else{

                if( (i == 2 && activate) || (i == 3 && activate) || (i == 5 && activate) ){
                    subMenu.add(0, i, Menu.FIRST, it)
                    subMenu.getItem(aux).setIcon(menuIcons[i])
                    aux++
                }
                else{
                    if(i != 2 && i != 3 && i != 5){
                        subMenu.add(0, i, Menu.FIRST, it)
                        subMenu.getItem(aux).setIcon(menuIcons[i])
                        aux++
                    }
                }
            }
        }

        /*if(permisosHashSet!!.size == 1){
            if(permisosHashSet.contains("Salidas")){

                var aux = 0
                menuArray.forEachIndexed{i, item->
                    if(i != 2){
                        subMenu.add(0, i, Menu.FIRST, item)
                        subMenu.getItem(aux).setIcon(menuIcons[i])
                        aux++
                    }
                }
            }
            else if(permisosHashSet.contains("Entradas")){
                var aux = 0
                menuArray.forEachIndexed{i, item->
                    if(i != 3){
                        subMenu.add(0, i, Menu.FIRST, item)
                        subMenu.getItem(aux).setIcon(menuIcons[i])
                        aux++
                    }
                }
            }
            else{
                menuArray.forEachIndexed{i, item->
                    subMenu.add(0, i, Menu.FIRST, item)
                    subMenu.getItem(i).setIcon(menuIcons[i])
                }
            }
        }
        else{
                //roles: Salidas y Entradas
            var aux = 0
            menuArray.forEachIndexed{i, item->
                if(i != 3 && i != 2){
                    subMenu.add(0, i, Menu.FIRST, item)
                    subMenu.getItem(aux).setIcon(menuIcons[i])
                    aux++
                }
            }
        }*/
    }

    fun setFirstFragment(){
        txt_title_top.visibility = View.GONE
        button_home.visibility = View.GONE
        img_top.visibility = View.VISIBLE

        controlFragment = 3
        fragment = HomeScreenView(this)
        CreateTransaction()
    }

    fun CreateTransaction(){
        val manager = supportFragmentManager
        manager.beginTransaction().replace(R.id.main_content, fragment).commit()
    }

    override fun setNameUser(name: String, lastName: String) {
        val headerLayout = navigation_view.getHeaderView(0)
        val txt_name = headerLayout.findViewById(R.id.txt_name) as TextView
        txt_name.text = "$name $lastName"
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment: Fragment? = null
        val aux = 0
        when(item.itemId){
            1 -> {
                fragment = SearchCatalogueView(this, "", "", "")
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.buscar_catalogo))
                controlFragment = 0
            }
            3 -> {
                fragment = SearchOrderView(true, this)
                typeSearch = true
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.buscar_orden_salida))
                controlFragment = 3
                removeMenuDetailRead()
            }
            5 -> {
                fragment = UnsuscribeView()
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.dar_baja))
                controlFragment = 2
            }
            4 -> {
                fragment = AltaTagView()
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.dar_alta))
                controlFragment = 10
            }
            7 -> {
                fragment = VerifyInventoryView(this)
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.Inventario_ciclico))
                controlFragment = 3
                removeMenuDetailRead()
            }
            0 -> {
                fragment = SetPlaceView(this)
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.asignar_lugar))
                controlFragment = 4
            }
            8 -> {
                fragment = SettingsView.createSettingsView(shouldClose = false, isFirstTime = false)
                this.fragment = fragment
                changeTitleToolbar("Configuración")
                controlFragment = 5
                removeMenuDetailRead()
            }
            6 -> {
                fragment = DetailTagView(this)
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.detalle_etiqueta))
                controlFragment = 6
            }
            2 -> {
                fragment = SearchOrderView(false, this)
                typeSearch = false
                this.fragment = fragment
                changeTitleToolbar(getString(R.string.buscar_orden_entrada))
                controlFragment = 3
                removeMenuDetailRead()
            }
            10 -> {
                fragment = VersionApp()
                this.fragment = fragment
                changeTitleToolbar("Información")
                controlFragment = 7
                removeMenuDetailRead()
            }
        }

        if(fragment != null){
            val manager = supportFragmentManager
            manager.beginTransaction().replace(R.id.main_content, fragment).commit()
        }
        else if(aux == 0) showAlertCloseSession()

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun asignarLugar(){
        fragment = SetPlaceView(this)
        controlFragment = 4
        CreateTransaction()
    }

    fun ordenesEntradaSalida(typeSearch: Boolean){
            //True -> Salidas,  False -> Entradas
        this.typeSearch = typeSearch
        fragment = SearchOrderView(typeSearch, this)
        controlFragment = 3
        CreateTransaction()
    }

    fun detailOrder(order: ResponseOrderItem, typeSearch: Boolean){
        val frag = DetailOrderView(this)
        responseOrderItem = order
        this.typeSearch = typeSearch
        frag.setDatos(order, typeSearch)
        fragment = frag
        controlFragment = 8
        CreateTransaction()
    }

    fun inventarioCiclico(){
        fragment = VerifyInventoryView(this)
        controlFragment = 3
        CreateTransaction()
    }

    fun busquedasView(){
        fragment = TypeBusquedasView(this)
        controlFragment = 3
        CreateTransaction()
    }

    fun buscarEnCatalogo(catalogo: String, nombre: String, lote: String){
        fragment = SearchCatalogueView(this, catalogo, nombre, lote)
        controlFragment = 0
        CreateTransaction()
    }

    fun buscarPorUbicacion(){
        fragment = SearchByUbicationView(this)
        controlFragment = 3
        CreateTransaction()
    }

    fun contarProductos(child: ProductChild, catalogo: String, nombre: String, lote: String){
        val frag = DetailVerifyLocalView.newInstance()
        frag.setChild(child, this, catalogo, nombre, lote)
        fragment = frag
        controlFragment = 1
        CreateTransaction()
    }

    fun buscarEtiquetas(){
        fragment = SearchByEtiquetaView(this)
        controlFragment = 5
        CreateTransaction()
    }

    override fun showLoginView() {
        startActivity<LoginView>()
        finish()
    }

    fun closeSession(){
        preferences!!.saveSession(false)
        preferences!!.closeSession()
        presenter!!.showLoginView()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean{
        when(controlFragment){
            0 -> (this.fragment as SearchCatalogueView).setKeyDown(keyCode, event!!)
            1 -> (this.fragment as DetailVerifyLocalView).setKeyDown(keyCode, event!!)
            2 -> (this.fragment as UnsuscribeView).setKeyDown(keyCode, event!!)
            4 -> (this.fragment as SetPlaceView).setKeyDown(keyCode, event!!)
            5 -> (this.fragment as SearchByEtiquetaView).setKeyDown(keyCode, event!!)
            6 -> (this.fragment as DetailTagView).setKeyDown(keyCode, event!!)
            7 -> (this.fragment as VersionApp)
            8 -> (this.fragment as DetailOrderView).setKeyDown(keyCode, event!!)
            9 -> (this.fragment as InitialAdjustmentView).setKeyDown(keyCode, event!!)
            10 -> (this.fragment as AltaTagView).setKeyDown(keyCode, event!!)
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean{
        when(controlFragment){
            0 -> (this.fragment as SearchCatalogueView).setKeyUp(keyCode, event!!)
            1 -> (this.fragment as DetailVerifyLocalView).setKeyUp(keyCode, event!!)
            2 -> (this.fragment as UnsuscribeView).setKeyUp(keyCode, event!!)
            4 -> (this.fragment as SetPlaceView).setKeyUp(keyCode, event!!)
            5 -> (this.fragment as SearchByEtiquetaView).setKeyUp(keyCode, event!!)
            6 -> (this.fragment as DetailTagView).setKeyUp(keyCode, event!!)
            7 -> (this.fragment as VersionApp)
            8 -> (this.fragment as DetailOrderView).setKeyUp(keyCode, event!!)
            9 -> (this.fragment as InitialAdjustmentView).setKeyUp(keyCode, event!!)
            10 -> (this.fragment as AltaTagView).setKeyUp(keyCode, event!!)
        }
        return super.onKeyUp(keyCode, event)
    }

    /*override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menu = menu
        return true
    }*/

    fun removeMenuDetailRead(){
        toolbar!!.menu.clear()
    }

    fun setToolbar() {
        setSupportActionBar(toolbar)
    }

    fun changeTitleToolbar(title:String){
        txt_title_top.text = title
        txt_title_top.visibility = View.VISIBLE
        button_home.visibility = View.VISIBLE
        img_top.visibility = View.GONE
    }

    fun setGestureNavigationDrawer() {
        val toogle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_abrir, R.string.navigation_drawer_cerrar)
        toogle.isDrawerIndicatorEnabled = false
        drawer_layout.addDrawerListener(toogle)
        toogle.syncState()

        button_drawer.setOnClickListener{
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) drawer_layout.closeDrawer(GravityCompat.START)
            else drawer_layout.openDrawer(GravityCompat.START)
        }
    }

    fun configurateNagiationView(){
        val transaction = supportFragmentManager.beginTransaction()
        transaction!!.replace(R.id.main_content, HomeScreenView(this))
        transaction.commit()
        navigation_view.setNavigationItemSelectedListener(this)

        val fm: FragmentManager? = supportFragmentManager
        val count = fm!!.backStackEntryCount
        //Log.e("BACKSTACK", count.toString())
    }

    private fun showAlertCloseSession(){
        alert {
            title = getString(R.string.atencion)
            message = getString(R.string.cerrar_sesion)
            positiveButton(getString(R.string.si)) { closeSession() }
            negativeButton(getString(R.string.no)){}
        }.show()
    }

    override fun onBackPressed(){
        when(nameFragment){
            "DetailOrder" -> ordenesEntradaSalida(typeSearch!!)
            else -> {
                responseOrderItem = null
                typeSearch = null
                showAlertCloseSession()
            }
        }
    }
}

package com.example.pingpongalien.conthea.Adapters.SectionedRecyclerSetPlace;

import com.example.pingpongalien.conthea.Models.ChildLocationTags;
import com.intrusoft.sectionedrecyclerview.Section;
import java.util.List;

public class ParentLocationTags implements Section<ChildLocationTags> {

    List<ChildLocationTags> childList;
    String catalogoText;
    String cantidadText;

    public ParentLocationTags(List<ChildLocationTags> childList, String catalogo, String cantidad) {
        this.childList = childList;
        this.catalogoText = catalogo;
        this.cantidadText = cantidad;
    }

    @Override
    public List<ChildLocationTags> getChildItems() {
        return childList;
    }

    public void setCantidadText(String cantidadText) {
        this.cantidadText = cantidadText;
    }

    public String getCatalogoText() {
        return catalogoText;
    }

    public String getCantidadText() {
        return cantidadText;
    }
}
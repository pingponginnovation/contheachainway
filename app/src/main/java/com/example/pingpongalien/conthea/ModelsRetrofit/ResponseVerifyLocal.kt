package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseVerifyLocal(
        @SerializedName("response")
        val response: ArrayList<String>? = null
)
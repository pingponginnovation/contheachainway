package com.example.pingpongalien.conthea.Activities.DetailOrder
import android.content.Context
import android.util.Log
import android.view.MenuItem
import com.example.pingpongalien.conthea.Activities.DetailOrder.DetailOrderView.Companion.CODIGO_REMISION
import com.example.pingpongalien.conthea.Activities.DetailOrder.DetailOrderView.Companion.MOVIMIENTO_ID
import com.example.pingpongalien.conthea.Adapters.AdapterProducts
import com.example.pingpongalien.conthea.Models.Product
import com.example.pingpongalien.conthea.Models.TagNoMatch
import com.example.pingpongalien.conthea.ModelsRetrofit.*
import com.example.pingpongalien.conthea.ModelsRoom.RoomOrdenes
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.example.pingpongalien.conthea.Room.RoomController
import com.google.gson.Gson
import org.jetbrains.anko.collections.forEachWithIndex
import org.jetbrains.anko.custom.async
import org.jetbrains.anko.doAsync
import kotlin.math.atan

/**
 * DESCRIPCIÓN DE ALGUNOS ARRAYS......
 *
 * aLotes -> Se manda a llenar desde el inicio de DetailOrderView
 * aProducts -> Se manda a llenar desde el inicio de DetailOrderView. Contiene los productos de cada orden
 * asi como la cantidad, id, caducidad,  etc.
 * aTagsForVerify -> Se llena de los tags que se leen con la handheld
 * aTagsNoMatch -> Se llena de los tags que se leen de más, en caso que hayan leido mas productos de los que
 * deberian salir o entrar, entonces con ese array mostramos que productos son los que van de más.
 * arrayAvoidedTags -> Se usa solo en salidas, para eliminar los tags que estan repetidos, esos tags repetidos
 * los metemos en este array y despues los pasamos al de aTagsNoMatch para mostrar los que estan demás.
 */
class DetailOrderModel(presenter: DetailOrderPresenter, context : Context) : DetailOrder.Model{

    val TAG = "DetailOrderModel"
    var presenter: DetailOrder.Presenter? = null
    var appContext: Context? = null
    var aLotes = ArrayList<String>()
    var aProductLote = ArrayList<Product>()
    var aProducts: ArrayList<ResponseOrderProduct> = ArrayList()
    var adapterProducts: AdapterProducts? = null
    var aTags: ArrayList<RequestRemisionProducts> = ArrayList()
    var requestRemision = RequestRemision()
    var aTagsForVerify = arrayListOf<String>()
    var aTagsNoMatch = arrayListOf<String>()
    var shouldAddNoMatch = true
    var totalRead = 0
    private var typeSearch = false
    var orderReady = false
    var validate = false
    private var movimientoType = ""
    var arrayAvoidedTags: ArrayList<ResponseVerifyTagItem> = ArrayList()

    init {
        this.presenter = presenter
        this.appContext = context
    }

    override fun searchTag(){
        //Log.e(" ENTRAA", aTagsForVerify.toString())
        if(aProductLote.size > 0) requestVerifyTags()
        else presenter!!.SetProgressBar(false)
    }

    private fun requestVerifyTags(){
        clearAll()
        val retrofit = RetrofitController(this.appContext!!)
        Log.e("Req", RequestVerifyTag(aLotes, aTagsForVerify, CODIGO_REMISION, MOVIMIENTO_ID).toString())
        retrofit.verifyTags(RequestVerifyTag(aLotes, aTagsForVerify, CODIGO_REMISION, MOVIMIENTO_ID), this, typeSearch)
    }

    override fun ErrorPeticionNetwork(){
        presenter!!.ErrorPeticionNetwork()
    }

    override fun UnsuccessVerifyTags(resp: String) {
        presenter!!.UnsuccessVerifyTags(resp)
    }

    override fun successRequestVerify(responseVerifyTag: ResponseVerifyTag){

                //Recibimos los tags que son válidos previamente leidos.

        //Log.e("Response", responseVerifyTag.toString())
        if(!responseVerifyTag.response.isNullOrEmpty()){
            responseVerifyTag.response.forEachWithIndex{ _ , prod ->
                Log.e("TAG", prod.id.toString())
                Log.e("TAG UID", prod.uuid)
            }
        }

        if(!responseVerifyTag.response.isNullOrEmpty()){

            //DeleteRepeatTags(responseVerifyTag) //pendiente talvez

            responseVerifyTag.response.forEach{tag ->
                if (!wasReader(tag.uuid!!)) updateProducts(tag)
            }

            if(!typeSearch){ //Para orden de entrada falta checar esto.

                Log.e("entrada", "ENTRADAAAA")
                //filterTagsNoRepeat()
                //filterTagsNoMatchFromRequest(responseVerifyTag)
                //filterTagsNoRepeatNoMatch()
            }

            Log.e("NO MATCH", aTagsNoMatch.toString())
            if(aTagsNoMatch.size > 0){
                requestNameUuid(RequestNameUuid(aTagsNoMatch))
                DeleteInvalidTags()
            }
            else presenter!!.setTotal(totalRead, "")

            //cleanTagsForVerify() //TODO, when this part is commented we are able to read order by order.
        }
        else presenter!!.setTotal(0, responseVerifyTag.error.toString())

        CheckOrderReady(responseVerifyTag)
        orderProducts()
        updateRecyclerView()
    }

    fun clearAll(){
        if(aTags.isNotEmpty()) aTags.clear()
        if(aTagsNoMatch.isNotEmpty()) aTagsNoMatch.clear()
        if(arrayAvoidedTags.isNotEmpty()) arrayAvoidedTags.clear()
        totalRead = 0
        aProducts.forEach{prod->
            prod.cantidadRegistrada = 0
            prod.validate = false
            prod.orderReady = false
        }
        validate = false
        orderReady = false
    }

    private fun DeleteInvalidTags(){
            //TODO, here we remove the invalid tags from the array that we send to the server in order to keep
            //TODO, only the valid tags, this is because if we make a first read and we find some tags that
            //TODO, don´t belong to the order and if we dont remove them from the array, when we make another read
        //TODO, and this time we only read the correct tags,  the invalid tags would show again altought we
        //TODO, haven´t read them again
        aTagsNoMatch.forEach{
            if(aTagsForVerify.contains(it)) aTagsForVerify.remove(it)
        }
    }

    private fun filterTagsNoRepeat(){
        val aDataNoRepeat = HashSet<String>()
        aDataNoRepeat.addAll(aTagsForVerify)
        aTagsForVerify.clear()
        aTagsForVerify.addAll(aDataNoRepeat)
    }

    private fun filterTagsNoMatchFromRequest(responseVerifyTag: ResponseVerifyTag){

        //Log.e("tagsForVerify", aTagsForVerify.toString())
        aTagsForVerify.forEach { tag ->

            responseVerifyTag.response.forEach {tagFromServer ->
                if(tag == tagFromServer.uuid) shouldAddNoMatch = false
            }

            if(shouldAddNoMatch){
                aTagsNoMatch.add(tag)
            } //Agrega los tags invalidos. Es decir, de los tags que leo los comparo con los tags correctos que recibo del servidor.
            shouldAddNoMatch = true
        }
        //Log.e("TAGS INVALIDOS", aTagsNoMatch.toString())
    }

    private fun filterTagsNoRepeatNoMatch(){
        val aDataNoRepeat = HashSet<String>()
        aDataNoRepeat.addAll(aTagsNoMatch)
        aTagsNoMatch.clear()
        aTagsNoMatch.addAll(aDataNoRepeat)
        //Log.e("ADATANOREPEAT", aTagsNoMatch.toString())
    }

    private fun DeleteRepeatTags(responseTags: ResponseVerifyTag){

        Log.e("EEES", responseTags.toString())
        Log.e("VENGAAA", aTagsForVerify.toString())

        responseTags.response.forEach{tag ->
            CheckCorrectAmount(tag)
        }

        /*aProducts.forEach{product->
            CheckCorrectAmount(product.id!!, product.cantidad!!, responseTags)
        }*/

        arrayAvoidedTags.forEach{
            aTagsNoMatch.add(it.uuid!!) //Aqui los agrego al noMatch en caso que hubieron tags de más.
        }

        val tagsResponse = arrayListOf<String>()
        responseTags.response.forEach{
            tagsResponse.add(it.uuid!!) //Aqui pasamos los tags del response del servidor a otro array para
                    //poder validar con contains
        }

        aTagsForVerify.forEach{item-> //aqui es para checar si no hay algun tag de más, se checan tags que no nos devuelve el server porque no pertencen a la orden.
            if(!tagsResponse.contains(item)) aTagsNoMatch.add(item)
        }

        //Log.e("ResultadoResponseVerify", arrayAvoidedTags.toString())
    }

    private fun CheckCorrectAmount(tag: ResponseVerifyTagItem){

        //En esta funcion se checa que no se hayan leido mas tags de lo requerido, comparando la cantidad
        //necesaria de cada orden, con los tags que nos devolvió el servidor, y los tags que estén de más
        //los agrego al array de noMatch para mostrarlos que hay de más.

        aProducts.forEachWithIndex{i, prod ->

            //Log.e("ID", prod.id.toString())
            //Log.e("LOTE", aProductLote[i].lote)
            //Log.e("IDunique", tag.id.toString())
            //Log.e("Loteunique", tag.lote.toString())

            if(prod.id!! == tag.id && aProductLote[i].lote != tag.lote){

            }
        }

        /*var cant = 0
        response.response.forEachWithIndex{i, item->

            Log.e("responseVerift", item.lote.toString())
            Log.e("id prod", id.toString())
            Log.e("prod lote", aProductLote[i].lote)

            if(id == item.id && aProductLote[i].lote == item.lote) cant++
        }

        if(cant > amountRequired){
            response.response.forEach{
                if(cant > amountRequired){
                    if(it.id == id){
                        cant--
                        arrayAvoidedTags.add(it)
                    }
                }
            }
        }*/
    }

    private fun CheckOrderReady(responseFromServer: ResponseVerifyTag){

        //Log.e("product", aProducts.toString())
        //Log.e("aTags", aTags.toString())
        //Log.e("SIZE RESPNSE SERVER", responseFromServer.response.size.toString())
        //Log.e("SIZE aTagsNomatch", aTagsNoMatch.toString())

        aProducts.forEach{producto->

            if( (producto.cantidad == producto.cantidadRegistrada) && totalRead == aProducts.size){ //Verificamos que la cantidad de productos solicitados, sea igual a los tags leidos correctos.

                //yproducto.orderReady = producto.validate && aTagsNoMatch.isEmpty() //IF reducido. Si "validate" es true y "aTagsNoMatch" está vacio -> orderReady se pone true.

                if(producto.validate && aTagsNoMatch.isEmpty()){

                    producto.orderReady = true //Esta propiedad es para pintar los circulos
                    orderReady = true //Esta se usa para validar cuando presionan el boton enviar y evitar hacer un for para que no imprima mensaje por cada iteracion
                }
            }
        }
    }

    private fun updateRecyclerView(){ //Esto es para actualizar el recycler y pintar el circulo verde asi como actualizar las cantidades.
        adapterProducts!!.swap(aProducts, 0)
    }

    private fun wasReader(tag: String): Boolean{

        //Log.e("aTAGS", aTags.toString()) //Inicialmente se encuentra vacio.

        aTags.forEach{ tagInner ->
            tagInner.tags!!.forEach{product ->
                if(tag == product ) return true
            }
        }
        return false
    }

    private fun updateProducts(responseVerifyTag: ResponseVerifyTagItem){

            //Aqui es para ir contando y agregando los productos de cada orden y validar cuando esten completos.

        //Log.e("aProducts", aProducts.toString()) // aProducts son los productos que regresa el servidor
        //Log.e("aProductsLote ", aProductLote.toString())
        //Log.e("resopnseVerifyTag", responseVerifyTag.toString()) //responseVerifyTag contiene el uuid, lote y id de cada producto de aProducts

        var almacen: String

        aProducts.forEachIndexed {index, product ->

            almacen = if(movimientoType.equals("Recibo traspaso", true)) product.almacen_destino?:""
            else product.almacen?:""

            if ( (product.id!! == responseVerifyTag.id) && (aProductLote[index].lote == responseVerifyTag.lote)
                    && (almacen.trim() == responseVerifyTag.almacen?.trim())){

                if(product.cantidadRegistrada < product.cantidad!!){

                    CheckArrayNoMatch(responseVerifyTag.uuid!!)
                    responseVerifyTag.correct = true

                    product.cantidadRegistrada++ //Producto correcto. Ejmplo:  van 2 de 5, van 3 de 5, van 4 de 5 etc...
                    addTagReaded(product.origenId!!, responseVerifyTag.uuid)

                    SaveTagToDB(responseVerifyTag.uuid, CODIGO_REMISION)

                    if (product.cantidadRegistrada == product.cantidad){  //Entra aquí cuando la orden del pedido está completa. Ejemplo; 5/5

                        //Log.e("C", "C")
                        product.validate = true
                        validate = true
                        totalRead++
                    }
                }
                else AddTagNoMatch(responseVerifyTag)
            }
            else AddTagNoMatch(responseVerifyTag)
        }
    }

    private fun AddTagNoMatch(tag: ResponseVerifyTagItem){
            //TODO, here we check if the tag has been validated, if its true it means that the tag belongs to
            //TODO, some order and has been verified. If not, maybe the tag doesn't belong to any order.
        if(!tag.correct){
            if(!aTagsNoMatch.isNullOrEmpty()){
                if(!aTagsNoMatch.contains(tag.uuid!!)) aTagsNoMatch.add(tag.uuid)
            }
            else aTagsNoMatch.add(tag.uuid!!)
        }
    }

    private fun CheckArrayNoMatch(tag: String){
                //TODO, here we remove the tag from the array because it exists in the order
        if(aTagsNoMatch.contains(tag)) aTagsNoMatch.remove(tag)
    }

    private fun addTagReaded(originId: Int, tag: String){
        aTags.forEach { tagInner ->
            if (originId == tagInner.origenId){
                tagInner.tags!!.add(tag)
                return
            }
        }
        aTags.add(RequestRemisionProducts(originId, arrayListOf(tag)))
    }

    private fun SaveTagToDB(tag: String, codigoRemision: String){

        if(!tagsFromDB.contains(tag)){
            doAsync {
                val database = RoomController.getInstance()
                //database.daoOrdenes().deleteOrdenesFromDB()
                val roomOrden = RoomOrdenes(0, null,null)
                roomOrden.remisionNumber = codigoRemision
                roomOrden.uuidNumber = tag
                database.daoOrdenes().InsertOrden(roomOrden)

                Log.e("Registro exitoso", tag)
            }
        }
    }

    var tagsFromDB = arrayListOf<String>()
    override fun LoadLastStateOrder(tags: ArrayList<String>){
        tagsFromDB = tags
        tags.forEach{
            aTagsForVerify.add(it)
        }
    }

    override fun setLotes(aProducts: List<ResponseOrderProduct>){
        aProducts.forEach {product ->
            //Log.e("PRODUCTOLON:", product.nombre)
            //Log.e("CODIGOO:", product.codigo)
            Log.e("LOTES", product.tagsLoteSalida.toString())
            product.tagsLoteSalida?.forEach{ lote ->
                aLotes.add(lote!!.lote?:"")
                aProductLote.add(Product(product.id!!, lote.lote?:""))
            }
        }
    }

    override fun setProducts(aProducts: List<ResponseOrderProduct>, movimientoType: String) {
        this.aProducts = aProducts as ArrayList<ResponseOrderProduct>
        this.movimientoType = movimientoType
    }

    override fun addTagForVerify(tag: String){
        //Log.e("tgas for verify array", aTagsForVerify.toString())
        if(aTagsForVerify.isEmpty()) aTagsForVerify.add(tag)
        else if(!aTagsForVerify.contains(tag)) aTagsForVerify.add(tag)
    }

    override fun setAdapter(adapter: AdapterProducts){
        adapterProducts = adapter
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        if(nameItem.equals("Lectura unica")) {
            presenter!!.setModeRead(true)
            presenter!!.setItemNameMenu("Lectura continua", item)
        } else{
            presenter!!.setModeRead(false)
            presenter!!.setItemNameMenu("Lectura unica", item)
        }
    }

    override fun sendData(totalProducts : Int){ //Se manda la orden de salida de los productos una vez verificados.

        if(aTags.size > 0){

            Log.e("Atags", aTags.toString())
            Log.e("TotalRead", totalRead.toString())
            Log.e("TotalRead", aProducts.size.toString())

            if(totalRead == aProducts.size){

                //Log.e("validate", validate.toString())
                //Log.e("orderrady", orderReady.toString())

                if(validate && orderReady){

                    requestRemision.productos = aTags

                    val gson = Gson()
                    val json = gson.toJson(requestRemision)
                    //Log.e("JSON", json.toString())

                    val retrofit = RetrofitController(this.appContext!!)
                    retrofit.sendRemision(requestRemision, this, typeSearch)
                }
                else if(validate) presenter!!.showNoCompleteOrder(1) //Hay productos de más en la orden
                else presenter!!.showNoCompleteOrder(-1) //Faltan productos en la orden
            }
            else presenter!!.showNoCompleteOrder(-1)
        }
        else presenter!!.showNoProductsToSend()
    }

    override fun successRequestRemision(response: ResponseRemision){
        presenter!!.successRequestRemision(response.response)
    }

    override fun unsuccessRequestRemision(error: String){
        //Log.e("unsuccess remision", "unsu")
        presenter!!.unsuccessRequestRemision(error)
    }

    override fun sendData(){ //no se usa este
        this.requestRemision.productos = aTags
        val retrofit = RetrofitController(this.appContext!!)
        retrofit.sendRemision(this.requestRemision, this, typeSearch)
    }

    override fun setTypeSearch(typeSearch: Boolean){
        this.typeSearch = typeSearch
    }

    private fun requestNameUuid(requestNameUuid: RequestNameUuid){
        val retrofit = RetrofitController(this.appContext!!)
        retrofit.getNameUuid(requestNameUuid, this)
    }

    override fun successRequestNameUuid(response: ResponseNameUuid){ //Nombres de los tags que no son validos
        //Log.e("LOPS TAGS", response.toString())
        presenter!!.setTotal(totalRead, "") //Lo pongo aqui para que el progressBar se quite hasta que termine la peticion de recuperar los nombres de los tags no validos.
        presenter!!.tagsNoMatch(createListNoMatchTotal(response.resultado!!))
        cleanTagsNoMatch()
    }

    override fun ErrorRequestNameUuid(msj: String, total: Int){
        //Log.e("unsuccess remision", "error reques name")
        presenter!!.ErrorRequestNameUuid(msj, totalRead)
    }

    private fun orderProducts(){

        val aProductsDisorder = aProducts
        aProducts = ArrayList()
        val aProducts = ArrayList<ResponseOrderProduct>()

        aProductsDisorder.forEach { product ->
            if(!product.validate) aProducts.add(product)
        }

        aProductsDisorder.forEach{ product ->
            if(product.validate) aProducts.add(product)
        }

        this.aProducts = aProducts
        aProductLote.clear()
        this.aProducts.forEach {product ->
            product.tagsLoteSalida!!.forEach { lote ->
                aLotes.add(lote!!.lote!!)
                aProductLote.add(Product(product.id!!, lote.lote!!))
            }
        }
    }

    private fun cleanTagsNoMatch(){
        aTagsNoMatch.clear()
    }

    private fun createListNoMatchTotal(aTagsNoMatch: List<String>): ArrayList<TagNoMatch>{
                            //TODO, function to know the total of every invalid tag
        var shouldAdd: Boolean
        val aNoMatchTotal = ArrayList<TagNoMatch>()

        aTagsNoMatch.forEach { tag ->

            shouldAdd = true

            if (aNoMatchTotal.size > 0) {

                for(tagNoMatch in aNoMatchTotal){
                    if (tag == tagNoMatch.name) {
                        tagNoMatch.amount++
                        shouldAdd = false
                    }
                }
            }

            if(shouldAdd) aNoMatchTotal.add(TagNoMatch(tag, 1))
        }

        return aNoMatchTotal
    }
}
package com.example.pingpongalien.conthea.Activities.Core
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

class CoreModel(presenter: CorePresenter, val sharedPreferences: PreferencesController): Core.Model {

    var presenter: Core.Presenter? = null
    var preferences: PreferencesController? = null

    init {
        this.presenter = presenter
        this.preferences = sharedPreferences
    }

    override fun getUser() {
        val name = preferences!!.GetName()
        val apellido = preferences!!.GetApellido()
        presenter!!.setNameUser(name!!, apellido!!)
    }
}
package com.example.pingpongalien.conthea.Fragments.InitialAdjustment

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.Models.TagsForRegister
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponsRegisterTags
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

class AltaTagPresenter(myview: AltaTag.View, context: Context, preferences: PreferencesController): AltaTag.Presenter{

    val model: AltaTag.Model = AltaTagModel(this, context, preferences)
    val view: AltaTag.View = myview

    override fun AddTag(tag: String){
        model.AddTag(tag)
    }

    override fun SendToRetrofit(){
        model.SendToRetrofit()
    }

    override fun SetAmount(total: Int){
        view.SetAmount(total)
    }

    override fun Unsuccess(error: String) {
        view.Unsuccess(error)
    }

    override fun setModeRead(mode: Boolean) {
        view.setModeRead(mode)
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        view.setItemNameMenu(name, item)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        model.setModeRead(nameItem, item)
    }

    override fun SetAdapterRegister(tags: ArrayList<TagsForRegister>, tagsOff: ArrayList<String>, inOrderOut: String){
        view.SetAdapterRegister(tags, tagsOff, inOrderOut)
    }

    override fun SendTagsRegister(tags: ArrayList<TagsForRegister>){
        model.SendTagsRegister(tags)
    }

    override fun SuccessTagsRegister(resp: String){
        view.SuccessTagsRegister(resp)
    }

    override fun UnSuccessTagsRegister(resp: String){
        view.UnSuccessTagsRegister(resp)
    }
}
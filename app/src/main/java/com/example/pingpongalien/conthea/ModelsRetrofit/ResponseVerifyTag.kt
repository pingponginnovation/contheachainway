package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseVerifyTag(
	@field:SerializedName("response")
	val response: ArrayList<ResponseVerifyTagItem>,

	@field:SerializedName("error")
	val error: String? = null
)
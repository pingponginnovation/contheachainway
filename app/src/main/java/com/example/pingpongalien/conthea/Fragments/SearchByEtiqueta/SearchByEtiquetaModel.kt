package com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta

import android.content.Context
import android.util.Log
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication
import com.example.pingpongalien.conthea.Helpers.HelperDate
import com.example.pingpongalien.conthea.Models.ProductChild
import com.example.pingpongalien.conthea.ModelsRetrofit.*
import com.example.pingpongalien.conthea.Retrofit.RetrofitController

class SearchByEtiquetaModel(presenter: SearchByEtiqueta.Presenter, context : Context): SearchByEtiqueta.Model{

    private var presenter: SearchByEtiqueta.Presenter? = null
    private var appContext: Context
    private val arrayWithUbication: ArrayList<ParentUbication> = ArrayList()
    private val arrayWithOutUbication: ArrayList<ParentUbication> = ArrayList()
    private var arrayTags: ArrayList<String> = arrayListOf()
    private var arrayShowedInUi: ArrayList<String> = arrayListOf()

    init {
        this.presenter = presenter
        this.appContext = context
    }

    override fun addTag(tag: String){
        if(!arrayTags.contains(tag)) arrayTags.add(tag)
    }

    override fun cleanTags() {
        if(arrayTags.isNotEmpty()) arrayTags.clear()
        if(arrayWithOutUbication.isNotEmpty()) arrayWithOutUbication.clear()
        if(arrayWithUbication.isNotEmpty()) arrayWithUbication.clear()
        if(arrayShowedInUi.isNotEmpty()) arrayShowedInUi.clear()
    }

    override fun sendTags(){
        if(arrayTags.isNotEmpty()){
            val retrofit = RetrofitController(appContext)
            val req = RequestEtiquetas(arrayTags)
            retrofit.search_by_etiquetas(this, req)
        }
        else presenter!!.errorTags("No se leyó ningun tag")
    }

    override fun successTags(response: ResponseEtiquetas?){
        Log.e("response", response?.response.toString())
        ConfigProducts(response?.response?.productos?.con_espacio, response?.response?.productos?.sin_espacio)
        presenter!!.successTags(arrayWithOutUbication, arrayWithUbication)
    }

    override fun errorTags(resp: String){
        presenter!!.errorTags(resp)
    }

    private fun ConfigProducts(productosEspacio: ArrayList<ResponseProductosSpace>?, productosSinEspacio: ArrayList<ResponseProductosSpace>?){

        if(!productosEspacio.isNullOrEmpty()){

            productosEspacio.forEach{

                if(arrayShowedInUi.isNullOrEmpty() || !arrayShowedInUi.contains(it.uuid)){

                    val arrayChilds: ArrayList<ProductChild> = arrayListOf() //Array de hijos

                    val child = ProductChild(it.uuid?:"", it.producto?.nombre?:"", it.lote?:"", it.caducidad?:"",
                            it.almacen?.codigo?:"", it.espacio?.codigo?:"", it.producto?.fabricante?:"", it.cantidad?:0,
                            it.producto?.articulo?:"", it.espacios_id?:0, it.producto?.id?:0, it.activo_id) //Creamos hijo
                    arrayChilds.add(child) //Añadimos hijo al array

                    if(arrayWithUbication.isNullOrEmpty()){
                        it.uuid?.let { it1 -> arrayShowedInUi.add(it1) }
                        arrayWithUbication.add(ParentUbication(arrayChilds, it.producto?.articulo?:"", it.activo_id)) //Se crea el primer padre
                    }
                    else{

                        var bandera = -1

                        arrayWithUbication.forEach{parent ->

                            arrayChilds.forEach{child ->

                                if(parent.catalogoText == child.catalogue){ //Agregamos nuevo hijo a padre existente en el array
                                    val newChild = ProductChild(it.uuid?:"", child.description, child.lote, child.expiration, child.storage,
                                            child.ubication, child.maker, child.cantidad, child.catalogue, child.espacio_id, child.producto_id, it.activo_id)
                                    parent.childList!!.add(newChild)
                                    it.uuid?.let { it1 -> arrayShowedInUi.add(it1) }
                                    bandera = 1
                                }
                            }
                        }

                        if(bandera == -1){
                            it.uuid?.let { it1 -> arrayShowedInUi.add(it1) }
                            arrayWithUbication.add(ParentUbication(arrayChilds, it.producto?.articulo?:"", it.activo_id))
                        } //Creamos nuevo padre, en caso de no existir
                    }
                }
            }
        }

        if(!productosSinEspacio.isNullOrEmpty()){

            productosSinEspacio.forEach{

                if(arrayShowedInUi.isNullOrEmpty() || !arrayShowedInUi.contains(it.uuid)){

                    val arrayChilds: ArrayList<ProductChild> = arrayListOf() //Array de hijos

                    val child = ProductChild(it.uuid?:"",it.producto?.nombre?:"", it.lote?:"", it.caducidad?:"",
                            it.almacen?.codigo?:"", it.espacio?.codigo?:"", it.producto?.fabricante?:"", it.cantidad?:0,
                            it.producto?.articulo?:"", it.espacios_id?:0, it.producto?.id?:0, it.activo_id) //Creamos hijo
                    arrayChilds.add(child) //Añadimos hijo al array

                    if(arrayWithOutUbication.isNullOrEmpty()){
                        it.uuid?.let { it1 -> arrayShowedInUi.add(it1) }
                        arrayWithOutUbication.add(ParentUbication(arrayChilds, it.producto?.articulo?:"", it.activo_id))  //Se crea el primer padre
                    }
                    else{

                        var bandera = -1

                        arrayWithOutUbication.forEach{parent ->

                            arrayChilds.forEach{child ->

                                if(parent.catalogoText == child.catalogue){ //Agregamos nuevo hijo a padre existente en el array
                                    val newChild = ProductChild(it.uuid?:"", child.description, child.lote, child.expiration, child.storage,
                                            child.ubication, child.maker, child.cantidad, child.catalogue, child.espacio_id, child.producto_id, it.activo_id)
                                    parent.childList!!.add(newChild)
                                    it.uuid?.let { it1 -> arrayShowedInUi.add(it1) }
                                    bandera = 1
                                }
                            }
                        }

                        if(bandera == -1){
                            it.uuid?.let { it1 -> arrayShowedInUi.add(it1) }
                            arrayWithOutUbication.add(ParentUbication(arrayChilds, it.producto?.articulo?:"", it.activo_id)) //Creamos nuevo padre, en caso de no existir
                        }
                    }
                }
            }
        }
        //Log.e("inUi", arrayShowedInUi.toString())
    }
}
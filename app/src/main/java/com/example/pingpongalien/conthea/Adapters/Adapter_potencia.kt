package com.example.pingpongalien.conthea.Adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.pingpongalien.conthea.R

class Adapter_potencia: RecyclerView.Adapter<Adapter_potencia.ViewHolder>(){

    private var potencias: ArrayList<Int>? = null
    private lateinit var listener: (Int) -> Unit?

    fun Adapter_potencia(pots: ArrayList<Int>, listener: (Int) -> Unit){
        potencias = pots
        this.listener = listener
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val titulo = holder.itemView.findViewById(R.id.txt_potencia) as TextView

        titulo.text = potencias!![position].toString()
        holder.itemView.setOnClickListener{
            listener(potencias!![position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_spinner_potency, parent, false))
    }

    override fun getItemCount(): Int {
        return potencias!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}

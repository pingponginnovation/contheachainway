package com.example.pingpongalien.conthea.Fragments.InitialAdjustment

import android.content.Context
import android.util.Log
import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseInitialAdjustment
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import com.google.gson.Gson

class InitialAdjustmentModel(presenter: InitialAdjustment.Presenter, context: Context, preferences: PreferencesController): InitialAdjustment.Model{

    data class RequestInitialAdjustment(val tags: ArrayList<String>)

    val presenter: InitialAdjustment.Presenter = presenter
    private var misTags = arrayListOf<String>()
    val appContext: Context = context

    override fun AddTag(tag: String){

        if(misTags.isEmpty()) misTags.add(tag)
        else if(!misTags.contains(tag)) misTags.add(tag)

        Log.e("TOTAL:", misTags.size.toString())

        //SetAmount(misTags.size)
    }

    override fun SendToRetrofit(){

        Log.e("ENTRA:", "YEAHHHHHHHHH")

        val inventoryAdjustment = RequestInitialAdjustment(misTags)
        val gson = Gson()
        val ajuste = gson.toJson(inventoryAdjustment)

        val retrofit = RetrofitController(appContext)
        retrofit.SendTagsRead(ajuste, this)
    }

    override fun Success(response: ResponseInitialAdjustment){
        SetAmount(response.response.toInt())
    }

    override fun Unsuccess(error: String) {
        presenter.Unsuccess(error)
    }

    fun SetAmount(total: Int){
        presenter.SetAmount(total)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        if(nameItem.equals("Lectura continua")) {
            presenter.setModeRead(true)
            presenter.setItemNameMenu("Lectura única", item)
        } else{
            presenter.setModeRead(false)
            presenter.setItemNameMenu("Lectura continua", item)
        }
    }
}
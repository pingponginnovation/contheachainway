package com.example.pingpongalien.conthea.Activities.DetailOrder

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.Adapters.AdapterProducts
import com.example.pingpongalien.conthea.Models.TagNoMatch
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderProduct

/**
 * Created by AOR on 28/11/17.
 */
class DetailOrderPresenter(viewDetail: DetailOrder.View, context : Context) : DetailOrder.Presenter {

    var view: DetailOrder.View? = null
    var model: DetailOrder.Model? = null

    init {
        this.view = viewDetail
        model = DetailOrderModel(this, context)
    }

    override fun successRequestRemision(response: String) {
        view!!.successRequestRemision(response)
    }

    override fun unsuccessRequestRemision(error: String) {
        view!!.unsuccessRequestRemision(error)
    }

    override fun addTag(tag: String) {
        model!!.addTagForVerify(tag)
    }

    override fun setLotes(aProducts: List<ResponseOrderProduct>) {
        model!!.setLotes(aProducts)
    }

    override fun setProducts(aProducts: List<ResponseOrderProduct>, movimientoType: String) {
        model!!.setProducts(aProducts, movimientoType)
    }

    override fun setAdapter(adapter: AdapterProducts) {
        model!!.setAdapter(adapter)
    }

    override fun sendData(totalProducts : Int){
        model!!.sendData(totalProducts)
    }

    override fun sendData() {
        model!!.sendData()
    }

    override fun setModeRead(mode: Boolean) {
        view!!.setModeRead(mode)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        model!!.setModeRead(nameItem, item)
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        view!!.setItemNameMenu(name, item)
    }

    override fun searchTag() {
        model!!.searchTag()
    }

    override fun LoadLastStateOrder(tags: ArrayList<String>) {
        model!!.LoadLastStateOrder(tags)
    }

    override fun setTypeSearch(typeSearch: Boolean) {
        model!!.setTypeSearch(typeSearch)
    }

    override fun tagsNoMatch(aTagsNoMatch:  ArrayList<TagNoMatch>) {
        view!!.tagsNoMatch(aTagsNoMatch)
    }

    override fun setTotal(total: Int, msj: String) {
        view!!.setTotal(total, msj)
    }

    override fun showNoProductsToSend() {
        view!!.showNoProductsToSend()
    }

    override fun showNoCompleteOrder(status: Int) {
        view!!.showNoCompleteOrder(status)
    }

    override fun SetProgressBar(state: Boolean){
        view!!.SetProgressBar(state)
    }

    override fun ErrorRequestNameUuid(msj: String, total: Int){
        view!!.ErrorRequestNameUuid(msj, total)
    }

    override fun ErrorPeticionNetwork(){
        view!!.ErrorPeticionNetwork()
    }

    override fun UnsuccessVerifyTags(resp: String) {
        view!!.UnsuccessVerifyTags(resp)
    }
}
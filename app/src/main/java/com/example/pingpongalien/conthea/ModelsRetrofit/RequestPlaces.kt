package com.example.pingpongalien.conthea.ModelsRetrofit

data class RequestPlaces(
		val espacios_id: Int? = null,
		val uuid: List<String?>? = null
)

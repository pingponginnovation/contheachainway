package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseAjuste(
        @SerializedName("response")
        val total: String
)
package com.example.pingpongalien.conthea.Fragments.SetPlace

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.*
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView.Companion.mContext
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Adapters.*
import com.example.pingpongalien.conthea.Adapters.SectionedRecyclerSetPlace.AdapterLocationTags
import com.example.pingpongalien.conthea.Adapters.SectionedRecyclerSetPlace.ParentLocationTags
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.Models.*
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponsePlacesItems
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import com.example.pingpongalien.conthea.SoundPlay
import com.rscja.deviceapi.RFIDWithUHFUART
import com.rscja.deviceapi.entity.UHFTAGInfo
import kotlinx.android.synthetic.main.dialog_select_potencia.*
import kotlinx.android.synthetic.main.dialog_select_potencia.btn_close_dialog
import kotlinx.android.synthetic.main.dialog_select_product.*
import kotlinx.android.synthetic.main.dialog_select_product.titulo_
import kotlinx.android.synthetic.main.fragment_set_place_updated.*
import kotlinx.android.synthetic.main.fragment_set_place_updated.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast

class SetPlaceView(val core: CoreView): ReaderFragment(), SetPlace.View {

    val TAG = "SetPlaceView"
    var presenter: SetPlace.Presenter? = null
    var preferences: PreferencesController? = null

    private var readSingle = false
    var adapterTagsLocation: AdapterLocationTags? = null
    private val arrayParent: ArrayList<ParentLocationTags> = ArrayList()
    private val uidsLoaded: ArrayList<String> = arrayListOf()
    private var tagsToSend: ArrayList<String> = arrayListOf()
    var builder: AlertDialog.Builder? = null
    var txt_total: TextView? = null
    var viewDialog: View? = null
    var idTxtMsj: TextView? = null
    var img: ImageView? = null
    private var auxiliar = 0
    private lateinit var mview: View
    private var dialog2: Dialog? = null
    private var dialog: Dialog? = null
    private var mSound: SoundPlay? = null
    private var popupWindow: PopupWindow? = null
    private var allSelected = false
    private var space = ""
    private var mAdapter: Adapter_potencia = Adapter_potencia()
    companion object{
        private var POWER_LEVEL = 20
        var loopFlag = false
        var mReader: RFIDWithUHFUART? = null
        var handlerMSJ: Handler? = null
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mview = inflater!!.inflate(R.layout.fragment_set_place_updated, container, false)

        presenter = SetPlacePresenter(this, mview.context)
        preferences = PreferencesController(mview.context)

        presenter!!.getPlaces("SET_PLACE")

        mSound = SoundPlay(context)
        iinit()
        configPotency()
        configLectura()
        createDialogTotal()

        mview.btn_clear_screen.setOnClickListener{
            if(arrayParent.isNotEmpty()){
                ClearScreenAndData()
            }
        }

        mview.btn_asignar.setOnClickListener{
            if(tagsToSend.isEmpty()) context.toast("Selecciona los tags")
            else{
                mview.progres_asign_place.visibility = View.VISIBLE
                presenter!!.SendCahngeLocationTags(tagsToSend, space)
            }
        }

        mview.btn_select_all.setOnClickListener{
            if(allSelected){
                allSelected = false
                MarkTags(false)
            }
            else{
                allSelected = true
                MarkTags(true)
            }
        }
        return mview
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //eventView()
        //initSound()
        core.changeTitleToolbar("Asignar lugar")
        InitHandler()
        //setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_detail_verify, menu)
    }

    /*override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.item_potency -> showPotency()
            R.id.item_type_read -> presenter!!.setModeRead(item.title.toString(), item)
        }
        return super.onOptionsItemSelected(item)
    }*/

    override fun setModeRead(mode: Boolean){
        //readSingle = mode
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        //item.title = name
    }

    override fun initReader(){}
    override fun activateReader(){}

    fun iinit(){
        try {
            mReader = RFIDWithUHFUART.getInstance()
            if (mReader != null){

                if (mReader != null) {

                    mview.autocomplete_potencia.setText(POWER_LEVEL.toString())

                    someTask().MyCustomTask(context)
                    someTask().execute()
                }
            }
        } catch (e: Exception) {
            context.toast(e.message!!)
        }
    }

    class someTask: AsyncTask<Void, Void, Boolean>() {

        var mypDialog: ProgressDialog? = null

        override fun doInBackground(vararg params: Void?): Boolean?{
            return mReader!!.init()
        }

        fun MyCustomTask(context: Context) {
            mContext = context
        }

        override fun onPreExecute() {
            super.onPreExecute()

            mypDialog = ProgressDialog(mContext)
            mypDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            mypDialog!!.setMessage("Iniciando RFID...")
            mypDialog!!.setCanceledOnTouchOutside(false)
            mypDialog!!.show()
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            mypDialog!!.cancel()
            if (!result!!) {
                Toast.makeText(mContext, "init fail", Toast.LENGTH_SHORT).show()
            }
            else{
                try {
                    mReader!!.power = POWER_LEVEL
                }catch(e: Exception){}
            }
        }
    }

    private fun configLectura(){
        mview.btn_continue_read.background.setColorFilter(ContextCompat.getColor(context, R.color.green_read), PorterDuff.Mode.MULTIPLY)
        mview.btn_continue_read.setTextColor(Color.WHITE)

        mview.btn_single_read.setOnClickListener{
            readSingle = true
            mview.btn_continue_read.background.setColorFilter(ContextCompat.getColor(context, R.color.gray_read), PorterDuff.Mode.MULTIPLY)
            mview.btn_continue_read.setTextColor(Color.parseColor("#2D2B2B"))

            mview.btn_single_read.background.setColorFilter(ContextCompat.getColor(context, R.color.green_read), PorterDuff.Mode.MULTIPLY)
            mview.btn_single_read.setTextColor(Color.WHITE)
        }

        mview.btn_continue_read.setOnClickListener{
            readSingle = false
            mview.btn_continue_read.background.setColorFilter(ContextCompat.getColor(context, R.color.green_read), PorterDuff.Mode.MULTIPLY)
            mview.btn_continue_read.setTextColor(Color.WHITE)

            mview.btn_single_read.background.setColorFilter(ContextCompat.getColor(context, R.color.gray_read), PorterDuff.Mode.MULTIPLY)
            mview.btn_single_read.setTextColor(Color.parseColor("#2D2B2B"))
        }
    }

    private fun configPotency(){

        mview.autocomplete_potencia.setOnClickListener{
            mview.autocomplete_potencia.isEnabled = false
            val arrayPotency = arrayListOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                    21,22,23,24,25,26,27,28,29,30)
            LoadPotenciasRecycler(arrayPotency)
        }

        /*adapterPotencias = AdapterSpinnerPotencia(arrayPotency){Setpotencia(it)}

        mview.sp_potencia.setOnClickListener{
            popupWindow?.dismiss()
            if (popupWindow == null) providePopupWindow(mview.shower_listview)
            popupWindow!!.showAsDropDown(mview.shower_listview, 0, - mview.shower_listview.height)
        }*/
    }

    private fun LoadPotenciasRecycler(potencia: ArrayList<Int>){
        dialog = Dialog(activity!!)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.dialog_select_potencia)

        dialog!!.rclv_potencias.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        dialog!!.rclv_potencias.setHasFixedSize(true)
        dialog!!.rclv_potencias.addItemDecoration(SimpleDividerItemDecoration(context))
        mAdapter.Adapter_potencia(potencia){Setpotencia(it)}
        dialog!!.rclv_potencias.adapter = mAdapter

        dialog!!.btn_close_dialog!!.setOnClickListener{
            mview.autocomplete_potencia.isEnabled = true
            dialog?.dismiss()
        }

        dialog!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog!!.show()
    }

    private fun providePopupWindow(vieww: View){
        /*popupWindow = PopupWindow(vieww.width, ViewGroup.LayoutParams.WRAP_CONTENT)
            .apply {
                val backgroundDrawable = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    activity!!.getDrawable(R.drawable.design_autocomplete).apply{}
                } else {
                    Log.e("Else", "menor lollipop")
                    TODO("VERSION.SDK_INT < LOLLIPOP")
                }
                setBackgroundDrawable(backgroundDrawable)
                isOutsideTouchable = true

                val listView = layoutInflater.inflate(R.layout.layout_potencia_dropdown, null,
                    false) as ListView
                listView.adapter = adapterPotencias
                contentView = listView
            }*/
    }

    private fun Setpotencia(num: Int){
        mview.autocomplete_potencia.isEnabled = true
        dialog?.dismiss()
        if(mReader!!.setPower(num)){
            mview.autocomplete_potencia.setText(num.toString())
            POWER_LEVEL = num
            context.toast("Potencia ajustada correctamente a: $num")
        }
        else context.toast("Error en establecer potencia")
    }

    override fun setPlaces(aPlaces : ArrayList<String>){ //Llenado del spinner del menu "Asignar lugar"
        mview.progres_asign_place.visibility = View.GONE
        mview.ly_inicia_lectura_msg.visibility = View.VISIBLE

        val first = aPlaces[0]
        sp_places_ac.setText(first)
        space = first

        sp_places_ac.setOnClickListener{
            LoadSpacesInRecycler(aPlaces)
        }
    }

    var auxi = -1
    override fun SetAdapterLocationTags(tagsCounted: ArrayList<TagsChangeLocation2>, tags: ArrayList<ResponsePlacesItems>){
                                    //Here we fill up the adapter with the tags available to change location

        if(tags.isNotEmpty()){
                                    //Similar a una lista de listas
            tagsCounted.forEach{

                val arrayChild: ArrayList<ChildLocationTags> = ArrayList()

                tags.forEach{child ->

                    if(it.article?.trim() == child.article?.trim() && it.lote?.trim() == child.lote?.trim() && it.almacen?.trim() == child.storage?.trim()){

                        if(uidsLoaded.isNotEmpty()){
                            if(!uidsLoaded.contains(child.uuid)){ //Checamos que no se repitan los uids que ya estan mostrados en el recycler
                                uidsLoaded.add(child.uuid!!)
                                val itemChild = ChildLocationTags(child.article!!, child.uuid, child.lote, child.storage, false)
                                arrayChild.add(itemChild)
                                auxi = 1
                            }
                        }
                        else{
                                //no se ha mostrado nada en el recycler, es primera vez
                            uidsLoaded.add(child.uuid!!)
                            val itemChild = ChildLocationTags(child.article!!, child.uuid, child.lote, child.storage, false)
                            arrayChild.add(itemChild)
                            auxi = 1
                        }
                    }
                }
                if(auxi != -1){
                        //Esta funcion verifica si un padre ya existe para agregar su nuevo hijo, sino existe, entonces se crea nuevo padre con sus hijos
                    var bandera = -1

                    if(arrayParent.isNotEmpty()){

                        arrayParent.forEach{ parent ->

                            arrayChild.forEach{new ->

                                if(parent.catalogoText == new.catalogo){  //Agregamos nuevo hijo a un padre que ya existia en el recyclerview
                                    val newChild = ChildLocationTags(parent.catalogoText, new.uuid!!, new.lote!!, new.almacen!!, false)
                                    parent.childItems.add(newChild)
                                    val totalAntes = parent.cantidadText.toInt()
                                    val nuevoTotal = totalAntes+1
                                    parent.cantidadText = nuevoTotal.toString()
                                    bandera = 1
                                }
                            }
                        }

                        if(bandera == -1) arrayParent.add(ParentLocationTags(arrayChild, it.article, it.total.toString()))  //Nuevo padre
                        bandera = -1
                    }
                    else arrayParent.add(ParentLocationTags(arrayChild, it.article, it.total.toString())) //Nuevo padre
                }
                auxi = -1
            }

            mview.progres_asign_place.visibility = View.GONE
            mview.ly_buttons.visibility = View.VISIBLE
            mview.rclv_tags.visibility = View.VISIBLE
            mview.ly_inicia_lectura_msg.visibility = View.GONE
            mview.rclv_tags.apply{
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapterTagsLocation = AdapterLocationTags(context, arrayParent){Click(it)}
                adapter = adapterTagsLocation
            }
            mview.txt_tags_leidos.text = "Total de tags leídos: ${uidsLoaded.size}"
            auxi = -1
        }
        else{
            auxiliar = 0
            if(arrayParent.isNotEmpty()) HideViews(true)
            else HideViews(false)
            mview.context.toast("No hay productos que mostrar")
        }
    }

    private fun ClearScreenAndData(){
        arrayParent.clear()
        uidsLoaded.clear()
        tagsToSend.clear()
        HideViews(false)
        mview.txt_tags_leidos.text = "Total de tags leídos: 0"
    }

    private fun Click(datos: DataLocation){

        arrayParent.forEach{

            if(datos.type == "CATALOGO"){
                            //Cuando se le da click al checkbox "seleccionar todoo" de una sección
                if(datos.data == it.catalogoText){
                    it.childItems.forEach{item ->

                        if(datos.state){
                            if(!tagsToSend.contains(item.uuid!!)) tagsToSend.add(item.uuid)
                        }
                        else tagsToSend.remove(item.uuid)

                        item.selected = datos.state
                    }
                }
            }
            else{
                        //Cuando se le da click a checkbox individualmente
                it.childItems.forEach{item ->

                    if(datos.data == item.uuid){
                        if(datos.state){
                            if(!tagsToSend.contains(item.uuid)) tagsToSend.add(item.uuid)
                        }
                        else tagsToSend.remove(item.uuid)

                        item.selected = datos.state
                    }
                }
            }
        }
        adapterTagsLocation!!.notifyDataSetChanged()
    }

    private fun MarkTags(selectAll: Boolean){
        if(selectAll){
            arrayParent.forEach{
                it.childItems.forEach{item ->

                    if(tagsToSend.isEmpty()) tagsToSend.add(item.uuid!!)
                    else if(!tagsToSend.contains(item.uuid)) tagsToSend.add(item.uuid!!)

                    item.selected = true
                }
            }
        }
        else{

            if(tagsToSend.isNotEmpty()) tagsToSend.clear()

            arrayParent.forEach{
                it.childItems.forEach{item ->
                    item.selected = false
                }
            }
        }
        adapterTagsLocation!!.notifyDataSetChanged()
    }

    private fun HideViews(hasSomething: Boolean){
        if(hasSomething) mview.progres_asign_place.visibility = View.GONE
        else{
            mview.progres_asign_place.visibility = View.GONE
            mview.ly_inicia_lectura_msg.visibility = View.VISIBLE
            mview.rclv_tags.visibility = View.GONE
            mview.ly_buttons.visibility = View.GONE
        }
    }

    override fun SuccessChangeLocationTags(msj: String){
        mview.progres_asign_place.visibility = View.GONE
        CreateDialogRespuesta(msj, 1)
    }

    override fun UnSuccessChangeLocationTags(msj: String){
        mview.progres_asign_place.visibility = View.GONE
        CreateDialogRespuesta(msj, -1)
    }

    override fun showErrorGetPlaces(error: String) {
        if(error == "Unauthorized") showExpiredSession()
        progres_asign_place.visibility = View.GONE
        ly_inicia_lectura_msg.visibility = View.VISIBLE
        mview.txt_inicia_lectura.text = error
        //context.toast(error)
    }

    override fun setNamePlace(name: String) {
        //context.toast(name)
    }

    override fun setTotalUpdate(totalUpdate: String) {
        showTotal(totalUpdate)
    }

    private fun showExpiredSession(){
        context.alert{
            title = "Advertencia"
            message = "Tú sesión ha expirado. Inicia sesión nuevamente"
            positiveButton("Aceptar"){
                ShowLoginView()
            }
            isCancelable = false
        }.show()
    }

    private fun ShowLoginView(){
        core.closeSession()
    }

    private fun LoadSpacesInRecycler(spaces: ArrayList<String>){
        val filterList: ArrayList<String>?
        filterList = ArrayList()
        var mAdapter = Adapter_espacios()

        dialog2 = Dialog(activity)
        dialog2!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog2!!.setCancelable(false)
        dialog2!!.setContentView(R.layout.dialog_select_product)

        dialog2!!.titulo_.text = "Selecciona un espacio"
        dialog2!!.rclv_productos.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        dialog2!!.rclv_productos.setHasFixedSize(true)
        dialog2!!.rclv_productos.addItemDecoration(SimpleDividerItemDecoration(context))
        mAdapter.Adapter_espacios(spaces){SelectedSpace(it)}
        dialog2!!.rclv_productos.adapter = mAdapter

        dialog2!!.autocomplete_prods.threshold = 1

        dialog2!!.autocomplete_prods.addTextChangedListener(object: TextWatcher {      //Filtrar RecyclerView

            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                val textlength = dialog2!!.autocomplete_prods.text.toString().length
                filterList.clear()

                for (i in spaces.indices){

                    if (textlength <= spaces[i].length) {

                        if (spaces[i].toLowerCase().trim().contains(dialog2!!.autocomplete_prods.text.toString().toLowerCase().trim { it <= ' ' })){
                            filterList.add(spaces[i])
                        }
                    }
                }

                mAdapter.Adapter_espacios(filterList){SelectedSpace(it)}
                mAdapter.notifyDataSetChanged()
            }
        })

        dialog2!!.btn_close_dialog!!.setOnClickListener{
            dialog2!!.dismiss()
        }

        dialog2!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog2!!.show()
    }

    fun SelectedSpace(spacee: String){
        if(dialog2 != null) dialog2!!.dismiss()
        sp_places_ac.setText(spacee)
        space = spacee
    }

    fun CreateDialogRespuesta(msj: String, aux: Int){

        this.builder = AlertDialog.Builder(mview.context)
        val layoutInflater = LayoutInflater.from(mview.context)
        this.viewDialog = layoutInflater.inflate(R.layout.dialog_possitive_message, null)
        this.idTxtMsj = viewDialog!!.findViewById(R.id.idTxtMsj)
        this.img = viewDialog!!.findViewById(R.id.im_circle)

        if(aux == -1) img!!.setImageResource(R.drawable.error)
        else img!!.setImageResource(R.drawable.correct)

        idTxtMsj!!.text = msj

        this.builder!!.setView(viewDialog).setPositiveButton(getString(R.string.aceptar)) { dialog, some->

            //SaveCode(code!!, positionn!!) //Guardamos valores en SharedPreferences

            HideViews(false)
            ClearScreenAndData()
            adapterTagsLocation!!.notifyDataSetChanged()
            dialog.dismiss()
        }
        val dialog: AlertDialog? = builder!!.create()
        dialog!!.setCancelable(false)
        dialog.show()
    }

    fun createDialogTotal(){
        this.builder = AlertDialog.Builder(mview.context)
        val layoutInflater = LayoutInflater.from(mview.context)
        this.viewDialog = layoutInflater.inflate(R.layout.dialog_possitive_message, null)
        this.txt_total = viewDialog!!.findViewById(R.id.txt_total)
        this.builder!!.setView(viewDialog).setPositiveButton(getString(R.string.aceptar)) { dialog, which -> }
    }

    fun showTotal(totalUpdate: String){
        this.txt_total!!.text = totalUpdate
        this.builder!!.show()
        controlateViewTotal()
    }

    fun controlateViewTotal(){
        if(this.txt_total!!.parent != null)
            (this.viewDialog!!.parent as ViewGroup).removeView(txt_total)
        this.viewDialog = activity.layoutInflater.inflate(R.layout.dialog_possitive_message, null)
        this.txt_total = viewDialog!!.findViewById(R.id.txt_total)
        this.builder!!.setView(viewDialog)
    }

    fun setKeyDown(keyCode: Int, event: KeyEvent?){
        if (keyCode == 139 || keyCode == 280 || keyCode == 293){
            if (event!!.repeatCount == 0) readTag()
        }
    }

    fun setKeyUp(keyCode: Int, event: KeyEvent?){
        stopInventory()
    }

    private fun readTag(){
        if(!readSingle){
            //TODO. Multiple read
            if (mReader!!.startInventoryTag()){
                loopFlag = true
                TagThread().start()
            }else{
                mReader!!.stopInventory()
                context.toast("Error de lectura")
            }
        }
        else{
            //todo. individual read
            val strUII: UHFTAGInfo? = mReader!!.inventorySingleTag()
            if(strUII != null){
                val strEPC = strUII.epc
                AddTagReaded(strEPC)
                mSound!!.playSuccess()
            }
            else context.toast("No se leyó ningun tag")
        }
    }

    internal class TagThread : Thread() {
        override fun run() {
            var strTid: String
            var strResult: String
            var res: UHFTAGInfo?
            while (loopFlag) {
                res = mReader!!.readTagFromBuffer()
                if (res != null){
                    strTid = res.tid
                    strResult = if (strTid.isNotEmpty() && strTid != "0000000" +
                            "000000000" && strTid != "000000000000000000000000"){
                        "TID:$strTid\n"
                    }else ""

                    val msg: Message = handlerMSJ!!.obtainMessage()
                    msg.obj = strResult + "EPC:" + res.epc + "@" + res.rssi
                    handlerMSJ!!.sendMessage(msg)
                }
            }
        }
    }

    private fun InitHandler(){
        handlerMSJ = object : Handler() {
            override fun handleMessage(msg: Message) {
                val result = msg.obj.toString() + ""
                val strs = result.split("@").toTypedArray()
                val epc = strs[0].split(":").toTypedArray()

                AddTagReaded(epc[1])
                //playSound(1)
            }
        }
    }

    private fun AddTagReaded(tag: String){
        presenter!!.addTag(tag)
        mSound!!.playSuccess()
    }

    private fun stopInventory(){
        if (loopFlag) {
            loopFlag = false
            if(!mReader!!.stopInventory()) context.toast("Fail stop operation")
        }

        mview.progres_asign_place.visibility = View.VISIBLE
        mview.ly_inicia_lectura_msg.visibility = View.GONE
        presenter!!.sendTags()
    }

    /*private fun initSound() {
        soundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 5)
        soundMap.put(1, soundPool!!.load(context, R.raw.beep, 1))
        soundMap.put(2, soundPool!!.load(context, R.raw.fail, 1))
        am = context.getSystemService(AUDIO_SERVICE) as AudioManager
    }

    fun playSound(id: Int) {
        val audioMaxVolumn = am!!.getStreamMaxVolume(AudioManager.STREAM_MUSIC).toFloat()
        val audioCurrentVolumn = am!!.getStreamVolume(AudioManager.STREAM_MUSIC).toFloat()
        volumnRatio = audioCurrentVolumn / audioMaxVolumn
        try {
            soundPool!!.play(soundMap[id]!!, volumnRatio, volumnRatio, 1, 0, 1f)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }*/

    protected fun playSuccess(){}

    override fun onResume(){
        if(mReader == null){
            mSound = SoundPlay(context)
            iinit()
        }
        super.onResume()
    }

    override fun onStop(){

        mSound!!.Release()

        if (mReader != null){
            if(mReader!!.free()){
                mReader = null
                Log.e("okfree", "free")
            }
            else Log.e("fuckfree", "Fuck free")
        }
        super.onStop()
    }
}

package com.example.pingpongalien.conthea.ModelsRetrofit

data class ResponseLogin (val token: ResponseLoginToken, val user: ResponseLoginUser)

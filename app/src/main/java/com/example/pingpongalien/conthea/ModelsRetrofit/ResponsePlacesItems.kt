package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponsePlacesItems(

        @field:SerializedName("uuid")
        val uuid: String? = null,

        @field:SerializedName("articulo")
        val article: String? = null,

        @field:SerializedName("lote")
        val lote: String? = null,

        @field:SerializedName("almacen")
        val storage: String? = null
)
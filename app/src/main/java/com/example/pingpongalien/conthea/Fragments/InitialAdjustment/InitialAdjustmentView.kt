package com.example.pingpongalien.conthea.Fragments.InitialAdjustment

import android.os.Bundle
import android.os.SystemClock
import android.view.*
import android.widget.TextView
import android.widget.Toast
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import org.jetbrains.anko.toast

class InitialAdjustmentView: ReaderFragment(), InitialAdjustment.View{

    private val TAG = "adjustment"
    var presenter: InitialAdjustment.Presenter? = null

    private var mOperationTime = 0
    private var mPowerLevel: Int = 0
    private var mIsReportRssi = false
    private var mElapsedTick: Long = 0
    private var mTick: Long = 0
    private val SKIP_KEY_EVENT_TIME: Long = 1000
    private var mThread: Thread? = null
    private var mIsAliveThread = false
    private val UPDATE_TIME = 500
    private var m_timeFlag = 0
    private var m_timeSec: Long = 1
    private val MAX_POWER_LEVEL = 300
    //TRUE CONTINUE, FALSE SINGLE
    private var modeRead = true
    private var aux = false
    private val POWER_LEVEL = 200
    lateinit var txt_total: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?{

        val view = inflater.inflate(R.layout.fragment_initial_adjustment, container, false)
        presenter = InitialAdjustmentPresenter(this, context, PreferencesController(context))
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        mPowerLevel = MAX_POWER_LEVEL
        mOperationTime = 0
        mTick = 0
        mElapsedTick = 0
        txt_total = activity.findViewById(R.id.txt_total)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_initial_adjustment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.item_potency -> showPotency()
            R.id.item_type_read -> presenter!!.setModeRead(item.title.toString(), item)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setModeRead(mode: Boolean){
        this.modeRead = mode
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        item.title = name
    }

    override fun SetAmount(amount: Int){
        txt_total.text = amount.toString() //Cantidad de tags leidos
    }

    override fun Unsuccess(error: String){
        context.toast(error)
    }

    private fun showPotency() {
    }

    protected fun getPowerLevel(): Int {
        return mPowerLevel
    }

    override fun initReader(){
    }

    override fun activateReader(){

        // Set Power Level
        setPowerLevel(mPowerLevel)

        // Set Operation Time
        setOperationTime(mOperationTime)
    }

    protected fun setPowerLevel(power: Int) {
        //POTENCIA
        mPowerLevel = power
    }

    protected fun setOperationTime(time: Int) {
        mOperationTime = time
    }

    fun setKeyDown(keyCode : Int, event: KeyEvent){

    }

    fun setKeyUp(keyCode : Int, event: KeyEvent){
    }

    protected fun playSuccess() {
    }
}
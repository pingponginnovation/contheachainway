package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseInitialAdjustment(

        @SerializedName("respuesta")
        val response: String
)
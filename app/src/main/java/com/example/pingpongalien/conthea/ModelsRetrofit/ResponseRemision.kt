package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

class ResponseRemision(
        @field:SerializedName("response")
        val response : String
)
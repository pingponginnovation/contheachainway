package com.example.pingpongalien.conthea.Adapters.ExpandableCatalogue;

import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.example.pingpongalien.conthea.Models.ProductChild;

import java.util.List;

public class ParentCatalogue implements Parent<ProductChild>{

    List<ProductChild> childList;
    String catalogo;
    String lote;

    public ParentCatalogue(List<ProductChild> childList, String catalogo, String lote) {
        this.childList = childList;
        this.catalogo = catalogo;
        this.lote = lote;
    }

    public void setCatalogo(String catalogo) {
        this.catalogo = catalogo;
    }

    public String getCatalogoText() {
        return catalogo;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getLote() {
        return lote;
    }

    @Override
    public List<ProductChild> getChildList() {
        return childList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
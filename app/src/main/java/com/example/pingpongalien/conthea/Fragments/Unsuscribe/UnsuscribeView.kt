package com.example.pingpongalien.conthea.Fragments.Unsuscribe

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Context.AUDIO_SERVICE
import android.media.AudioManager
import android.media.SoundPool
import android.os.*
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView.Companion.mContext
import com.example.pingpongalien.conthea.Adapters.AdapterPotency
import com.example.pingpongalien.conthea.Adapters.AdapterTagsDelete
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.Models.TagsForDelete
import org.jetbrains.anko.toast
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragmentUnsuscribe
import com.example.pingpongalien.conthea.SoundPlay
import com.rscja.deviceapi.RFIDWithUHFUART
import com.rscja.deviceapi.entity.UHFTAGInfo
import kotlinx.android.synthetic.main.dialog_select_potencia.*
import kotlinx.android.synthetic.main.fragment_unsuscribe.*
import java.util.HashMap

class UnsuscribeView : ReaderFragmentUnsuscribe(), Unsuscribe.View{

    private var presenter: Unsuscribe.Presenter? =null
    private var readSingle = false
    private var builder: AlertDialog.Builder? = null
    private var idTxtMsj: TextView? = null
    private var img: ImageView? = null
    private var viewDialog: View? = null
    private var adapterTagsDelete: AdapterTagsDelete? = null
    private var auxiliar = 0
    private var tagsDelete: ArrayList<TagsForDelete>? = null
    private var dialog: Dialog? = null
    //private var am: AudioManager? = null
    //private var volumnRatio = 0f
    //private var soundMap = HashMap<Int, Int>()
    //private var soundPool: SoundPool? = null
    private var mSound: SoundPlay? = null
    companion object{
        private val POWER_LEVEL = 20
        var loopFlag = false
        var mReader: RFIDWithUHFUART? = null
        var handler: Handler? = null
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        val view = inflater!!.inflate(R.layout.fragment_unsuscribe, container, false)
        presenter = UnsuscribePresenter(this, activity)
        iinit()
        mSound = SoundPlay(context)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        InitHandler()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater){
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_detail_verify, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.item_potency -> showPotency()
            R.id.item_type_read -> presenter!!.setModeRead(item.title.toString(), item)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showErrorGetPlaces(error: String) {
        activity.toast(error)
        LY_progressBar_unsuscribe.visibility = View.GONE
        Layout_Recycler.visibility = View.GONE
        LY_Main.visibility = View.VISIBLE
    }

    override fun SetAdapterDelete(arrayDelete: ArrayList<TagsForDelete>){
                                //Here we fill up the adapter with the tags available to delete
        Log.e("Array delete", arrayDelete.size.toString())
        if(arrayDelete.isNotEmpty()){
            tagsDelete = arrayDelete
            auxiliar = 1
            LY_progressBar_unsuscribe.visibility = View.GONE
            Layout_Recycler.visibility = View.VISIBLE
            LY_Main.visibility = View.GONE
            adapterTagsDelete = AdapterTagsDelete(tagsDelete!!, presenter!!, activity, this)
            idRecyclerTags.adapter = adapterTagsDelete
            idRecyclerTags.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        }
        else{
            auxiliar = 0
            LY_progressBar_unsuscribe.visibility = View.GONE
            Layout_Recycler.visibility = View.GONE
            LY_Main.visibility = View.VISIBLE
            activity.toast("No hay productos que mostrar")
        }
    }

    override fun SuccessDeletedTags(msj: String){
        CreateDialogRespuesta(msj, 1)
    }

    override fun UnSuccessDeletedTags(msj: String){
        CreateDialogRespuesta(msj, -1)
    }

    fun CreateDialogRespuesta(msj: String, aux: Int){

        this.builder = AlertDialog.Builder(activity)
        val layoutInflater = LayoutInflater.from(activity)
        this.viewDialog = layoutInflater.inflate(R.layout.dialog_possitive_message, null)
        this.idTxtMsj = viewDialog!!.findViewById(R.id.idTxtMsj)
        this.img = viewDialog!!.findViewById(R.id.im_circle)

        if(aux == -1) img!!.setImageResource(R.drawable.error)
        else img!!.setImageResource(R.drawable.correct)

        idTxtMsj!!.text = msj

        this.builder!!.setView(viewDialog).setPositiveButton(getString(R.string.aceptar)) { _ , _ ->

            val ft = fragmentManager.beginTransaction()
            if(Build.VERSION.SDK_INT >= 26) ft.setReorderingAllowed(false)
            ft.detach(this).attach(this).commit()
        }

        this.builder!!.setCancelable(false)
        this.builder!!.show()
    }

    override fun setModeRead(mode: Boolean){
        this.readSingle = mode
    }

    private fun showPotency(){
        val arrayPotency = arrayListOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                21,22,23,24,25,26,27,28,29,30)
        dialog = Dialog(activity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.dialog_select_potencia)

        dialog!!.rclv_potencias.addItemDecoration(SimpleDividerItemDecoration(activity))
        dialog!!.rclv_potencias.apply{
            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            setHasFixedSize(true)
            adapter = AdapterPotency(arrayPotency){Setpotencia(it)}
        }

        dialog!!.btn_close_dialog!!.setOnClickListener{
            dialog!!.dismiss()
        }
        dialog!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog!!.show()
    }

    private fun Setpotencia(num: Int){
        if(dialog != null) dialog!!.dismiss()
        if(mReader!!.setPower(num)) activity.toast("Potencia ajustada correctamente a: $num")
        else activity.toast("Error en establecer potencia")
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        item.title = name
    }

    override fun initReader(){}
    override fun activateReader(){}

    fun iinit(){
        try {
            mReader = RFIDWithUHFUART.getInstance()
            if (mReader != null){

                if (mReader != null) {
                    someTask().MyCustomTask(activity)
                    someTask().execute()
                }
            }
        } catch (e: Exception) {
            activity.toast(e.message!!)
        }
    }

    class someTask: AsyncTask<Void, Void, Boolean>() {

        var mypDialog: ProgressDialog? = null

        override fun doInBackground(vararg params: Void?): Boolean?{
            return mReader!!.init()
        }

        fun MyCustomTask(activity: Context) {
            mContext = activity
        }

        override fun onPreExecute() {
            super.onPreExecute()

            mypDialog = ProgressDialog(mContext)
            mypDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            mypDialog!!.setMessage("Iniciando RFID...")
            mypDialog!!.setCanceledOnTouchOutside(false)
            mypDialog!!.show()
            //mContext!!.progressDialog("Please wait a minute.", "Downloading…")
            //mContext!!.indeterminateProgressDialog("Fetching the data…")
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            mypDialog!!.cancel()
            if (!result!!) {
                Toast.makeText(mContext, "init fail", Toast.LENGTH_SHORT).show()
            }
            else{
                try {
                    mReader!!.power = POWER_LEVEL
                    Log.e("Power", mReader!!.power.toString())
                }catch(e: Exception){}
            }
        }
    }

    fun setKeyDown(keyCode : Int, event: KeyEvent){
        Log.e("key code", keyCode.toString())
        /*if((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT
                || keyCode == KeyEvent.KEYCODE_F7 || keyCode == KeyEvent.KEYCODE_F8)
                && (event.repeatCount == 0)) readTag()*/

        if (keyCode == 139 || keyCode == 280 || keyCode == 293){
            if(event.repeatCount == 0) readTag()
        }
    }

    fun setKeyUp(keyCode : Int, event: KeyEvent){
        LY_progressBar_unsuscribe.visibility = View.VISIBLE
        LY_Main.visibility = View.GONE
        Layout_Recycler.visibility = View.GONE
        stopInventory()
    }

    private fun readTag(){
        if(!readSingle){
            //TODO. Multiple read
            if (mReader!!.startInventoryTag()){
                loopFlag = true
                TagThread().start()
            }else{
                mReader!!.stopInventory()
                activity.toast("Error de lectura")
            }
        }
        else{
            //todo. individual read
            val strUII: UHFTAGInfo? = mReader!!.inventorySingleTag()
            if(strUII != null){
                val strEPC = strUII.epc
                AddTagReaded(strEPC)
                mSound!!.playSuccess()
            }
            else activity.toast("No se leyó ningun tag")
        }
    }

    internal class TagThread : Thread() {
        override fun run() {
            var strTid: String
            var strResult: String
            var res: UHFTAGInfo?
            while (loopFlag) {
                res = mReader!!.readTagFromBuffer()
                if (res != null){
                    strTid = res.tid
                    strResult = if (strTid.isNotEmpty() && strTid != "0000000" +
                            "000000000" && strTid != "000000000000000000000000"){
                        "TID:$strTid\n"
                    }else ""

                    val msg: Message = handler!!.obtainMessage()
                    msg.obj = strResult + "EPC:" + res.epc + "@" + res.rssi
                    handler!!.sendMessage(msg)
                }
            }
        }
    }

    private fun InitHandler(){
        handler = object : Handler() {
            override fun handleMessage(msg: Message) {
                val result = msg.obj.toString() + ""
                val strs = result.split("@").toTypedArray()
                val epc = strs[0].split(":").toTypedArray()

                AddTagReaded(epc[1])
            }
        }
    }

    private fun AddTagReaded(tag: String){
        presenter!!.addTag(tag)
        mSound!!.playSuccess()
    }

    private fun stopInventory(){
        if (loopFlag) {
            loopFlag = false
            if(!mReader!!.stopInventory()) activity.toast("Fail stop operation")
        }
        presenter!!.sendTags()
    }

    protected fun playSuccess(){}

    override fun onResume(){
        if(mReader == null){
            iinit()
            mSound = SoundPlay(context)
        }
        super.onResume()
    }

    override fun onStop(){

        mSound!!.Release()

        if (mReader != null){
            if(mReader!!.free()){
                mReader = null
                Log.e("okfree", "free")
            }
            else Log.e("fuckfree", "Fuck free")
        }
        super.onStop()
    }
}
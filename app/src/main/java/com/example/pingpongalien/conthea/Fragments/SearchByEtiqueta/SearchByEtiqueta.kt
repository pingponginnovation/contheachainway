package com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta

import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalog
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseEtiquetas
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseGetPlaces

interface SearchByEtiqueta {

    interface View{
        fun successTags(tagsSinUbicacion: ArrayList<ParentUbication>?, tagsConUbicacion: ArrayList<ParentUbication>?)
        fun errorTags(resp: String)
        fun cleanTags()
    }

    interface Presenter{
        //MODEL
        fun addTag(tag: String)
        fun sendTags()
        fun cleanTags()
        //VIEW
        fun successTags(tagsSinUbicacion: ArrayList<ParentUbication>?, tagsConUbicacion: ArrayList<ParentUbication>?)
        fun errorTags(resp: String)
    }

    interface Model{
        fun addTag(tag: String)
        fun sendTags()
        fun successTags(response: ResponseEtiquetas?)
        fun errorTags(resp: String)
        fun cleanTags()
    }
}
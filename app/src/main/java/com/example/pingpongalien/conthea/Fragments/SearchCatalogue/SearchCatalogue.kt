package com.example.pingpongalien.conthea.Fragments.SearchCatalogue

import com.example.pingpongalien.conthea.Adapters.ExpandableCatalogue.ParentCatalogue
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalog
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDetailTag

/**
 * Created by AOR on 5/12/17.
 */
interface SearchCatalogue {

    interface View{
        fun showErrorUnsuccessCatalog(error : String)
        fun setMessage(message : String)
        fun visibilitySearch(state : Boolean)
        fun visibilityTextDate(state : Boolean)
        fun visibilityRfid()
        fun setStartDate(date : String)
        fun setFinishDate( date : String)
        fun playSuccess()
        fun SuccessCatalogue()
        fun setProductos(aProducts : ArrayList<ParentCatalogue>)
    }

    interface Presenter{
        //MODEL
        fun searchProduct(search : String, typeSearch : Int)
        fun setComponentsCorrect(typeSearch : Int)
        fun searchProductByExpiration(startDate : String, finishDate : String)
        fun formatDate(year : Int, month : Int, day : Int, type : Boolean)
        fun shouldSearch(uuid : String, typeSearch : Int)
        fun getCatalogue(catalogo: String, name: String, lote: String)

        //VIEW
        fun unsuccessGetCataloge(error : String)
        fun setMessage(message : String)
        fun visibilitySearch(state : Boolean)
        fun visibilityTextDate(state : Boolean)
        fun setStartDate(date : String)
        fun setFinishDate( date : String)
        fun playSuccess()
        fun visibilityRfid()
        fun SuccessCatalogue()
        fun setProductos(aProducts : ArrayList<ParentCatalogue>)
    }

    interface Model{
        fun successGetCataloge(responseCatalog: ResponseCatalog)
        fun unsuccessGetCataloge(error : String)
        fun searchProduct(search : String, typeSearch : Int)
        fun setComponentsCorrect(typeSearch : Int)
        fun searchProductByExpiration(startDate : String, finishDate : String)
        fun formatDate(year : Int, month : Int, day : Int, type : Boolean)
        fun shouldSearch(uuid : String, typeSearch : Int)
        fun successDetailTag(responseCatalog: ResponseDetailTag)
        fun unsuccessDetailTag(error : String)
        fun getCatalogue(catalogo: String, name: String, lote: String)
    }
}
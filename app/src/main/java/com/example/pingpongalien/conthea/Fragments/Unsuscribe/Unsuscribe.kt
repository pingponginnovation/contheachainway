package com.example.pingpongalien.conthea.Fragments.Unsuscribe

import android.view.MenuItem
import com.example.pingpongalien.conthea.Models.TagsForDelete
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDelete
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseGetPlaces

interface Unsuscribe {

    interface View{
        fun SetAdapterDelete(arrayDelete: ArrayList<TagsForDelete>)
        fun showErrorGetPlaces(error : String)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun SuccessDeletedTags(msj: String)
        fun UnSuccessDeletedTags(msj: String)
    }

    interface Presenter{
        //MODEL
        fun sendTags()
        fun addTag(tag : String)
        fun setModeRead(nameItem : String, item: MenuItem)
        fun SendTagsDelete(tags: ArrayList<TagsForDelete>)

        //VIEW
        fun SetAdapterDelete(arrayDelete: ArrayList<TagsForDelete>)
        fun unSuccessRequestPlaces(error : String)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun SuccessDeletedTags(msj: String)
        fun UnSuccessDeletedTags(msj: String)
    }

    interface Model{
        fun successRequestPlaces(response : ResponseDelete)
        fun unSuccessRequestGetPlaces(error : String)
        fun sendTags()
        fun addTag(tag : String)
        fun setModeRead(nameItem : String, item: MenuItem)
        fun SendTagsDelete(tags: ArrayList<TagsForDelete>)
        fun SuccessDeletedTags(msj: String)
        fun UnSuccessDeletedTags(msj: String)
    }
}
package com.example.pingpongalien.conthea.Adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.pingpongalien.conthea.Fragments.VerifyInventory.VerifyInventoryModel
import com.example.pingpongalien.conthea.Fragments.VerifyInventory.VerifyInventoryView
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseProducts
import com.example.pingpongalien.conthea.R

class Adapter_productos: RecyclerView.Adapter<Adapter_productos.ViewHolder>(){

    private var productos: List<ResponseProducts>? = null
    private var actividad: VerifyInventoryView? = null

    fun Adapter_productos(prods: List<ResponseProducts>, actividad: VerifyInventoryView){
        productos = prods
        this.actividad = actividad
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val titulo = holder.itemView.findViewById(R.id.txt_nombre) as TextView

        titulo.text = productos!![position].nombre!!
        holder.itemView.setOnClickListener{
            actividad!!.UpdatePlaces(productos!![position].id!!, productos!![position].nombre!!)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_select_product, parent, false))
    }

    override fun getItemCount(): Int {
        return productos!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}

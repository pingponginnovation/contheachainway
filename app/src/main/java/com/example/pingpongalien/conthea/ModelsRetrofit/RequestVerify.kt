package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class RequestVerify(
		@field:SerializedName("dispositivo")
		val id_dispositivo: String? = null,

		@field:SerializedName("espacios_id")
		val espaciosId: Int? = null,

		@field:SerializedName("productos_id")
		val productosId: Int? = null,

		@field:SerializedName("lote")
		val lote: String? = null,

		@field:SerializedName("uuid")
		val uuid: List<String?>? = null
)
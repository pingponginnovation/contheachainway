package com.example.pingpongalien.conthea;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;
import com.rscja.deviceapi.RFIDWithUHFUART;

public abstract class ReaderActivity extends AppCompatActivity{

	// ------------------------------------------------------------------------
	// Member Variable
	// ------------------------------------------------------------------------

	private static final String TAG = ReaderActivity.class.getSimpleName();
	protected int mView;
	// ------------------------------------------------------------------------
	// Constructor
	// ------------------------------------------------------------------------

	public ReaderActivity() {
		super();
		mView = 0;
	}

	// ------------------------------------------------------------------------
	// Activit Event Handler
	// ------------------------------------------------------------------------

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(mView);

		// Initialize Widgets
		initWidgets();
		// Disable All Widgets
		enableWidgets(false);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.e("start", "start");
		onInitReader(false);
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// ------------------------------------------------------------------------
	// Reader Event Handler
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// Override Widgets Control Methods
	// ------------------------------------------------------------------------

	// Initialize Activity Widgets
	protected abstract void initWidgets();

	// Eanble Activity Widgets
	protected abstract void enableWidgets(boolean enabled);

	// Initialize Reader
	protected abstract void initReader();

	// Activated Reader
	protected abstract void activateReader();

	// Begin Initialize Reader
	private void onInitReader(final boolean enabled) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Log.e("Init", "RUN");
				//initReader();
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						//activateReader();
						if (enabled) enableWidgets(true);
					}

				});
			}

		}).start();
	}
}

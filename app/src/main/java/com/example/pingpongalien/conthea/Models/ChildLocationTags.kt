package com.example.pingpongalien.conthea.Models

data class ChildLocationTags(
        val catalogo: String,
        val uuid: String?,
        val lote: String?,
        val almacen: String?,
        var selected: Boolean
)
package com.example.pingpongalien.conthea.Models

data class TagsForDelete(val uuid: String?,
                         val article: String?,
                         val lote: String?,
                         val caducidad: String?,
                         val almacen: String?,
                         var selected: Boolean)
package com.example.pingpongalien.conthea.Fragments.SearchByUbication

import android.content.Context
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseGetPlacesBody

class SearchByUbicationPresenter(viewCataloge : SearchByUbication.View, context: Context) : SearchByUbication.Presenter {

    private var view: SearchByUbication.View? = null
    private var model: SearchByUbication.Model? = null

    init {
        this.view = viewCataloge
        model = SearchByUbicationModel(this, context)
    }

    override fun getPlaces(from: String) {
        model!!.requestPlaces(from)
    }

    override fun setPlaces(aPlaces: ArrayList<ResponseGetPlacesBody>) {
        view!!.setPlaces(aPlaces)
    }

    override fun unSuccessRequestPlaces(error: String) {
        view!!.unSuccessRequestPlaces(error)
    }

    override fun getProducts(space: Int) {
        model!!.getProducts(space)
    }

    override fun setProducts(productos: ArrayList<ParentUbication>){
        view!!.setProducts(productos)
    }

    override fun errorProductos(error: String) {
        view!!.errorProductos(error)
    }
}
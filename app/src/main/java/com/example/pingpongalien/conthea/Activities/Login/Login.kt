package com.example.pingpongalien.conthea.Activities.Login.Login

import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseErrorAuth
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseLogin
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

interface Login{

    interface View{
        fun makeLogin()
        fun showErrorLogin(message: String)
        fun showCore()
        fun verifyStateSession()
        fun ErrorServer(msj: String)
    }

    interface Presenter{
        fun makeLogin(user: String, password: String, stateSession: Boolean, preferences: PreferencesController, token: String)
        fun successRequestLogin()
        fun errorRequestLogin(message: String)
        fun verifyStateSession(preferences: PreferencesController)
        fun ErrorServer(msj: String)
    }

    interface Model{
        fun makeLogin(user: String, password: String, stateSession: Boolean, preferences: PreferencesController, token: String)
        fun successRequestLogin(responseSuccess: ResponseLogin, stateSession: Boolean)
        fun errorRequestLogin(responseError: ResponseErrorAuth)
        fun saveKeepSession(keepSession: Boolean, preferences: PreferencesController)
        fun verifyStateSession(preferences: PreferencesController)
        fun ErrorServer(msj: String)
        fun ErrorResponseLogin(resp: String)
    }
}
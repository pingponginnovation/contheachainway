package com.example.pingpongalien.conthea.Activities.Core

import android.os.Bundle
import android.view.*
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView.Companion.nameFragment
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import kotlinx.android.synthetic.main.home_screen_shorcuts.view.*

class HomeScreenView(private val core: CoreView): ReaderFragment(){

    private lateinit var mvi: View
    private var preferences: PreferencesController? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mvi = inflater!!.inflate(R.layout.home_screen_shorcuts, container, false)
        nameFragment = ""
        HandleClicks()
        return mvi
    }

    private fun HandleClicks(){
        mvi.btn_shortcut_asignar_lugar.setOnClickListener{
            core.asignarLugar()
        }
        mvi.btn_shortcut_busquedas.setOnClickListener{
            core.busquedasView()
        }
        mvi.btn_shortcut_salidas.setOnClickListener{
            core.ordenesEntradaSalida(true)
        }
        mvi.btn_shortcut_inventario_ciclico.setOnClickListener{
            core.inventarioCiclico()
        }
    }

    override fun activateReader(){}
    override fun initReader(){}
}

package com.example.pingpongalien.conthea.ModelsRoom

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import java.util.*
import kotlin.collections.ArrayList

@Dao
interface DaoInventario{
    @Insert
    fun InsertTags(inventarioTags: RoomInventario)

    @Query("DELETE FROM inventario_ciclico WHERE lote_number LIKE :lote AND almacen_number LIKE :almacen AND catalogo_number LIKE :catalogo")
    fun deleteFromDB(lote: String, almacen: String, catalogo: String)

    @Query("SELECT EXISTS (SELECT * FROM inventario_ciclico)")
    fun isExists(): Boolean

    @Query("SELECT * FROM inventario_ciclico WHERE lote_number LIKE :lote AND almacen_number LIKE :almacen AND catalogo_number LIKE :catalogo")
    fun GetTagsSaved(lote: String, almacen: String, catalogo: String): List<RoomInventario>
}
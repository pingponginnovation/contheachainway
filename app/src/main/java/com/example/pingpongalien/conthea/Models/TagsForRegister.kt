package com.example.pingpongalien.conthea.Models

data class TagsForRegister(val uuid: String?, val article: String?, val lote: String?, val almacen: String?, var selected: Boolean)
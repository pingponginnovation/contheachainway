package com.example.pingpongalien.conthea.Adapters

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.example.pingpongalien.conthea.Activities.DetailOrder.DetailOrderView
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseEspacios
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderProduct
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_products.view.*

/**
 * Created by AOR on 28/11/17.
 */
class AdapterProducts(var responseOrderProduct: ArrayList<ResponseOrderProduct>, var actividad: Activity,
                      val cliente: String, val actOrderView: DetailOrderView, val loteNull: Boolean,
    val listener: (ArrayList<ResponseEspacios>) -> Unit): RecyclerView.Adapter<AdapterProducts.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(responseOrderProduct[position],
            actividad, cliente, actOrderView, loteNull, listener)

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){

        var tags: StringBuilder = StringBuilder()

        fun bind(responseOrderProduct: ResponseOrderProduct, miactividad: Activity, cliente: String,
        actOrderView: DetailOrderView, loteNull: Boolean, listener: (ArrayList<ResponseEspacios>) -> Unit) = with(itemView){

            if(loteNull) actOrderView.ShowDialogErrorLotes()

            tags.setLength(0)
            this.txt_product.text = responseOrderProduct.nombre
            this.txt_amount.text = "${responseOrderProduct.cantidadRegistrada} / ${responseOrderProduct.cantidad}"
            this.txt_customer.text = cliente

            //Log.e("ordendetalle", responseOrderProduct.toString())

            if(responseOrderProduct.almacen.isNullOrEmpty())
                this.txt_almacen.text = "Sin almacén"
            else this.txt_almacen.text = responseOrderProduct.almacen

            if(responseOrderProduct.description.isNullOrEmpty())
                this.txt_descripcion.text = "Sin descripción"
            else this.txt_descripcion.text = responseOrderProduct.description

            responseOrderProduct.tagsLoteSalida!!.forEach { tag ->
                tags.append("${tag!!.lote} , ")
            }

            if(tags.isEmpty()) tags.append("Sin lotes disponibles")

            this.txt_lote.text = tags

            if(responseOrderProduct.validate && responseOrderProduct.orderReady) this.im_state.setImageResource(R.drawable.ic_validate)
            else{

                this.im_state.setImageResource(R.drawable.ic_invalidate)

                //Funcion para validar del lado del backend cuando una orden está siendo consultada y no le aparezca la misma orden a otra persona.
                val retrofit = RetrofitController(context)
                val gson = Gson()
                val n = gson.toJson(responseOrderProduct.origenId)
                retrofit.sendOrderID(n, miactividad)
                //Toast.makeText(miactividad, "Desde bin: ${msj}", Toast.LENGTH_LONG).show()
            }

            btn_spaces.setOnClickListener{
                if(!responseOrderProduct.espacios.isNullOrEmpty()) listener(responseOrderProduct.espacios)
            }
        }
    }

    fun swap(aResponseOrderProduct: ArrayList<ResponseOrderProduct>?, clear: Int){
        if (aResponseOrderProduct == null || aResponseOrderProduct.size == 0) return
        if (responseOrderProduct.size > 0) responseOrderProduct.clear()
        responseOrderProduct.addAll(aResponseOrderProduct)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_products, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = responseOrderProduct.size
}
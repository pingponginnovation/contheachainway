package com.example.pingpongalien.conthea.Fragments.SetPlace

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.Models.TagsChangeLocation
import com.example.pingpongalien.conthea.Models.TagsChangeLocation2
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponsePlacesItems

/**
 * Created by AOR on 5/12/17.
 */
class SetPlacePresenter(viewPlace : SetPlace.View, context: Context) : SetPlace.Presenter {

    var view: SetPlace.View? = null
    var model: SetPlace.Model? = null

    init {
        this.view = viewPlace
        this.model = SetPlaceModel(this, context)
    }

    //MODEL
    override fun getPlaces(from: String) {
        model!!.requestPlaces(from)
    }

    override fun setidPlace(position: Int) {
        model!!.setidPlace(position)
    }

    override fun getName(position: Int) {
        model!!.getName(position)
    }

    override fun sendTags() {
        model!!.sendTags()
    }

    override fun addTag(tag: String) {
        model!!.addTag(tag)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        model!!.setModeRead(nameItem, item)
    }

    override fun SendCahngeLocationTags(tags: ArrayList<String>, code: String) {
        model!!.SendCahngeLocationTags(tags, code)
    }

    //VIEW
    override fun setPlaces(aPlaces: ArrayList<String>) {
        view!!.setPlaces(aPlaces)
    }

    override fun unSuccessRequestPlaces(error: String) {
        view!!.showErrorGetPlaces(error)
    }

    override fun setNamePlace(name: String) {
        view!!.setNamePlace(name)
    }

    override fun setTotalUpdate(totalUpdate: String) {
        view!!.setTotalUpdate(totalUpdate)
    }

    override fun setModeRead(mode: Boolean) {
        view!!.setModeRead(mode)
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        view!!.setItemNameMenu(name, item)
    }

    override fun SetAdapterLocationTags(tags: ArrayList<TagsChangeLocation2>, tags2: ArrayList<ResponsePlacesItems>){
        view!!.SetAdapterLocationTags(tags, tags2)
    }

    override fun SuccessChangeLocationTags(msj: String) {
        view!!.SuccessChangeLocationTags(msj)
    }

    override fun UnSuccessChangeLocationTags(msj: String) {
        view!!.UnSuccessChangeLocationTags(msj)
    }
}
package com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.*
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import com.example.pingpongalien.conthea.Adapters.AdapterByEtiquetas
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.Adapter
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta.SearchByEtiquetaView.Companion.receiverNoSpace
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import kotlinx.android.synthetic.main.fragment_productos_sin_espacio.view.*

class ProductosSinEspacio: ReaderFragment(), SearchByEtiqueta.View{

    private lateinit var mvi: View
    private var tagsSinUbicacion: ArrayList<ParentUbication>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mvi = inflater!!.inflate(R.layout.fragment_productos_sin_espacio, container, false)
        ListenBroadcast()
        RegisterBroadcast()
        return mvi
    }

    private fun ListenBroadcast(){
        Log.e("Register22", "registered broadcast22")
        receiverNoSpace = object : BroadcastReceiver() {
            override fun onReceive(contxt: Context?, intent: Intent?) {
                when (intent?.action) {
                    "SIN_UBICACION" -> {

                        if(intent.getSerializableExtra("TAGS") != null){
                            val tags = intent.getSerializableExtra("TAGS")!! as ArrayList<ParentUbication>

                            //if(!tagsSinUbicacion.isNullOrEmpty()) tagsSinUbicacion?.clear()

                            Log.e("tags1sinespacio", tags.toString())
                            tagsSinUbicacion = tags
                            setData()
                        }

                        val clear = intent.getBooleanExtra("CLEAR", false)
                        if(clear) cleanTags()

                        /*tagsSinUbicacion?.forEach {
                            it.childList.forEach{item ->
                                Log.e("uuid", item.lote)
                            }
                        }*/
                    }
                }
            }
        }
    }

    private fun RegisterBroadcast(){
        LocalBroadcastManager.getInstance(activity).registerReceiver(receiverNoSpace, IntentFilter("SIN_UBICACION"))
    }

    private fun UnregisterBroadcast(){
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(receiverNoSpace)
        receiverNoSpace = null
    }

    private fun setData(){
        Log.e("TagsEnData", tagsSinUbicacion.toString())
        val adapterProds = tagsSinUbicacion?.let{AdapterByEtiquetas(context, it)}
        mvi.rclv_sin_espacio.addItemDecoration(SimpleDividerItemDecoration(context))
        mvi.rclv_sin_espacio.apply{
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = adapterProds
        }
    }

    override fun cleanTags(){
        //if(!tagsSinUbicacion.isNullOrEmpty()) tagsSinUbicacion?.clear()
        setData()
    }

    override fun onResume() {
        super.onResume()
        if(receiverNoSpace == null){
            Log.e("res", "resume")
            ListenBroadcast()
            RegisterBroadcast()
        }
    }

    override fun onStop() {
        super.onStop()
        Log.e("stop", "stop")
        if(receiverNoSpace != null) UnregisterBroadcast()
    }

    override fun initReader() {}
    override fun activateReader() {}
    override fun successTags(tagsSinUbicacion: ArrayList<ParentUbication>?, tagsConUbicacion: ArrayList<ParentUbication>?) {}
    override fun errorTags(resp: String) {}
}

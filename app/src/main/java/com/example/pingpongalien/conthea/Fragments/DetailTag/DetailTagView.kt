package com.example.pingpongalien.conthea.Fragments.DetailTag

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Context.AUDIO_SERVICE
import android.media.AudioManager
import android.media.SoundPool
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.Toast
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView.Companion.mContext
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Activities.Login.Login.LoginView
import com.example.pingpongalien.conthea.Adapters.AdapterPotency
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDetailTagDetail
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import com.example.pingpongalien.conthea.SoundPlay
import com.rscja.deviceapi.RFIDWithUHFUART
import com.rscja.deviceapi.entity.UHFTAGInfo
import kotlinx.android.synthetic.main.dialog_select_potencia.*
import kotlinx.android.synthetic.main.fragment_detail_tag.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.util.*

class DetailTagView(val core: CoreView): ReaderFragment(), DetailTag.View {

    val TAG = "DetailTagView"

    var presenter: DetailTag.Presenter? =null
    var preferences: PreferencesController? = null
    private var readSingle = true
    private var tags_leidos = false
    //private var am: AudioManager? = null
    //private var volumnRatio = 0f
    //private var soundMap = HashMap<Int, Int>()
    //private var soundPool: SoundPool? = null
    private var dialog: Dialog? = null
    private var mSound: SoundPlay? = null
    companion object{
        var loopFlag = false
        var mReader: RFIDWithUHFUART? = null
        var handler: Handler? = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View?{ val view = inflater.inflate(R.layout.fragment_detail_tag, container, false)

        preferences = PreferencesController(context)
        mSound = SoundPlay(context)
        presenter = DetailTagPresenter(this, context)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iinit()
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_detail_tag, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.item_potency -> showPotency()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showErrorDetailTag(error: String) {
        if(error == "Unauthorized") showExpiredSession()
        tags_leidos = false
        LY_progress.visibility = View.GONE
        LY_info.visibility = View.VISIBLE

        context.toast(error)
        SetNoInformation()
    }

    override fun setDetailTag(detailTag: ResponseDetailTagDetail){
        tags_leidos = false
        LY_progress.visibility = View.GONE
        LY_info.visibility = View.VISIBLE

        txt_catalogue.text = detailTag.catalogo
        txt_description.text = detailTag.descripcion
        txt_lote.text = detailTag.lote
        txt_expiration.text = detailTag.caducidad
        txt_cost.text = detailTag.costo.toString()
        txt_currency.text = detailTag.moneda
        txt_ubication.text = detailTag.ubicacion
        almacen_info.text = detailTag.almacen
    }

    private fun showExpiredSession(){
        context.alert{
            title = "Advertencia"
            message = "Tú sesión ha expirado. Inicia sesión nuevamente"
            positiveButton("Aceptar"){
                ShowLoginView()
            }
            isCancelable = false
        }.show()
    }

    private fun ShowLoginView(){
        core.closeSession()
    }

    private fun SetNoInformation(){
        txt_catalogue.text = getString(R.string.sin_detalle)
        txt_description.text = getString(R.string.sin_detalle)
        txt_lote.text = getString(R.string.sin_detalle)
        txt_expiration.text = getString(R.string.sin_detalle)
        txt_cost.text = getString(R.string.sin_detalle)
        txt_currency.text = getString(R.string.sin_detalle)
        txt_ubication.text = getString(R.string.sin_detalle)
        almacen_info.text = "Sin información"
    }

    override fun initReader() {}

    fun iinit(){
        try {
            mReader = RFIDWithUHFUART.getInstance()
            if (mReader != null){

                if (mReader != null) {
                    someTask().MyCustomTask(context)
                    someTask().execute()
                }
            }
        } catch (e: Exception) {
            context.toast(e.message!!)
        }
    }

    class someTask: AsyncTask<Void, Void, Boolean>() {

        var mypDialog: ProgressDialog? = null

        override fun doInBackground(vararg params: Void?): Boolean?{
            return mReader!!.init()
        }

        fun MyCustomTask(context: Context) {
            mContext = context
        }

        override fun onPreExecute() {
            super.onPreExecute()

            mypDialog = ProgressDialog(mContext)
            mypDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            mypDialog!!.setMessage("Iniciando RFID...")
            mypDialog!!.setCanceledOnTouchOutside(false)
            mypDialog!!.show()
            //mContext!!.progressDialog("Please wait a minute.", "Downloading…")
            //mContext!!.indeterminateProgressDialog("Fetching the data…")
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            mypDialog!!.cancel()
            if (!result!!) {
                Toast.makeText(mContext, "init fail", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun showPotency(){

        val arrayPotency = arrayListOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                21,22,23,24,25,26,27,28,29,30)
        dialog = Dialog(activity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.dialog_select_potencia)

        dialog!!.rclv_potencias.addItemDecoration(SimpleDividerItemDecoration(context))
        dialog!!.rclv_potencias.apply{
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            setHasFixedSize(true)
            adapter = AdapterPotency(arrayPotency){Setpotencia(it)}
        }

        dialog!!.btn_close_dialog!!.setOnClickListener{
            dialog!!.dismiss()
        }
        dialog!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog!!.show()
    }

    private fun Setpotencia(num: Int){
        if(dialog != null) dialog!!.dismiss()
        if(mReader!!.setPower(num)) context.toast("Potencia ajustada correctamente a: $num")
        else context.toast("Error en establecer potencia")
    }

    fun setKeyDown(keyCode : Int, event: KeyEvent){
        if (keyCode == 139 || keyCode == 280 || keyCode == 293){
            if (event.repeatCount == 0) readTag()
        }
    }

    fun setKeyUp(keyCode : Int, event: KeyEvent){
        LY_progress.visibility = View.VISIBLE
        LY_info.visibility = View.GONE
        stopInventory()
    }

    private fun readTag(){
        if(!readSingle){
            //TODO. Multiple read
            if (mReader!!.startInventoryTag()){
                loopFlag = true
                TagThread().start()
            }else{
                mReader!!.stopInventory()
                context.toast("Error de lectura")
            }
        }
        else{
            //todo. individual read
            val strUII: UHFTAGInfo? = mReader!!.inventorySingleTag()
            if(strUII != null){
                val strEPC = strUII.epc
                tags_leidos = true
                presenter!!.searchDetailTag(strEPC)
                mSound!!.playSuccess()
            }
            else context.toast("No se leyó ningun tag")
        }
    }

    internal class TagThread : Thread() {
        override fun run() {
            var strTid: String
            var strResult: String
            var res: UHFTAGInfo?
            while (loopFlag) {
                res = mReader!!.readTagFromBuffer()
                if (res != null){
                    strTid = res.tid
                    strResult = if (strTid.isNotEmpty() && strTid != "0000000" +
                            "000000000" && strTid != "000000000000000000000000"){
                        "TID:$strTid\n"
                    }else ""

                    val msg: Message = handler!!.obtainMessage()
                    msg.obj = strResult + "EPC:" + res.epc + "@" + res.rssi
                    handler!!.sendMessage(msg)
                }
            }
        }
    }

    private fun stopInventory() {
        if (loopFlag) {
            loopFlag = false
            if(!mReader!!.stopInventory()) context.toast("Fail stop operation")
        }

        if(!tags_leidos){
            SetNoInformation()
            LY_progress.visibility = View.GONE
            LY_info.visibility = View.VISIBLE
            context!!.toast("No se leyó ningún Tag")
        }
    }

    override fun activateReader(){}

    override fun onResume(){
        if(mReader == null){
            iinit()
            mSound = SoundPlay(context)
        }
        super.onResume()
    }

    override fun onStop(){

        mSound!!.Release()

        if (mReader != null){
            if(mReader!!.free()){
                mReader = null
                Log.e("okfree", "free")
            }
            else Log.e("fuckfree", "Fuck free")
        }
        super.onStop()
    }
}

package com.example.pingpongalien.conthea.Activities.Core

import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrder

interface Core {

    interface View{
        fun showLoginView()
        fun setNameUser(name: String, lastName: String)
    }

    interface Presenter{
        fun showLoginView()
        fun setNameUser(name: String, lastName: String)
        fun getUser()
    }

    interface Model{
        fun getUser()
    }
}
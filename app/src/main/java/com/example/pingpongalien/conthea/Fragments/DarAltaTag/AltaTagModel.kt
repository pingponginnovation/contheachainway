package com.example.pingpongalien.conthea.Fragments.InitialAdjustment

import android.content.Context
import android.util.Log
import android.view.MenuItem
import com.example.pingpongalien.conthea.Models.TagsForRegister
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponsRegisterTags
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseAltaTag
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseAltaTagItems
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import com.google.gson.Gson

class AltaTagModel(presenter: AltaTag.Presenter, context: Context, preferences: PreferencesController): AltaTag.Model{

    data class RequestInitialAdjustment(val tags: ArrayList<String>)

    val presenter: AltaTag.Presenter = presenter
    private var misTags = arrayListOf<String>()
    val appContext: Context = context
    val arrayTagsForRegister = ArrayList<TagsForRegister>() //Para recibir los tags leídos por primera vez y ver cuales van a dar de alta
    val arrayTagsSelected = arrayListOf<TagsForRegister>() //Para guardar los tags que son seleccionados para registrar
    val arrayTagsNotRegistered = arrayListOf<String>() //Para guardar los tags que no se registraron porque estan en alguna orden de salida

    override fun AddTag(tag: String){
        if(misTags.isEmpty()) misTags.add(tag)
        else if(!misTags.contains(tag)) misTags.add(tag)
    }

    override fun SendToRetrofit(){

        //Log.e("retrofit", misTags.toString())
        val getTags = RequestInitialAdjustment(misTags)
        val gson = Gson()
        val get = gson.toJson(getTags)
        val retrofit = RetrofitController(appContext)
        retrofit.GetTagAlta(get, this)
    }

    override fun Success(response: ResponseAltaTag){
        ClearArrayTags()
        if(response.respuesta != null){
            FillTagsForRegister(response.respuesta, false)
            presenter.SetAdapterRegister(arrayTagsForRegister, arrayTagsNotRegistered, "No")
        }

        if(!misTags.isEmpty()) misTags.clear()
    }

    override fun Unsuccess(error: String) {
        presenter.Unsuccess(error)
    }

    fun ClearArrayTags(){
        if(!arrayTagsForRegister.isEmpty()) arrayTagsForRegister.clear()
    }

    fun FillTagsForRegister(tags: ArrayList<ResponseAltaTagItems>, select: Boolean){

        tags.forEach{produc->
            arrayTagsForRegister.add(TagsForRegister(produc.uuid, produc.artic, produc.lotee, produc.almacen, select))
        }
    }

    override fun SendTagsRegister(tags: ArrayList<TagsForRegister>){

        tags.forEach{i ->
            arrayTagsSelected.add(i)
        }

        val gson = Gson()
        val okTags = gson.toJson(tags)
        val retrofit = RetrofitController(appContext)
        retrofit.RegisterTags(okTags, this)
    }

    override fun VerifyTagsInAnyOrder(resp: ResponsRegisterTags){

        resp.uids!!.forEach {tag ->
            arrayTagsNotRegistered.add(tag)
        }

        if(resp.uids.isNullOrEmpty()) presenter.SuccessTagsRegister("Tags registrados correctamente")
        else presenter.SetAdapterRegister(arrayTagsSelected, arrayTagsNotRegistered, "Si")
    }

    override fun UnSuccessTagsRegister(resp: String){
        presenter.UnSuccessTagsRegister(resp)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        if(nameItem.equals("Lectura unica")) {
            presenter.setModeRead(true)
            presenter.setItemNameMenu("Lectura continua", item)
        } else{
            presenter.setModeRead(false)
            presenter.setItemNameMenu("Lectura unica", item)
        }
    }
}
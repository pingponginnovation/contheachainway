package com.example.pingpongalien.conthea.Room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Room
import android.arch.persistence.room.TypeConverters
import android.content.Context
import android.util.Log
import com.example.pingpongalien.conthea.Helpers.HelperDate
import com.example.pingpongalien.conthea.ModelsRoom.*

//TODO.  Cuando haga cambios en el modelo de una tabla, ya sea que agregue nueva columna o cambie el tipo de dato o elimine columna, tengo que incrementar la version de esta DB.
@Database(entities = arrayOf(RoomCatalogItem::class, RoomOrdenes::class, RoomInventario::class), version = 2, exportSchema = true)
@TypeConverters(HelperDate::class)
abstract class RoomController: RoomDatabase(){

    abstract fun daoController(): DaoCatalog
    abstract fun daoOrdenes(): DaoOrdenes
    abstract fun daoInventario(): DaoInventario

    companion object {

        private var INSTANCE: RoomController? = null

        fun getAppDatabase(context: Context): RoomController {
            Log.e("GetappDB", "db")
            if (INSTANCE == null) {
                Log.e("instance null", "db")
                INSTANCE = Room.databaseBuilder(context, RoomController::class.java, "conthea")
                        .fallbackToDestructiveMigration().build()
            }
            return INSTANCE!!
        }

        fun getInstance(): RoomController{
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}
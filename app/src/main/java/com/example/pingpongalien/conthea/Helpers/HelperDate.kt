package com.example.pingpongalien.conthea.Helpers

import android.arch.persistence.room.TypeConverter
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date

/**
 * Created by AOR on 18/12/17.
 */
class HelperDate {

    var dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")

    @TypeConverter
    fun convertStringToDate(value: String?): Date {
        var date: Date? = null
        if (value != null) {
            try {
                date = dateFormat.parse(value)
            } catch (e: ParseException) { }
        }
        return date!!
    }

    @TypeConverter
    fun convertDateToString(indate: Date): String {
        var dateString: String? = null
        try {
            dateString = dateFormat.format(indate)
        } catch (ex: Exception) { }
        return dateString!!
    }
}
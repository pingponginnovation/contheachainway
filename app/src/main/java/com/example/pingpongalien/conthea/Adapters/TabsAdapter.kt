package com.example.pingpongalien.conthea.Adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta.ProductosConEspacio
import com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta.ProductosSinEspacio

class TabsAdapter(fm: FragmentManager?, internal var totalTabs: Int): FragmentStatePagerAdapter(fm){

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ProductosConEspacio()
            1 -> ProductosSinEspacio()
            else->{throw IllegalStateException("$position is illegal") }
        }
    }

    override fun getCount(): Int {
        return totalTabs
    }
}
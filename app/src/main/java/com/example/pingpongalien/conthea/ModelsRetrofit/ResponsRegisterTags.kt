package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponsRegisterTags(

        @field:SerializedName("agregados")
        val total: Int? = null,

        @field:SerializedName("en_orden")
        val uids: ArrayList<String>? = null
)
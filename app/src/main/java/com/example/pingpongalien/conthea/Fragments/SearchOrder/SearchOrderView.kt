package com.example.pingpongalien.conthea.Fragments.SearchOrder

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView.Companion.nameFragment
import com.example.pingpongalien.conthea.Adapters.AdapterOrders
import com.example.pingpongalien.conthea.ControllerWifiManager
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrder
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderItem
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import kotlinx.android.synthetic.main.fragment_search_order_updated.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.util.*

//typeSearch = true = OUT
class SearchOrderView(val typeSearch: Boolean, val core: CoreView): Fragment(), SearchOrder.View{

    val TAG = "SearchOrderView"
    var presenter: SearchOrder.Presenter? = null
    var preferences: PreferencesController? = null
    var adapter: AdapterOrders? = null
    lateinit var datePickerStart: DatePickerDialog
    private var aux = 0 //usada para verificar cuando el recycler tiene o no tiene productos.
    private lateinit var mView: View
    var handler = Handler()
    lateinit var mRunnabale: Runnable
    private var wifiManager: ControllerWifiManager? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        mView = inflater!!.inflate(R.layout.fragment_search_order_updated, container, false)

        nameFragment = ""

        preferences = PreferencesController(context)
        wifiManager = ControllerWifiManager(context)
        presenter = SearchOrderPresenter(this, context)
        configureDatePickerDialog()
        presenter!!.setTypeSearch(typeSearch)

        mView.img_reload.setOnClickListener{
            mView.rcv_orders.visibility = View.GONE
            mView.pb_search_order.visibility = View.VISIBLE
            getOrders()
        }

        return mView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        if(typeSearch) core.changeTitleToolbar("Salidas")
        else core.changeTitleToolbar("Entradas")
    }

    override fun getOrders(){
        presenter!!.getOrders()
    }

    override fun setOrders(responseSuccess: ResponseOrder){
        aux = 1
        mView.pb_search_order.visibility = View.GONE
        mView.rcv_orders.layoutManager = LinearLayoutManager(context)
        adapter =  AdapterOrders(responseSuccess){showDetailOrder(it)}
        mView.rcv_orders.adapter = adapter
    }

    override fun responseErrorOrders(error: String){
        if(error == "Unauthorized") {
            showExpiredSession()
        }
        mView.rcv_orders.visibility = View.VISIBLE
        mView.pb_search_order.visibility = View.GONE
        core.toast(error)
    }

    override fun showNoOrders(){
        aux = 0
        mView.pb_search_order.visibility = View.GONE
        mView.txt_no_orders.visibility = View.VISIBLE
        mView.rcv_orders.visibility = View.GONE
    }

    override fun showOrders(){
        mView.txt_no_orders.visibility = View.GONE
        mView.rcv_orders.visibility = View.VISIBLE
    }

    private fun showExpiredSession(){
        context.alert{
            title = "Advertencia"
            message = "Tú sesión ha expirado. Inicia sesión nuevamente"
            positiveButton("Aceptar"){
                ShowLoginView()
            }
            isCancelable = false
        }.show()
    }

    private fun ShowLoginView(){
        core.closeSession()
    }

    private fun StartThread(){
        mRunnabale = Runnable{
            if(!wifiManager!!.IsNetworkConnected()) core.toast("No estás conectado a ninguna red WIFI")
            else{
                val speed = wifiManager!!.GetNetworkSpeed()
                val strenght = wifiManager!!.GetStrenghtWIFI()
                //if(strenght == 3) core.toast("Conexión WIFI un poco débil")
                if(strenght <= 2) core.toast("Conexión WIFI muy débil")
                if(speed <= 5) core.toast("Baja velocidad del WIFI")
            }
            handler.postDelayed(mRunnabale, 9000)
        }
        handler.postDelayed(mRunnabale, 9000)
    }

    private fun showDetailOrder(order:ResponseOrderItem){   //Here we can go to the details of any order in the list.
        core.detailOrder(order, typeSearch)
    }

    fun eventsView(){
        mView.edt_search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }
            override fun afterTextChanged(editable: Editable) {
                presenter!!.filterOrders(editable.toString(), adapter, 0)
            }
        })
    }

    private fun configureDatePickerDialog(){
        val calendar = Calendar.getInstance()
        datePickerStart = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _ , year, monthOfYear, dayOfMonth ->
            presenter!!.formatDateAndFilterData(year, monthOfYear, dayOfMonth, adapter)
        },calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
    }

    override fun onResume(){
        super.onResume()
        eventsView()
        getOrders()
        StartThread()
    }

    override fun onPause(){
        super.onPause()
        mView.rcv_orders.visibility = View.GONE
        mView.pb_search_order.visibility = View.VISIBLE
        handler.removeCallbacks(mRunnabale)
    }

    //TRUE SHOW DATE - FALSE HIDE DATE
    override fun visibilityDateAndSearchBar(type : Boolean){}
    override fun setDate(date: String){}
}

package com.example.pingpongalien.conthea.Fragments.InitialAdjustment

import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseInitialAdjustment

interface InitialAdjustment{

    interface Model{
        fun AddTag(tag: String)
        fun SendToRetrofit()
        fun Success(response: ResponseInitialAdjustment)
        fun Unsuccess(error: String)
        fun setModeRead(nameItem: String, item: MenuItem)
    }

    interface Presenter{
        fun AddTag(tag: String)
        fun SendToRetrofit()
        fun setModeRead(nameItem: String, item: MenuItem)

        //For the view
        fun Unsuccess(error: String)
        fun SetAmount(total: Int)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
    }

    interface View{
        fun SetAmount(amount: Int)
        fun Unsuccess(error: String)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun setModeRead(mode: Boolean)
    }
}
package com.example.pingpongalien.conthea.ModelsRetrofit

import java.io.Serializable

data class ResponseCatalogItem(
		val productos_id: Int? = null,
		val caducidad: String? = null,
		val espacio_actual: ResponseCatalogPlace? = null,
		val lote: String? = null,
		val cantidad: Int? = null,
		val espacio: ResponseEspacio? = null,
		val producto: ResponseCatalogProduct? = null,
		val almacen: ResponseAlmacen? = null
) : Serializable

data class ResponseAlmacen(
		val id: Int? = null,
		val codigo: String? = null,
		val nombre: String? = null
): Serializable

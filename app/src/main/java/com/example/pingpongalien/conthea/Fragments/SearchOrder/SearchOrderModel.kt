package com.example.pingpongalien.conthea.Fragments.SearchOrder


import android.content.Context
import android.util.Log
import com.example.pingpongalien.conthea.Adapters.AdapterOrders
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrder
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderItem
import com.example.pingpongalien.conthea.Retrofit.RetrofitController

/**
 * Created by AOR on 27/11/17.
 */
class SearchOrderModel(presenter: SearchOrderPresenter, context: Context): SearchOrder.Model {

    val TAG = "SearchOrderModel"

    var presenter: SearchOrder.Presenter? = null
    var aOrders: ResponseOrder? = null
    var appContext: Context

    init {
        this.presenter = presenter
        this.appContext = context
    }

    private var typeSearch = false

    override fun getOrders() {
        val retrofit = RetrofitController(appContext)
        retrofit.getOrders(this, typeSearch)
    }

    override fun responseOrderSuccess(responseSuccess: ResponseOrder) {

        //Log.e("ORDERS", responseSuccess.response.toString())

        aOrders = responseSuccess
        if(responseSuccess.response!!.isNotEmpty()){
            presenter!!.showOrders()
            presenter!!.responseOrderSuccess(responseSuccess)
        }
        else presenter!!.showNoOrders()
    }

    override fun responseErrorOrders(error: String){
        presenter!!.responseErrorOrders(error)
    }

    override fun filterOrders(filter: String, adapterOrders: AdapterOrders?, typeSearch: Int) {
        if(filter.length > 2){
            if(adapterOrders != null) {
                when(typeSearch){
                    0 -> filterByCode(filter, adapterOrders)
                    1 -> filterByCustomer(filter, adapterOrders)
                }
            }else
                presenter!!.showNoOrders()
        }else if(filter.isEmpty()){
            if(adapterOrders != null && aOrders != null) {
                presenter!!.showOrders()
                adapterOrders.filterList(aOrders!!)
            }else
                presenter!!.showNoOrders()
        }
    }

    override fun formatDateAndFilterData(year: Int, month: Int, day: Int,  adapterOrders: AdapterOrders?) {

        var monthInner = month
        monthInner ++

        val dayCorrect: String = if(day.toString().length == 1) "0$day"
        else day.toString()

        val monthCorrect = if(monthInner.toString().length == 1)  "0$monthInner"
        else monthInner.toString()

        presenter!!.setDate("$year-$monthCorrect-$dayCorrect")

        if(adapterOrders != null) filterByDate("$year-$monthCorrect-$dayCorrect", adapterOrders)
        else presenter!!.showNoOrders()
    }

    //TRUE SHOW DATE - FALSE HIDE DATE
    override fun setComponentsCorrect(position: Int) {
        if(position == 2)
            presenter!!.visibilityDateAndSearchBar(true)
        else
            presenter!!.visibilityDateAndSearchBar(false)
    }

    override fun setTypeSearch(typeSearch: Boolean) {
        this.typeSearch = typeSearch
    }

    private fun filterByCode(search: String, adapterOrders: AdapterOrders) {
        val aOrdersItemFilter = ArrayList<ResponseOrderItem>()
        aOrders!!.response!!.forEach { order ->
            if(order!!.codigo!!.contains(search.toUpperCase()))
                aOrdersItemFilter.add(order)
        }
        if(aOrdersItemFilter.size > 0)
            setFilter(adapterOrders, aOrdersItemFilter)
        else
            presenter!!.showNoOrders()
    }

    private fun filterByCustomer(search: String, adapterOrders: AdapterOrders) {
        val aOrdersItemFilter = ArrayList<ResponseOrderItem>()
        aOrders!!.response!!.forEach { order ->
            if(order!!.cliente!!.nombre!!.contains(search.toUpperCase()))
                aOrdersItemFilter.add(order)
        }
        if(aOrdersItemFilter.size > 0)
            setFilter(adapterOrders, aOrdersItemFilter)
        else
            presenter!!.showNoOrders()
    }

    private fun filterByDate(search: String, adapterOrders: AdapterOrders) {
        val aOrdersItemFilter = ArrayList<ResponseOrderItem>()
        aOrders!!.response!!.forEach { order ->
            if(order!!.fecha!!.contains(search.toUpperCase()))
                aOrdersItemFilter.add(order)
        }
        if(aOrdersItemFilter.size > 0)
            setFilter(adapterOrders, aOrdersItemFilter)
         else
            presenter!!.showNoOrders()
    }

    private fun setFilter(adapterOrders: AdapterOrders, aOrdersItemFilter: ArrayList<ResponseOrderItem>){
        presenter!!.showOrders()
        val aOrdersFilter = ResponseOrder(aOrdersItemFilter)
        adapterOrders.filterList(aOrdersFilter)
    }

}
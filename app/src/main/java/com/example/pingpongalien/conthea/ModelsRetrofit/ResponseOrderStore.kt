package com.example.pingpongalien.conthea.ModelsRetrofit


import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class ResponseOrderStore(

	@field:SerializedName("codigo")
	val codigo: String? = null,

	@field:SerializedName("tipo")
	val tipo: String? = null,

	@field:SerializedName("estado")
	val estado: String? = null,

	@field:SerializedName("transito")
	val transito: Int? = null,

	@field:SerializedName("direccion")
	val direccion: String? = null,

	@field:SerializedName("grupo")
	val grupo: String? = null,

	@field:SerializedName("ultimo_cambio")
	val ultimoCambio: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("nombre")
	val nombre: String? = null,

	@field:SerializedName("estatus")
	val estatus: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("alta")
	val alta: String? = null,

	@field:SerializedName("ciudad")
	val ciudad: String? = null,

	@field:SerializedName("observaciones")
	val observaciones: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
) : Serializable
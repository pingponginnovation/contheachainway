package com.example.pingpongalien.conthea.Adapters.ExpandableCatalogue;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.example.pingpongalien.conthea.R;

public class ParentCatalogueViewHolder extends ParentViewHolder{

    private final TextView txt_catalogo;
    private final TextView txt_lote;

    public ParentCatalogueViewHolder(@NonNull View itemView) {
        super(itemView);
        txt_catalogo = itemView.findViewById(R.id.txt_title);
        txt_lote = itemView.findViewById(R.id.txt_lote);
    }

    public TextView getTxtview_catalogo() {
        return txt_catalogo;
    }

    public TextView getTxt_lote() {
        return txt_lote;
    }
}
package com.example.pingpongalien.conthea.Retrofit

import com.example.pingpongalien.conthea.ModelsRetrofit.*
import retrofit2.Call
import retrofit2.http.*

interface Api {

    @POST("login")
    fun login(@Body requestLogin: RequestLogin) : Call<ResponseLogin>

    @POST("lote/verificacion")
    fun verifyTagsOut(@Header("Authorization")token : String, @Body requestVerification : RequestVerifyTag) : Call<ResponseVerifyTag>

    @POST("lote/verificacion/reingreso")
    fun verifyTagsIn(@Header("Authorization")token : String, @Body requestVerification : RequestVerifyTag) : Call<ResponseVerifyTag>

    @POST("almacen/salida/productos")
    fun sendRemisionOut(@Header("Authorization")token : String, @Body requestOrders: RequestRemision) : Call<ResponseRemision>

    @POST("almacen/entrada/productos")
    fun sendRemisionIn(@Header("Authorization")token : String, @Body requestOrders: RequestRemision) : Call<ResponseRemision>

    @POST("producto/ubicacion")
    fun sendTags(@Header("Authorization")token : String, @Body requestPlaces: RequestPlaces) : Call<ResponsePlaces>

    @POST("producto/ubicacion/confirm")
    fun SendTagsForLocation(@Header("Authorization")token: String, @Body tags: String): Call<ResponseTagsLocation>

    @POST("producto/ubicacion/verificar")
    fun verifyUbication(@Header("Authorization")token : String, @Body requestUbication : RequestVerify) : Call<ResponseVerify>

    @POST("producto/ubicacion/verificar/local")
    fun verifyUbicationLocal(@Header("Authorization")token : String, @Body requestUbication : RequestVerify) : Call<ResponseVerifyLocal>

    @POST("items/borrar")
    fun deleteProduct(@Header("Authorization")token : String, @Body requestDelete: RequestDelete) : Call<ResponseDelete>

    @POST("items/borrar/confirm")
    fun DeleteTags(@Header("Authorization")token : String, @Body tags: String) : Call<ResponseDeletedTags>

    @POST("items/info")
    fun detailTag(@Header("Authorization")token : String, @Body requestDetailTag : RequestDetailTag) : Call<ResponseDetailTag>

    @POST("productos/nombres")
    fun getNameUuid(@Header("Authorization")token : String, @Body requestNameUuid: RequestNameUuid) : Call<ResponseNameUuid>

    @POST("inventario-inicial/sobrante")
    fun startInventory(@Header("Authorization") token: String, @Body requestStartInventory: RequestStartInventory) : Call<ResponseStartInventory>

    @GET("producto/ubicacion/{idProducto}")
    fun getPlaces(@Header("Authorization")token : String, @Path ("idProducto") idProducto : Int) : Call<ResponseCatalog>

    @GET("almacen/salida/pendiente")
    fun getOrdersOut(@Header("Authorization")token : String) : Call<ResponseOrder>

    @GET("almacen/entrada/pendiente")
    fun getOrdersIn(@Header("Authorization")token : String) : Call<ResponseOrder>

    @GET("ubicaciones")
    fun getPlaces(@Header("Authorization")token : String) : Call<ResponseGetPlaces>

    @GET("productos")
    fun getProducts(@Header("Authorization")token : String) : Call<List<ResponseProducts>>

    @GET("producto/ubicacion?catalogue=catalogue&name=name&lote=lote")
    fun getCataloge(@Header("Authorization")token : String, @Query("catalogue") catalogo: String, @Query("name") nombre: String, @Query("lote") lote_: String): Call<ResponseCatalog>

    @GET("espacio?space_id=space_id")
    fun getproductos(@Header("Authorization")token: String, @Query("space_id") spaceID: Int): Call<ResponseCatalog>

    @POST("almacen/salida/ocupar")
    fun sendIDOrden(@Header("Authorization")token: String, @Body idOrden: String): Call<ResponseIdOrden>

    @POST("almacen/ajuste/cant_intelisis")
    fun checkAjuste(@Header("Authorization")token: String, @Body ajuste: String): Call<ResponseAjuste>

    @POST("almacen/ajuste")
    fun checkAjuste2(@Header("Authorization")token: String, @Body ajuste: String): Call<ResponseAjuste2>

    @POST("almacen/ajuste/ajuste_inicial")
    fun SendInitialAdjustment(@Header("Authorization")token: String, @Body ajuste: String): Call<ResponseInitialAdjustment>

    @POST("items/agregar")
    fun DarAltaTag(@Header("Authorization")token: String, @Body ajuste: String): Call<ResponseAltaTag>

    @POST("items/agregar/confirm")
    fun RegisterTags(@Header("Authorization")token: String, @Body ajuste: String): Call<ResponsRegisterTags>

    @POST("lectura/tags")
    fun search_by_etiqueta(@Header("Authorization")token: String, @Body req: RequestEtiquetas): Call<ResponseEtiquetas>
}
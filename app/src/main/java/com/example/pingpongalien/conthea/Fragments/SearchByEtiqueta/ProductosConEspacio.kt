package com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta

import android.app.Fragment
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.*
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import com.example.pingpongalien.conthea.Adapters.AdapterByEtiquetas
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.Adapter
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta.SearchByEtiquetaView.Companion.receiverSpace
import com.example.pingpongalien.conthea.Models.DataProductsSpaces
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import kotlinx.android.synthetic.main.fragment_productos_con_espacio.view.*
import org.jetbrains.anko.toast

class ProductosConEspacio: ReaderFragment(), SearchByEtiqueta.View{

    private lateinit var mvi: View
    private var tagsConUbicacion: ArrayList<ParentUbication>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mvi = inflater!!.inflate(R.layout.fragment_productos_con_espacio, container, false)
        ListenBroadcast()
        RegisterBroadcast()
        return mvi
    }

    private fun ListenBroadcast(){
        Log.e("Register", "registered broadcast")
        receiverSpace = object : BroadcastReceiver() {
            override fun onReceive(contxt: Context?, intent: Intent?) {
                when (intent?.action) {
                    "CON_UBICACION" -> {

                        if(intent.getSerializableExtra("TAGS") != null){
                            val tags = intent.getSerializableExtra("TAGS") as ArrayList<ParentUbication>
                            tagsConUbicacion = tags
                            Log.e("tagsconespacioWOW", tagsConUbicacion.toString())
                            setData()
                        }

                        val clear = intent.getBooleanExtra("CLEAR", false)
                        if(clear) cleanTags()
                    }
                }
            }
        }
    }

    private fun RegisterBroadcast(){
        LocalBroadcastManager.getInstance(activity).registerReceiver(receiverSpace, IntentFilter("CON_UBICACION"))
    }

    private fun UnregisterBroadcast(){
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(receiverSpace)
        receiverSpace = null
    }

    private fun setData(){
        Log.e("endata", tagsConUbicacion.toString())
        val adapterProds = tagsConUbicacion?.let{AdapterByEtiquetas(context, it)}
        mvi.rclv_con_espacio.addItemDecoration(SimpleDividerItemDecoration(context))
        mvi.rclv_con_espacio.apply{
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = adapterProds
        }
    }

    override fun cleanTags() {
        //if(!tagsConUbicacion.isNullOrEmpty()) tagsConUbicacion?.clear()
        setData()
    }

    override fun onResume() {
        super.onResume()
        if(receiverSpace == null) {
            ListenBroadcast()
            RegisterBroadcast()
        }
    }

    override fun onStop() {
        super.onStop()
        if(receiverSpace != null) UnregisterBroadcast()
    }

    override fun initReader() {}
    override fun activateReader() {}
    override fun successTags(tagsSinUbicacion: ArrayList<ParentUbication>?, tagsConUbicacion: ArrayList<ParentUbication>?) {}
    override fun errorTags(resp: String) {}
}

package com.example.pingpongalien.conthea.ModelsRoom

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import java.util.*
import kotlin.collections.ArrayList

@Dao
interface DaoOrdenes{

    @Insert
    fun InsertOrden(ordenRoom: RoomOrdenes)

    @Query("DELETE FROM ordenes_pendientes WHERE remission_number LIKE :remision")
    fun deleteOrdenesFromDB(remision: String)

    @Query("SELECT EXISTS (SELECT * FROM ordenes_pendientes)")
    fun isExists(): Boolean

    @Query("SELECT * FROM ordenes_pendientes WHERE remission_number LIKE :remision")
    fun GetTagsSaved(remision: String): List<RoomOrdenes>
}
package com.example.pingpongalien.conthea.FCMnotifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.example.appnetworking.FCMnotifications.MyWorker
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Fragments.SearchOrder.SearchOrderView
import com.example.pingpongalien.conthea.Fragments.Unsuscribe.UnsuscribeView
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService: FirebaseMessagingService(){

    var notificationIDPrefs: String? = null
    var notificationID: Int? = null
    var preferences: PreferencesController? = null

    lateinit var notification: Notification
    private val channelId = "com.example.vicky.notificationexample"
    var body = ""
    var title = ""
    var id = -1

    override fun onMessageReceived(remoteMessage: RemoteMessage){

        preferences = PreferencesController(this)
        if(!preferences!!.getToken().isNullOrEmpty()){

            notificationIDPrefs = preferences!!.getIDnotification()!!
            if(notificationIDPrefs.isNullOrEmpty()) notificationID = 0
            else{
                var n: Int = notificationIDPrefs!!.toInt()
                n += 1
                notificationID = n
            }

            remoteMessage.data.isNotEmpty().let {

                Log.e("Message data payload: ", remoteMessage.data.toString())

                body = remoteMessage.data.get("body").toString()
                title = remoteMessage.data.get("title").toString()
                GetReadyNotification(notificationID!!)

                if (/* Check if data needs to be processed by long running job */ true) {
                    // For long-running tasks (10 seconds or more) use WorkManager.
                    scheduleJob()
                } else {
                    // Handle message within 10 seconds
                    handleNow()
                }
            }
        }
        else Log.e("null", "nulltoken")

        remoteMessage.notification?.let {
            Log.e("Message Notification:", it.body!!)
        }
    }

    fun GetReadyNotification(notificationID: Int){

        var intent: Intent? = null
        intent = Intent(this, CoreView::class.java)
        intent.putExtra("NOTIFICATION", true) //para la vista de salidas

        val pendingIntent = PendingIntent.getActivity(this, notificationID, intent, PendingIntent.FLAG_ONE_SHOT)
        val alarmSound: Uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + this.packageName + "/raw/twitter")

        SetUpNotification(pendingIntent, alarmSound)
        with(NotificationManagerCompat.from(this)){
            notify(notificationID, notification)
        }
        preferences!!.SaveIDNotification(notificationID)
    }

    fun SetUpNotification(pendingIntent: PendingIntent, alarmSound: Uri){

        notification = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.icon_notificaciotn)
            .setLargeIcon(BitmapFactory.decodeResource(this.resources,R.drawable.icon_notificaciotn))
            .setContentTitle(title)
            .setSound(alarmSound)
            .setContentText(body)
            .setAutoCancel(true)
                .setContentIntent(pendingIntent)
            .build()
    }

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
        Log.e("NUEVOTOKEN", "SIIII")

        sendRegistrationToServer(token)
    }

    private fun scheduleJob() {
        // [START dispatch_job]
        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        WorkManager.getInstance().beginWith(work).enqueue()
        // [END dispatch_job]
    }

    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
        Log.d(TAG, "sendRegistrationTokenToServer($token)")
    }
}

package com.example.pingpongalien.conthea.Activities.DetailVerify

import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseAjuste
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalogItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseVerify
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseVerifyLocal

/**
 * Created by AOR on 7/12/17.
 */
interface DetailVerify {

    interface View{
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun unsuccessVerify(error : String)
        fun setAmount(verify: Int, total : Int)
        fun setTotal(total : String)
        fun setColorComplete()
        fun setColorIncomplete()
        fun setCorrectTags(misTags: ArrayList<String>)
        fun SetAmountIntelisis(cantidad: Int)
        fun UnsuccessAmountIntelisis(msj: String)
    }

    interface Presenter {
        //MODEL
        fun addTag(tag: String)
        fun searchTag()
        fun setModeRead(nameItem : String, item: MenuItem)
        fun setCatalog(product : ResponseCatalogItem)
        fun GetAmountIntelisis(request: String)
        fun LoadLastStateOrder(tags: ArrayList<String>)
        fun addTagNoDB(tag: String)
        fun clearView()

        //VIEW
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun unsuccessVerify(error : String)
        fun setAmount(verify: Int, total : Int)
        fun setTotal(total : String)
        fun setColorComplete()
        fun setColorIncomplete()
        fun setCorrectTags(misTags: ArrayList<String>)
        fun SetAmountIntelisis(cantidad: Int)
        fun UnsuccessAmountIntelisis(msj: String)
    }

    interface Model{
        fun addTagForVerify(tag: String)
        fun searchTag()
        fun setModeRead(nameItem : String, item: MenuItem)
        fun successVerify(response : ResponseVerify)
        fun successVerifyLocal(response : ResponseVerifyLocal)
        fun unsuccessVerify(error : String)
        fun setCatalog(product : ResponseCatalogItem)
        fun SuccessAmountIntelisis(resp: ResponseAjuste)
        fun GetAmountIntelisis(request: String)
        fun UnsuccessAmountIntelisis(msj: String)
        fun LoadLastStateOrder(tags: ArrayList<String>)
        fun addTagNoDB(tag: String)
        fun clearView()
    }

}
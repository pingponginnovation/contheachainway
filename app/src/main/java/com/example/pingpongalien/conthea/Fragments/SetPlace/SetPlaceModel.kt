package com.example.pingpongalien.conthea.Fragments.SetPlace

import android.content.Context
import android.util.Log
import android.view.MenuItem
import com.example.pingpongalien.conthea.Models.TagsChangeLocation
import com.example.pingpongalien.conthea.Models.TagsChangeLocation2
import com.example.pingpongalien.conthea.Models.TagsForRegister
import com.example.pingpongalien.conthea.ModelsRetrofit.*
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.google.gson.Gson
import org.jetbrains.anko.collections.forEachWithIndex

/**
 * Created by AOR on 5/12/17.
 */
class SetPlaceModel(presenter: SetPlacePresenter, context: Context): SetPlace.Model {

    var presenter: SetPlace.Presenter? = null
    var idPlace = 0
    var aPlaces: ResponseGetPlaces? = null
    var aTags = arrayListOf<String>()
    var appContext: Context
    val arrayTagsForLocation = ArrayList<TagsChangeLocation2>()

    init {
        this.presenter = presenter
        this.appContext = context
    }

    override fun requestPlaces(from: String) {
        val retrofit = RetrofitController(appContext)
        retrofit.getPlaces(this, from)
    }

    override fun successRequestGetPlaces(response : ResponseGetPlaces){
        //Log.e("responseplaces", response.toString())
        if(response.aPlaces.size > 0) {
            aPlaces = response
            //Log.e("responseplaces", aPlaces.toString())
            setPlaces()
            setId()
            //setNamePlace()
        }
        else presenter!!.unSuccessRequestPlaces("No existen ubicaciones activas")
    }

    override fun unSuccessRequestGetPlaces(error : String){
        presenter!!.unSuccessRequestPlaces(error)
        aTags.clear()
    }

    override fun setidPlace(position: Int) {
        idPlace = aPlaces!!.aPlaces[position].id!!
    }

    override fun getName(position : Int) {
        //presenter!!.setNamePlace(aPlaces!!.aPlaces[position].almacen!!.nombre!!)
    }

    override fun addTag(tag: String) {
        aTags.add(tag)
    }

    override fun sendTags(){
        val retrofit = RetrofitController(appContext)
        retrofit.sendTags(RequestPlaces(idPlace, aTags), this)
    }

    override fun successRequestPlaces(response: ResponsePlaces){
        ClearArrayTags()
        if(response.response != null){
            response.response.forEach{
                CountAmountEachOne(it)
            }
            presenter!!.SetAdapterLocationTags(arrayTagsForLocation, response.response)
        }
        if(aTags.isNotEmpty()) aTags.clear()
    }

    fun ClearArrayTags(){
        if(arrayTagsForLocation.isNotEmpty()) arrayTagsForLocation.clear()
    }

    fun CountAmountEachOne(item: ResponsePlacesItems){
        if(arrayTagsForLocation.isEmpty()) arrayTagsForLocation.add(TagsChangeLocation2(item.article, item.lote, item.storage, false, 1))
        else{
            val size = arrayTagsForLocation.size

            arrayTagsForLocation.forEachWithIndex{i, it ->
                if(it.article!!.trim() == item.article!!.trim() && it.lote!!.trim() == item.lote!!.trim()
                        && it.almacen!!.trim() == item.storage!!.trim()) it.total++
                else if(i+1 == size)
                    arrayTagsForLocation.add(TagsChangeLocation2(item.article, item.lote, item.storage, false, 1))
            }
        }
    }

    data class RequestForLocation(val tags: ArrayList<String>, val code: String)
    override fun SendCahngeLocationTags(tags: ArrayList<String>, code: String){

        val retrofit = RetrofitController(appContext)
        val myRequest = RequestForLocation(tags, code)
        val gson = Gson()
        val get = gson.toJson(myRequest)
        Log.e("reqqq", get)
        retrofit.SendTagsLocation(get,this)
    }

    override fun SuccessChangeLocationTags(msj: String) {
        presenter!!.SuccessChangeLocationTags(msj)
    }

    override fun UnSuccessChangeLocationTags(msj: String) {
        presenter!!.UnSuccessChangeLocationTags(msj)
    }

    override fun setModeRead(nameItem: String, item: MenuItem){
        if(nameItem.equals("Lectura unica")) {
            presenter!!.setModeRead(true)
            presenter!!.setItemNameMenu("Lectura continua", item)
        } else{
            presenter!!.setModeRead(false)
            presenter!!.setItemNameMenu("Lectura unica", item)
        }
    }

    fun setPlaces(){
        val aNames = ArrayList<String>()
        aPlaces!!.aPlaces.forEach{ place ->
            aNames.add(place.codigo!!)
        }
        presenter!!.setPlaces(aNames)
    }

    fun setId(){
        idPlace = aPlaces!!.aPlaces[0].id!!
    }
}
package com.example.pingpongalien.conthea;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.rscja.deviceapi.RFIDWithUHFUART;

public abstract class ReaderFragmentUnsuscribe extends Fragment{

    // ------------------------------------------------------------------------
    // Member Variable
    // ------------------------------------------------------------------------

    protected int mView;

    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------

    public ReaderFragmentUnsuscribe() {
        super();
        mView = 0;
    }

    // ------------------------------------------------------------------------
    // Activit Event Handler
    // ------------------------------------------------------------------------


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_unsuscribe, container, false);
        mView = view.getId();
        getActivity().setContentView(mView);
        return view;
    }

    @Override
    public void onDestroy() {
        // Deinitalize RFID reader Instance
        super.onDestroy();
    }

    @Override
    public void onStart(){
        super.onStart();
        onInitReader(false);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.e("Acaaaes", "finiiish");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    // Initialize Reader
    protected abstract void initReader();

    // Activated Reader
    protected abstract void activateReader();

    // Begin Initialize Reader
    private void onInitReader(final boolean enabled) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //initReader();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //activateReader();
                    }

                });
            }

        }).start();
    }
}
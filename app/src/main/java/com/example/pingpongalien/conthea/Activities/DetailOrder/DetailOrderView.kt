package com.example.pingpongalien.conthea.Activities.DetailOrder

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.media.AudioManager
import android.media.SoundPool
import android.os.*
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.example.pingpongalien.conthea.*
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView.Companion.mContext
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView.Companion.nameFragment
import com.example.pingpongalien.conthea.Adapters.*
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta.SearchByEtiquetaView
import com.example.pingpongalien.conthea.Fragments.SetPlace.SetPlaceView
import com.example.pingpongalien.conthea.Models.TagNoMatch
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseEspacios
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseOrderProduct
import com.example.pingpongalien.conthea.Room.RoomController
import com.google.android.gms.common.internal.FallbackServiceBroker
import com.rscja.deviceapi.RFIDWithUHFUART
import com.rscja.deviceapi.entity.UHFTAGInfo
import kotlinx.android.synthetic.main.activity_detail_order_updated.view.*
import kotlinx.android.synthetic.main.activity_detail_order_updated.view.btn_continue_read
import kotlinx.android.synthetic.main.activity_detail_order_updated.view.btn_single_read
import kotlinx.android.synthetic.main.activity_detail_order_updated.view.shower_listview
import kotlinx.android.synthetic.main.dialog_no_match.view.*
import kotlinx.android.synthetic.main.dialog_nomatch_products.*
import kotlinx.android.synthetic.main.dialog_select_potencia.*
import org.jetbrains.anko.*
import org.jetbrains.anko.collections.forEachWithIndex
import java.util.HashMap

class DetailOrderView(val coreView: CoreView): ReaderFragment(), DetailOrder.View{

    val TAG = "DetailOrderView"
    var presenter: DetailOrder.Presenter? = null
    var responseOrderItem: ResponseOrderItem? = null
    var adapter: AdapterProducts? = null
    private lateinit var cliente: String
    private var readSingle = false
    private var dialog: Dialog? = null
    var handler = Handler()
    lateinit var mRunnabale: Runnable
    private var wifiManager: ControllerWifiManager? = null
    private var mSound: SoundPlay? = null
    private var isOffline = false
    private lateinit var mview: View
    private var mAdapter: Adapter_potencia = Adapter_potencia()
    private var typeSearch: Boolean? = null
    companion object{
        private var POWER_LEVEL = 10
        var CODIGO_REMISION = ""
        var MOVIMIENTO_ID = -1
        var loopFlag = false
        var mReader: RFIDWithUHFUART? = null
        var handlerMSJ: Handler? = null
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mview = inflater!!.inflate(R.layout.activity_detail_order_updated, container, false)

        nameFragment = "DetailOrder"

        //activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        HideKeyboard()

        CODIGO_REMISION = responseOrderItem?.codigo!!
        MOVIMIENTO_ID = responseOrderItem?.id_movimiento?:0
        setDataView()
        setAdapter()
        wifiManager = ControllerWifiManager(context)
        presenter = DetailOrderPresenter(this, context)
        setLotes()
        setProducts()
        setAdapterModel()
        setTotal(0, "")
        CheckDatabase()
        presenter!!.setTypeSearch(typeSearch!!)
        mSound = SoundPlay(context)
        iinit()
        InitHandler()
        return mview
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(typeSearch!!) coreView.changeTitleToolbar("Salidas")
        else coreView.changeTitleToolbar("Entradas")

        configPotency()
        configLectura()

        stateButtonSend(false)
        mview.btn_send.setOnClickListener{
            if(!isOffline){
                mview.LY_progressBar_orders.visibility = View.VISIBLE
                mview.LY_alldata.visibility = View.GONE
                presenter!!.sendData(responseOrderItem!!.productos!!.size)
            }
            else{
                mview.LY_progressBar_orders.visibility = View.VISIBLE
                mview.LY_alldata.visibility = View.GONE
                presenter!!.searchTag()
            }
        }
        mview.mode_switch.setOnCheckedChangeListener{ _, ischeck ->
            isOffline = !ischeck
        }
    }

    private fun HideKeyboard(){
        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if(activity!!.currentFocus == null) return
        if(activity!!.currentFocus!!.windowToken == null) return
        imm.hideSoftInputFromWindow(activity!!.currentFocus!!.windowToken, 0)
    }

    private fun stateButtonSend(enable: Boolean){
        if(enable){
            mview.btn_send.background.setColorFilter(ContextCompat.getColor(coreView, R.color.green_read), PorterDuff.Mode.MULTIPLY)
            mview.btn_send.setTextColor(Color.WHITE)
            mview.btn_send.isEnabled = true
        }
        else{
            mview.btn_send.background.setColorFilter(ContextCompat.getColor(coreView, R.color.gray_read), PorterDuff.Mode.MULTIPLY)
            mview.btn_send.setTextColor(Color.WHITE)
            mview.btn_send.isEnabled = false
        }
    }

    fun setDatos(order:ResponseOrderItem, typeSearch: Boolean){
        responseOrderItem = order
        this.typeSearch = typeSearch
    }

    private fun configPotency(){

        mview.autocomplete_potencia.setOnClickListener{
            mview.autocomplete_potencia.isEnabled = false
            val arrayPotency = arrayListOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                    21,22,23,24,25,26,27,28,29,30)
            LoadPotenciasRecycler(arrayPotency)
        }

        /*adapterPotencias = AdapterSpinnerPotencia(arrayPotency){Setpotencia(it)}

        mview.sp_potencia.setOnClickListener{
            popupWindow?.dismiss()
            if (popupWindow == null) providePopupWindow(mview.shower_listview)
            popupWindow!!.showAsDropDown(mview.shower_listview, 0, - mview.shower_listview.height)
        }*/
    }

    private fun LoadPotenciasRecycler(potencia: ArrayList<Int>){
        dialog = Dialog(activity!!)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.dialog_select_potencia)

        dialog!!.rclv_potencias.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        dialog!!.rclv_potencias.setHasFixedSize(true)
        dialog!!.rclv_potencias.addItemDecoration(SimpleDividerItemDecoration(context))
        mAdapter.Adapter_potencia(potencia){Setpotencia(it)}
        dialog!!.rclv_potencias.adapter = mAdapter

        dialog!!.btn_close_dialog!!.setOnClickListener{
            mview.autocomplete_potencia.isEnabled = true
            dialog?.dismiss()
        }

        dialog!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog!!.show()
    }

    private fun providePopupWindow(vieww: View){
        /*popupWindow = PopupWindow(vieww.width, ViewGroup.LayoutParams.WRAP_CONTENT)
            .apply {
                val backgroundDrawable = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    activity!!.getDrawable(R.drawable.design_autocomplete).apply{}
                } else {
                    Log.e("Else", "menor lollipop")
                    TODO("VERSION.SDK_INT < LOLLIPOP")
                }
                setBackgroundDrawable(backgroundDrawable)
                isOutsideTouchable = true

                val listView = layoutInflater.inflate(R.layout.layout_potencia_dropdown, null,
                    false) as ListView
                listView.adapter = adapterPotencias
                contentView = listView
            }*/
    }

    private fun Setpotencia(num: Int){
        mview.autocomplete_potencia.isEnabled = true
        dialog?.dismiss()
        if(mReader!!.setPower(num)){
            mview.autocomplete_potencia.setText(num.toString())
            POWER_LEVEL = num
            context.toast("Potencia ajustada correctamente a: $num")
        }
        else context.toast("Error en establecer potencia")
    }

    private fun configLectura(){

        mview.btn_continue_read.background.setColorFilter(ContextCompat.getColor(context, R.color.green_read), PorterDuff.Mode.MULTIPLY)
        mview.btn_continue_read.setTextColor(Color.WHITE)

        mview.btn_single_read.setOnClickListener{
            readSingle = true
            mview.btn_continue_read.background.setColorFilter(ContextCompat.getColor(context, R.color.gray_read), PorterDuff.Mode.MULTIPLY)
            mview.btn_continue_read.setTextColor(Color.parseColor("#2D2B2B"))

            mview.btn_single_read.background.setColorFilter(ContextCompat.getColor(context, R.color.green_read), PorterDuff.Mode.MULTIPLY)
            mview.btn_single_read.setTextColor(Color.WHITE)
        }

        mview.btn_continue_read.setOnClickListener{
            readSingle = false
            mview.btn_continue_read.background.setColorFilter(ContextCompat.getColor(context, R.color.green_read), PorterDuff.Mode.MULTIPLY)
            mview.btn_continue_read.setTextColor(Color.WHITE)

            mview.btn_single_read.background.setColorFilter(ContextCompat.getColor(context, R.color.gray_read), PorterDuff.Mode.MULTIPLY)
            mview.btn_single_read.setTextColor(Color.parseColor("#2D2B2B"))
        }
    }

    private fun StartThread(){
        mRunnabale = Runnable{
            if(!wifiManager!!.IsNetworkConnected()) coreView.toast("No estás conectado a ninguna red WIFI")
            else{
                val speed = wifiManager!!.GetNetworkSpeed()
                val strenght = wifiManager!!.GetStrenghtWIFI()
                //if(strenght == 3) coreView.toast("Conexión WIFI un poco débil")
                if(strenght <= 2) coreView.toast("Conexión WIFI muy débil")
                if(speed <= 2) coreView.toast("Baja velocidad del WIFI")
            }
            handler.postDelayed(mRunnabale, 9000)
        }
        handler.postDelayed(mRunnabale, 9000)
    }

    private fun CheckDatabase(){

        var exist: Boolean?
        val arrayOfTags: ArrayList<String> = ArrayList()

        doAsync{
            val database = RoomController.getInstance()
            exist = database.daoOrdenes().isExists()
            if(exist!!){
                val order = database.daoOrdenes().GetTagsSaved(responseOrderItem!!.codigo!!)
                uiThread {
                    if(order.isNotEmpty()){
                        context.alert{
                            title = "Aviso"
                            message = "Existe un respaldo de esta órden, ¿Deseas cargar los datos?"
                            negativeButton("Eliminar respaldo"){deleteBackup()}
                            neutralPressed("Cancelar"){}
                            positiveButton("Si"){
                                order.forEach{
                                    arrayOfTags.add(it.uuidNumber!!)
                                }
                                if(arrayOfTags.isNotEmpty()) presenter!!.LoadLastStateOrder(arrayOfTags)
                            }
                            isCancelable = false
                        }.show()
                    }
                }
            }else Log.e("No existe", "la DB")
        }
    }

    private fun deleteBackup(){
        doAsync {
            val database = RoomController.getInstance()
            database.daoOrdenes().deleteOrdenesFromDB(CODIGO_REMISION)
        }
    }

    override fun successRequestRemision(response: String){
        deleteBackup()
        CreateDialogRespuesta(response, 1)
    }

    fun CreateDialogRespuesta(msj: String, aux: Int){

        val builder = AlertDialog.Builder(context)
        val layoutInflater = LayoutInflater.from(context)
        val viewDialog: View = layoutInflater.inflate(R.layout.dialog_possitive_message, null)
        val idTxtMsj = viewDialog.findViewById(R.id.idTxtMsj) as TextView
        val img = viewDialog.findViewById(R.id.im_circle) as ImageView

        if(aux == -1) img.setImageResource(R.drawable.error)
        else img.setImageResource(R.drawable.correct)

        idTxtMsj.text = msj

        builder.setView(viewDialog).setPositiveButton(getString(R.string.aceptar)) { _ , _ ->
            coreView.ordenesEntradaSalida(typeSearch!!)
        }
        builder.setCancelable(false)
        builder.show()
    }

    override fun unsuccessRequestRemision(error: String){
        mview.LY_alldata.visibility = View.VISIBLE
        mview.LY_progressBar_orders.visibility = View.GONE
        CreateDialogRespuesta(error, -1)
    }

    override fun tagsNoMatch(aTagsNoMatch:  ArrayList<TagNoMatch>){
                                //Muestra los tags que no pertenecen a la orden de salida
        showDialogNoMatch(aTagsNoMatch)
    }

    override fun setModeRead(mode: Boolean){}
    override fun initReader(){}

    override fun SetProgressBar(state: Boolean){
        if(state){
            mview.LY_progressBar_badproducts.visibility = View.VISIBLE
            mview.LY_alldata.visibility = View.GONE
        }
        else{
            mview.LY_progressBar_orders.visibility = View.GONE
            mview.LY_progressBar_badproducts.visibility = View.GONE
            mview.LY_alldata.visibility = View.VISIBLE
        }
    }

    override fun UnsuccessVerifyTags(resp: String) {
        mview.LY_progressBar_orders.visibility = View.GONE
        mview.LY_progressBar_badproducts.visibility = View.GONE
        mview.LY_alldata.visibility = View.VISIBLE
        coreView.toast(resp)
    }

    private fun showDialogNoMatch(aTagsNoMatch: ArrayList<TagNoMatch>){  //Muestra los tags que no son validos para la orden de salida

        dialog = Dialog(context)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.dialog_nomatch_products)

        val adapter = AdapterNoMatch(aTagsNoMatch)
        val rcv_tags_no_match = dialog!!.findViewById(R.id.rclv_productos_nomatch) as RecyclerView
        rcv_tags_no_match.setHasFixedSize(true)
        rcv_tags_no_match.layoutManager = LinearLayoutManager(context)
        rcv_tags_no_match.adapter = adapter


        dialog!!.btn_close_dialog_nomatch!!.setOnClickListener{
            dialog!!.dismiss()
        }

        dialog!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog!!.show()
    }

    fun iinit(){
        try {
            mReader = RFIDWithUHFUART.getInstance()
            if (mReader != null){

                if (mReader != null) {

                    mview.autocomplete_potencia.setText(POWER_LEVEL.toString())

                    someTask().MyCustomTask(context)
                    someTask().execute()
                }
            }
        } catch (e: Exception) {
            coreView.toast(e.message!!)
        }
    }

    class someTask: AsyncTask<Void, Void, Boolean>() {

        var mypDialog: ProgressDialog? = null

        override fun doInBackground(vararg params: Void?): Boolean?{
            return mReader!!.init()
        }

        fun MyCustomTask(context: Context) {
            mContext = context
        }

        override fun onPreExecute() {
            super.onPreExecute()

            mypDialog = ProgressDialog(mContext)
            mypDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            mypDialog!!.setMessage("Iniciando RFID...")
            mypDialog!!.setCanceledOnTouchOutside(false)
            mypDialog!!.show()
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            mypDialog!!.cancel()
            if (!result!!) {
                Toast.makeText(mContext, "init fail", Toast.LENGTH_SHORT).show()
            }
            else{
                try {
                    mReader!!.power = POWER_LEVEL
                    Log.e("Power", mReader!!.power.toString())
                }catch(e: Exception){}
            }
        }
    }

    private fun setDataView(){
        mview.txt_name_order.text = getString(R.string.pedido_nombre, responseOrderItem!!.codigo)     //Codigo del pedido  ejemplo: MTY20275
        cliente = responseOrderItem!!.cliente!!.nombre.toString() //Nombre del cliente
    }

    var loteNull = false
    private fun setAdapter(){
                //Llenado del recycler de productos de la orden de salida donde se le dió click

        responseOrderItem!!.productos!!.forEachWithIndex{i, prod ->
            if(prod!!.tagsLoteSalida.isNullOrEmpty()) loteNull = true
        }

        mview.rcv_products.layoutManager = LinearLayoutManager(context)
        adapter = AdapterProducts(responseOrderItem!!.productos as ArrayList<ResponseOrderProduct>, activity, cliente, this, loteNull){showSpaces(it)}
        mview.rcv_products.adapter = adapter
    }

    private fun showSpaces(spaces: ArrayList<ResponseEspacios>){
        dialog = Dialog(context)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.dialog_location_products)

        val textoSpacios = dialog!!.findViewById(R.id.txt_spacios) as TextView

        spaces.forEach{
            textoSpacios.append("${it.espacio}: ${it.total} artículos"+"\n")
        }

        dialog!!.btn_close_dialog_nomatch!!.setOnClickListener{
            dialog!!.dismiss()
        }

        dialog!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog!!.show()
    }

    private fun setLotes(){
        presenter!!.setLotes(responseOrderItem?.productos as List<ResponseOrderProduct>)
    }

    private fun setProducts(){
        //Log.e("setProductos", "entra")
        //Log.e("setProductos", responseOrderItem!!.productos.toString())
        presenter!!.setProducts(responseOrderItem?.productos as List<ResponseOrderProduct>, responseOrderItem?.movimiento?:"")
    }

    private fun setAdapterModel(){
        presenter!!.setAdapter(adapter!!)
    }

    override fun ErrorPeticionNetwork(){
        coreView.toast("No se completó la petición, error de conexión a internet")
        mview.LY_alldata.visibility = View.VISIBLE
        mview.LY_progressBar_orders.visibility = View.GONE
    }

    override fun setTotal(total: Int, msj: String){
        if(msj.isNotEmpty()) coreView.toast(msj)
        mview.LY_alldata.visibility = View.VISIBLE
        mview.LY_progressBar_orders.visibility = View.GONE
        mview.txt_total.text = "$total / ${responseOrderItem!!.productos!!.size}"
        if(total == responseOrderItem!!.productos!!.size) stateButtonSend(true)
    }

    override fun ErrorRequestNameUuid(msj: String, total: Int){
        coreView.toast(msj)
        mview.LY_alldata.visibility = View.VISIBLE
        mview.LY_progressBar_orders.visibility = View.GONE
        mview.txt_total.text = "$total / ${responseOrderItem!!.productos!!.size}"
    }

    override fun showNoProductsToSend(){
        mview.LY_alldata.visibility = View.VISIBLE
        mview.LY_progressBar_orders.visibility = View.GONE

        if(typeSearch!!)coreView.toast(getString(R.string.no_productos_salida))
        else coreView.toast(getString(R.string.no_productos_entrada))
    }

    override fun showNoCompleteOrder(status: Int){
        mview.LY_alldata.visibility = View.VISIBLE
        mview.LY_progressBar_orders.visibility = View.GONE

        if(status == 1){
            context.alert(getString(R.string.productos_de_mas), getString(R.string.atencion)){
                yesButton{}
            }.show()
        }
        else{
            context.alert(getString(R.string.productos_menos), getString(R.string.atencion)){
                yesButton{}
            }.show()
        }
    }

    private var aux = 0
    fun ShowDialogErrorLotes(){
        if(typeSearch!!){
            if(aux == 0){
                context.alert("Algún producto de la órden no se encuentra en Almacén"){
                    yesButton{}
                }.show()
                aux = 1
            }
        }
    }

    fun setKeyDown(keyCode: Int, event: KeyEvent?){
        if(!loteNull){

            if (keyCode == 139 || keyCode == 280 || keyCode == 293){
                if (event!!.repeatCount == 0){
                    mview.txt_total.text = "0 / ${responseOrderItem!!.productos!!.size}"
                    readTag()
                }
            }
        }
        else if(!typeSearch!!){ //Si es entrada pero con lotes null
            if (keyCode == 139 || keyCode == 280 || keyCode == 293){
                if (event!!.repeatCount == 0){
                    mview.txt_total.text = "0 / ${responseOrderItem!!.productos!!.size}"
                    readTag()
                }
            }
        }
        else stateButtonSend(false)
    }

    fun setKeyUp(keyCode: Int, event: KeyEvent?){
        stopInventory()
    }

    private fun readTag(){
        if(!readSingle){
            //TODO. Multiple read
            if (mReader!!.startInventoryTag()){
                loopFlag = true
                TagThread().start()
            }else{
                mReader!!.stopInventory()
                coreView.toast("Error de lectura")
            }
        }
        else{
            //todo. individual read
            val strUII: UHFTAGInfo? = mReader!!.inventorySingleTag()
            if(strUII != null){
                val strEPC = strUII.epc
                AddTagReaded(strEPC)
                mSound!!.playSuccess()
            }
            else coreView.toast("No se leyó ningun tag")
        }
    }

    internal class TagThread : Thread() {
        override fun run() {
            var strTid: String
            var strResult: String
            var res: UHFTAGInfo?
            while (loopFlag) {
                res = mReader!!.readTagFromBuffer()
                if (res != null){
                    strTid = res.tid
                    strResult = if (strTid.isNotEmpty() && strTid != "0000000" +
                            "000000000" && strTid != "000000000000000000000000"){
                        "TID:$strTid\n"
                    }else ""

                    val msg: Message = handlerMSJ!!.obtainMessage()
                    msg.obj = strResult + "EPC:" + res.epc + "@" + res.rssi
                    handlerMSJ!!.sendMessage(msg)
                }
            }
        }
    }

    private fun InitHandler(){
        handlerMSJ = object : Handler() {
            override fun handleMessage(msg: Message) {
                val result = msg.obj.toString() + ""
                val strs = result.split("@").toTypedArray()
                val epc = strs[0].split(":").toTypedArray()
                AddTagReaded(epc[1])
            }
        }
    }

    private fun AddTagReaded(tag: String){
        presenter!!.addTag(tag)
        mSound!!.playSuccess()
    }

    private fun stopInventory(){
        if (loopFlag) {
            loopFlag = false
            if(!mReader!!.stopInventory()) coreView.toast("Fail stop operation")
        }

        if(!isOffline){
            mview.LY_progressBar_orders.visibility = View.VISIBLE
            mview.LY_alldata.visibility = View.GONE
            presenter!!.searchTag()
        } //Aqui es cuando hacen la lectura teniendo wifi y que se manden los tags al dejar de presionar el gatillo
        //Porque cuando entran al cuarto frio no hay wifi
    }

    override fun onResume(){
        if(mReader == null){
            iinit()
            mSound = SoundPlay(context)
        }
        StartThread()
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(mRunnabale)
    }

    override fun onStop(){

        mSound!!.Release()

        if (mReader != null){
            if(mReader!!.free()){
                mReader = null
                Log.e("okfree", "free")
            }
            else Log.e("fuckfree", "Fuck free")
        }
        super.onStop()
    }


    override fun setItemNameMenu(name: String, item: MenuItem){}
    protected fun playSuccess(){}
    override fun activateReader(){}
}

package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ResponseEtiquetas(
	val response: Response? = null
)

data class Response(
		val total: Int? = null,
		val productos: ResponseProductos? = null
)

data class ResponseProductos(
		val total_con_espacio: Int? = null,
		val total_sin_espacio: Int? = null,
		val con_espacio: ArrayList<ResponseProductosSpace>? = null,
		val sin_espacio: ArrayList<ResponseProductosSpace>? = null
)

data class ResponseProductosSpace(
		val uuid: String? = null,
		val productos_id: Int? = null,
		val espacios_id: Int? = null,
		val almacenes_id: Int? = null,
		val cantidad: Int? = null,
		val lote: String? = null,
		val caducidad: String? = null,
		val activo_id: Int? = null,
		val activo: String? = null,
		val espacio: ResponseEspacio? = null,
		val producto: ResponseProductData? = null,
		val almacen: ResponseAlmacenData? = null
)

data class ResponseEspacio(
		val id: Int? = null,
		val codigo: String? = null,
		val status: String? = null
): Serializable

data class ResponseProductData(
		val id: Int? = null,
		@SerializedName("codigo")
		val articulo: String? = null,
		val nombre: String? = null,
		val fabricante: String? = null
)

data class ResponseAlmacenData(
		val id: Int? = null,
		val codigo: String? = null,
		val nombre: String? = null
)

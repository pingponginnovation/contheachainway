package com.example.pingpongalien.conthea.ModelsRoom

import android.arch.persistence.room.*
import com.example.pingpongalien.conthea.Helpers.HelperDate
import org.jetbrains.annotations.NotNull
import java.util.Date

@Entity(tableName = "ordenes_pendientes")
data class RoomOrdenes(
        @PrimaryKey(autoGenerate = true)
        var id: Int = 0,
        @ColumnInfo(name = "remission_number") var remisionNumber: String?,
        @ColumnInfo(name = "uuid_number") var uuidNumber: String?
)
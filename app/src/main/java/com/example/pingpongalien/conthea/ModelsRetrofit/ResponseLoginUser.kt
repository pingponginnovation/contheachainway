package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResponseLoginUser(
    @SerializedName("apellido_paterno")
    var lastName: String,
    @SerializedName("nombre")
    var name: String,
    @SerializedName("permisos")
    val responsePermisos: ArrayList<ResponseLoginRoles>
)
package com.example.pingpongalien.conthea.Fragments.Settings

import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

class SettingsModel(presenter : Settings.Presenter, val sharedPreferences: PreferencesController): Settings.Model {

    var presenter: Settings.Presenter? = null
    val TAG = "SettingsModel"
    var preferences: PreferencesController? = null

    init {
        this.presenter = presenter
        this.preferences = sharedPreferences
    }

    override fun saveUrl(url: String){

        if(url.isNotEmpty()){

            preferences!!.saveUrl(url)
            presenter!!.showSaveUrl()
        }
        else presenter!!.showErrorUrl()
    }
}
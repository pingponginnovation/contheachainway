package com.example.pingpongalien.conthea.Fragments.InitialAdjustment

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Context.AUDIO_SERVICE
import android.media.AudioManager
import android.media.SoundPool
import android.os.*
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView.Companion.mContext
import com.example.pingpongalien.conthea.Adapters.AdapterPotency
import com.example.pingpongalien.conthea.Adapters.AdapterTagsRegister
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.Models.TagsForRegister
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import com.example.pingpongalien.conthea.SoundPlay
import com.rscja.deviceapi.RFIDWithUHFUART
import com.rscja.deviceapi.entity.UHFTAGInfo
import kotlinx.android.synthetic.main.dialog_select_potencia.*
import kotlinx.android.synthetic.main.fragment_alta_tag.*
import org.jetbrains.anko.toast
import java.util.HashMap

class AltaTagView: ReaderFragment(), AltaTag.View{

    var presenter: AltaTag.Presenter? = null

    private var readSingle = false
    private var alguno_leido = false
    private var aux = false
    lateinit var txt_total: TextView
    var adapterTagsRegist: AdapterTagsRegister? = null
    var builder: AlertDialog.Builder? = null
    var idTxtMsj: TextView? = null
    var img: ImageView? = null
    var viewDialog: View? = null
    private var dialog: Dialog? = null
    //private var am: AudioManager? = null
    //private var volumnRatio = 0f
    //private var soundMap = HashMap<Int, Int>()
    //private var soundPool: SoundPool? = null
    private var mSound: SoundPlay? = null
    companion object{
        private val POWER_LEVEL = 20
        var loopFlag = false
        var mReader: RFIDWithUHFUART? = null
        var handler: Handler? = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        val view = inflater.inflate(R.layout.fragment_alta_tag, container, false)
        presenter = AltaTagPresenter(this, context, PreferencesController(context))
        iinit()
        mSound = SoundPlay(context)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        InitHandler()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_initial_adjustment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.item_potency -> showPotency()
            R.id.item_type_read -> presenter!!.setModeRead(item.title.toString(), item)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setModeRead(mode: Boolean){
        readSingle = mode
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        item.title = name
    }

    override fun SetAmount(amount: Int){
        //txt_total.text = amount.toString() //Cantidad de tags leidos
    }

    override fun SetAdapterRegister(tags: ArrayList<TagsForRegister>, tagsOff: ArrayList<String>, inOrderOut: String){
                                                                                //Here we fill up the adapter with the tags available to change location
        if(!tags.isEmpty()){

            LY_progressBar_altatag.visibility = View.GONE
            LY_Recycler2.visibility = View.VISIBLE
            ly_Main2.visibility = View.GONE
            adapterTagsRegist = AdapterTagsRegister(tags, tagsOff, inOrderOut, presenter!!, activity, this)
            idRecyclerTags2.adapter = adapterTagsRegist
            idRecyclerTags2.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }
        else{
            LY_Recycler2.visibility = View.GONE
            ly_Main2.visibility = View.VISIBLE
            LY_progressBar_altatag.visibility = View.GONE
            context.toast("No hay productos que mostrar")
        }
    }

    override fun SuccessTagsRegister(resp: String){
        CreateDialogRespuesta(resp, 1)
    }

    override fun UnSuccessTagsRegister(resp: String){
        CreateDialogRespuesta(resp, -1)
    }

    fun CreateDialogRespuesta(msj: String, aux: Int){

        this.builder = AlertDialog.Builder(context)
        val layoutInflater = LayoutInflater.from(context)
        this.viewDialog = layoutInflater.inflate(R.layout.dialog_possitive_message, null)
        this.idTxtMsj = viewDialog!!.findViewById(R.id.idTxtMsj)
        this.img = viewDialog!!.findViewById(R.id.im_circle)

        if(aux == -1) img!!.setImageResource(R.drawable.error)
        else img!!.setImageResource(R.drawable.correct)

        idTxtMsj!!.text = msj

        this.builder!!.setView(viewDialog).setPositiveButton(getString(R.string.aceptar)) { dialog, some->

            val ft = fragmentManager.beginTransaction()
            if(Build.VERSION.SDK_INT >= 26) ft.setReorderingAllowed(false)
            ft.detach(this).attach(this).commit()
        }
        this.builder!!.setCancelable(false)
        this.builder!!.show()
    }

    override fun Unsuccess(error: String){
        context.toast(error)
        LY_progressBar_altatag.visibility = View.GONE
        ly_Main2.visibility = View.VISIBLE
        LY_Recycler2.visibility = View.GONE
    }

    private fun showPotency(){
        val arrayPotency = arrayListOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                21,22,23,24,25,26,27,28,29,30)
        dialog = Dialog(context)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.dialog_select_potencia)

        dialog!!.rclv_potencias.addItemDecoration(SimpleDividerItemDecoration(context))
        dialog!!.rclv_potencias.apply{
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            setHasFixedSize(true)
            adapter = AdapterPotency(arrayPotency){Setpotencia(it)}
        }

        dialog!!.btn_close_dialog!!.setOnClickListener{
            dialog!!.dismiss()
        }
        dialog!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog!!.show()
    }

    private fun Setpotencia(num: Int){
        if(dialog != null) dialog!!.dismiss()
        if(mReader!!.setPower(num)) context.toast("Potencia ajustada correctamente a: $num")
        else context.toast("Error en establecer potencia")
    }

    override fun initReader(){}
    override fun activateReader(){}

    fun iinit(){
        try {
            mReader = RFIDWithUHFUART.getInstance()
            if (mReader != null){

                if (mReader != null) {
                    someTask().MyCustomTask(context)
                    someTask().execute()
                }
            }
        } catch (e: Exception) {
            context.toast(e.message!!)
        }
    }

    class someTask: AsyncTask<Void, Void, Boolean>() {

        var mypDialog: ProgressDialog? = null

        override fun doInBackground(vararg params: Void?): Boolean?{
            return mReader!!.init()
        }

        fun MyCustomTask(context: Context) {
            mContext = context
        }

        override fun onPreExecute() {
            super.onPreExecute()

            mypDialog = ProgressDialog(mContext)
            mypDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            mypDialog!!.setMessage("Iniciando RFID...")
            mypDialog!!.setCanceledOnTouchOutside(false)
            mypDialog!!.show()
            //mContext!!.progressDialog("Please wait a minute.", "Downloading…")
            //mContext!!.indeterminateProgressDialog("Fetching the data…")
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            mypDialog!!.cancel()
            if (!result!!) {
                Toast.makeText(mContext, "init fail", Toast.LENGTH_SHORT).show()
            }
            else{
                try {
                    mReader!!.power = POWER_LEVEL
                    Log.e("Power", mReader!!.power.toString())
                }catch(e: Exception){}
            }
        }
    }

    fun setKeyDown(keyCode : Int, event: KeyEvent){
        alguno_leido = false
        if (keyCode == 139 || keyCode == 280 || keyCode == 293){
            if (event.repeatCount == 0) readTag()
        }
    }

    fun setKeyUp(keyCode : Int, event: KeyEvent){
        if(alguno_leido){
            LY_progressBar_altatag.visibility = View.VISIBLE
            ly_Main2.visibility = View.GONE
            LY_Recycler2.visibility = View.GONE
        }
        else Toast.makeText(context!!, "No se ha leído ningun Tag", Toast.LENGTH_LONG).show()
        aux = true
        stopInventory()
    }

    private fun readTag(){
        alguno_leido = true

        if(!readSingle){
            //TODO. Multiple read
            if (mReader!!.startInventoryTag()){
                loopFlag = true
                TagThread().start()
            }else{
                mReader!!.stopInventory()
                context.toast("Error de lectura")
            }
        }
        else{
            //todo. individual read
            val strUII: UHFTAGInfo? = mReader!!.inventorySingleTag()
            if(strUII != null){
                val strEPC = strUII.epc
                AddTagReaded(strEPC)
                mSound!!.playSuccess()
            }
            else context.toast("No se leyó ningun tag")
        }
    }

    internal class TagThread : Thread() {
        override fun run() {
            var strTid: String
            var strResult: String
            var res: UHFTAGInfo?
            while (loopFlag) {
                res = mReader!!.readTagFromBuffer()
                if (res != null){
                    strTid = res.tid
                    strResult = if (strTid.isNotEmpty() && strTid != "0000000" +
                            "000000000" && strTid != "000000000000000000000000"){
                        "TID:$strTid\n"
                    }else ""

                    val msg: Message = handler!!.obtainMessage()
                    msg.obj = strResult + "EPC:" + res.epc + "@" + res.rssi
                    handler!!.sendMessage(msg)
                }
            }
        }
    }

    private fun InitHandler(){
        handler = object : Handler() {
            override fun handleMessage(msg: Message) {
                val result = msg.obj.toString() + ""
                val strs = result.split("@").toTypedArray()
                val epc = strs[0].split(":").toTypedArray()

                AddTagReaded(epc[1])
            }
        }
    }

    private fun AddTagReaded(tag: String){
        presenter!!.AddTag(tag)
        mSound!!.playSuccess()
    }

    private fun stopInventory(){
        if (loopFlag) {
            loopFlag = false
            if(!mReader!!.stopInventory()) context.toast("Fail stop operation")
        }

        if(aux || !readSingle){
            presenter!!.SendToRetrofit()
            aux = false
        }
    }

    /*private fun initSound() {
        soundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 5)
        soundMap.put(1, soundPool!!.load(context, R.raw.beep, 1))
        soundMap.put(2, soundPool!!.load(context, R.raw.fail, 1))
        am = context.getSystemService(AUDIO_SERVICE) as AudioManager
    }

    fun playSound(id: Int) {
        val audioMaxVolumn = am!!.getStreamMaxVolume(AudioManager.STREAM_MUSIC).toFloat()
        val audioCurrentVolumn = am!!.getStreamVolume(AudioManager.STREAM_MUSIC).toFloat()
        volumnRatio = audioCurrentVolumn / audioMaxVolumn
        try {
            soundPool!!.play(soundMap[id]!!, volumnRatio, volumnRatio, 1, 0, 1f)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }*/

    protected fun playSuccess(){}

    override fun onResume(){
        if(mReader == null){
            iinit()
            mSound = SoundPlay(context)
        }
        super.onResume()
    }

    override fun onStop(){

        mSound!!.Release()

        if (mReader != null){
            if(mReader!!.free()){
                mReader = null
                Log.e("okfree", "free")
            }
            else Log.e("fuckfree", "Fuck free")
        }
        super.onStop()
    }
}
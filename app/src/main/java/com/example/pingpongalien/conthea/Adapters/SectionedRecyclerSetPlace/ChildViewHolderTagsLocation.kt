package com.example.pingpongalien.conthea.Adapters.SectionedRecyclerSetPlace

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.example.pingpongalien.conthea.R

class ChildViewHolderTagsLocation(itemView: View) : RecyclerView.ViewHolder(itemView){
    var lote: TextView = itemView.findViewById(R.id.txt_lote_child)
    var almacen: TextView = itemView.findViewById(R.id.txt_almacen_child)
}
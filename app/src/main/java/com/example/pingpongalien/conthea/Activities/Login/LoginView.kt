package com.example.pingpongalien.conthea.Activities.Login.Login

import android.content.Context
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Fragments.Settings.SettingsView
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.io.IOException

class LoginView: AppCompatActivity(), Login.View{

    private val TAG = "LoginView"
    private var presenter: Login.Presenter? = null
    private var preferences: PreferencesController? = null
    private var tokenFirebase: String? = null
    private var set: HashSet<String> = HashSet()
    private var adaptador_dropdownList: ArrayAdapter<String>? = null

    companion object{
        var uniqueID = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        adaptador_dropdownList = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line)
        preferences = PreferencesController(applicationContext)
        presenter = LoginPresenter(this, preferences!!, applicationContext)
        eventsView()
        verifyStateSession()
        isFirstTimeInApp()
        GetTokenFirebase()
        GetEmails()
    }

    fun GetTokenFirebase(){
        intent.extras?.let{
            for (key in it.keySet()){
                val value = intent.extras?.get(key)
                Log.e("DataExtra", value.toString())
            }
        }
        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener{task ->
                    if (!task.isSuccessful){
                        Log.e("InstanceID failed", task.exception.toString())
                        return@OnCompleteListener
                    }
                    tokenFirebase = task.result?.token!!
                })
    }

    private fun GetEmails(){
        if(!preferences!!.getEmails().isNullOrEmpty()){
            val emails = preferences!!.getEmails()
            emails!!.forEach{
                set.add(it)
            }
            ConfigAutocompleteEmails(set)
        }
    }

    fun ConfigAutocompleteEmails(emails: HashSet<String>){
        emails.forEach{
            adaptador_dropdownList!!.add(it)
        }
        edt_user.threshold = 1
        edt_user.setAdapter(adaptador_dropdownList)
    }

    override fun makeLogin(){
        pb_login.visibility = View.VISIBLE
        presenter!!.makeLogin(edt_user.text.toString(), edt_password.text.toString(), chk_session_started.isChecked, preferences!!, tokenFirebase!!)
    }

    override fun showErrorLogin(message:String){
        pb_login.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun ErrorServer(msj: String){
        pb_login.visibility = View.GONE
        toast(msj)
    }

    override fun showCore(){
        VerifyToSaveEmails()
        uniqueID = Build.SERIAL
        startActivity<CoreView>()
        finish()
    }

    private fun VerifyToSaveEmails(){

        val email = edt_user.text.toString()

        if(!preferences!!.getEmails().isNullOrEmpty()){
            val emails = preferences!!.getEmails()
            emails!!.forEach{
                set.add(it)
            }

            if(!set.contains(email)){
                set.add(email)
                preferences!!.saveEmails(set)
            }
        }
        else{
            set.add(email)
            preferences!!.saveEmails(set)
        }

        if(!set.isNullOrEmpty()) set.clear()
    }

    override fun verifyStateSession(){
        presenter!!.verifyStateSession(preferences!!)
    }

    fun eventsView(){
        btn_session_start.setOnClickListener{
            makeLogin()
            HideKeyboard()
        }
        txt_change_url.setOnClickListener { showSettingsUrl(false) }
    }

    fun isFirstTimeInApp(){
        if(preferences!!.getUrl() == "") showSettingsUrl(true)
    }

    fun showSettingsUrl(isFirstTime: Boolean){

            //ESTE ES PARA MOSTRAR LA VISTA DE CONFIGURACION DE LA URL

        btn_session_start.visibility = View.GONE
        val fragment: Fragment = SettingsView.createSettingsView(true, isFirstTime)

        val manager = supportFragmentManager
        manager.beginTransaction().add(R.id.cl_login, fragment).commit()
    }

    private fun HideKeyboard(){
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if(currentFocus == null) return
        if(currentFocus!!.windowToken == null) return
        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }
}

package com.example.pingpongalien.conthea.Activities.DetailVerify

import android.content.Context
import android.util.Log
import android.view.MenuItem
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyLocalView.Companion.cantidadConteha
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyLocalView.Companion.espacioID
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyLocalView.Companion.lote
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyLocalView.Companion.productoID
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView.Companion.almacen
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView.Companion.catalogoo
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView.Companion.lotee
import com.example.pingpongalien.conthea.Activities.Login.Login.LoginView
import com.example.pingpongalien.conthea.Activities.Login.Login.LoginView.Companion.uniqueID
import com.example.pingpongalien.conthea.ModelsRetrofit.*
import com.example.pingpongalien.conthea.ModelsRoom.RoomInventario
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.example.pingpongalien.conthea.Room.RoomController
import com.google.gson.Gson
import org.jetbrains.anko.doAsync

class DetailVerifyModel(presenter : DetailVerify.Presenter, context: Context, from: String): DetailVerify.Model {

    var presenter: DetailVerify.Presenter? = null
    var aTagsForVerify = arrayListOf<String>()
    var checkTags = arrayListOf<String>()
    var product: ResponseCatalogItem? = null
    var appContext: Context
    var n = 0
    var aux = 0
    private var from: String = ""
    private var times = 0
    private var totalIntelisis: Int? = null

    init {
        this.presenter = presenter
        this.appContext = context
        this.from = from
    }

    override fun searchTag() {
        requestVerifyTags()
    }

    override fun addTagNoDB(tag: String) {
        if (aTagsForVerify.isEmpty()) aTagsForVerify.add(tag)
        else if (!aTagsForVerify.contains(tag)) aTagsForVerify.add(tag)
    }

    override fun GetAmountIntelisis(request: String) {
        val retrofit = RetrofitController(appContext)
        retrofit.ajusteInventario(request, this)
    }

    override fun SuccessAmountIntelisis(resp: ResponseAjuste) {
        presenter!!.SetAmountIntelisis(resp.total.toInt())
        totalIntelisis = resp.total.toInt()
    }

    override fun UnsuccessAmountIntelisis(msj: String) {
        presenter!!.UnsuccessAmountIntelisis(msj)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        if (nameItem.equals("Lectura unica")) {
            presenter!!.setModeRead(true)
            presenter!!.setItemNameMenu("Lectura continua", item)
        } else {
            presenter!!.setModeRead(false)
            presenter!!.setItemNameMenu("Lectura unica", item)
        }
    }

    override fun addTagForVerify(tag: String) {
        if (aTagsForVerify.isEmpty()) {
            aTagsForVerify.add(tag)
            SaveTagToDB(tag)
        } else if (!aTagsForVerify.contains(tag)) {
            aTagsForVerify.add(tag)
            SaveTagToDB(tag)
        }
    }

    private fun SaveTagToDB(tag: String) {
        if (!tagsFromDB.contains(tag)) {

            Log.e("save", "Save tags")

            doAsync {
                val database = RoomController.getInstance()
                //database.daoOrdenes().deleteOrdenesFromDB()
                val roomOrden = RoomInventario(0, null, null, null, null)
                roomOrden.lote = lotee
                roomOrden.almacen = almacen
                roomOrden.catalogo = catalogoo
                roomOrden.uuidNumber = tag
                database.daoInventario().InsertTags(roomOrden)
            }
        }
    }

    var tagsFromDB = arrayListOf<String>()
    override fun LoadLastStateOrder(tags: ArrayList<String>) {
        tagsFromDB = tags
        tags.forEach {
            aTagsForVerify.add(it)
        }
    }

    override fun successVerify(respuesta: ResponseVerify) {

        Log.e("response", respuesta.toString())

        if (checkTags.isEmpty()) {

            n = respuesta.response!!.size
            checkTags = respuesta.response
        } else {
            for (i in respuesta.response!!) {
                if (!checkTags.contains(i)) {
                    aux = 1
                    times += 1 //Increment every time that a tag is added to the array
                    checkTags.add(i)
                }
            }

            if (aux == 1) {
                n += times
                aux = 0
                times = 0
            }
        }

        if (from == "VERIFY_LOCAL") {
            setColor(n, cantidadConteha)
            //presenter!!.setAmount(respuesta.response.size, cantidadConteha) //(num de tags leidos, total conthea)
        } else {
            setColor(n, product!!.cantidad!!)
            presenter!!.setAmount(respuesta.response.size, product!!.cantidad!!) //(num de tags leidos, total conthea)
            presenter!!.setCorrectTags(respuesta.response)
        }

        presenter!!.setTotal(n.toString())
        cleanTagsForVerify()
    }

    override fun successVerifyLocal(response: ResponseVerifyLocal) {

        Log.e("responseLocal", response.toString())
        Log.e("checktagsLocal", checkTags.toString())

        if (checkTags.isEmpty()) {

            n = response.response!!.size
            checkTags = response.response
        } else {
            for (i in response.response!!) {
                if (!checkTags.contains(i)) {
                    aux = 1
                    times += 1 //Increment every time that a tag is added to the array
                    checkTags.add(i)
                }
            }

            if (aux == 1) {
                n += times
                aux = 0
                times = 0
            }
        }

        setColor(n, cantidadConteha)
        presenter!!.setTotal(n.toString())
        cleanTagsForVerify()
    }

    override fun clearView(){
        if(checkTags.isNotEmpty()) checkTags.clear()
    }

    override fun unsuccessVerify(error: String){
        presenter!!.unsuccessVerify(error)
    }

    override fun setCatalog(product: ResponseCatalogItem){
        this.product = product
        presenter!!.setAmount(0, product.cantidad!!)
    }

    fun requestVerifyTags(){
        val retrofit = RetrofitController(appContext)
        if(from == "VERIFY_LOCAL") retrofit.verifyUbicationLocal(createVerifyLocal(), this, from)
        else retrofit.verifyUbication(createVerify(), this)
    }

    fun createVerifyLocal(): RequestVerify{
        if(aTagsForVerify.isNullOrEmpty()) aTagsForVerify.add("") //Agregamos vacio para que me pueda responder el server con success
        val j = Gson()
        val ar = j.toJson(RequestVerify(uniqueID, espacioID, productoID, lote, aTagsForVerify))
        Log.e("req", ar.toString())
        return RequestVerify(uniqueID, espacioID, productoID, lote, aTagsForVerify)
    }

    fun createVerify(): RequestVerify{
        if(aTagsForVerify.isNullOrEmpty()) aTagsForVerify.add("") //Agregamos vacio para que me pueda responder el server con success
        val j = Gson()
        val ar = j.toJson(RequestVerify(uniqueID, product!!.espacio_actual!!.id, product!!.producto!!.id, product!!.lote, aTagsForVerify))
        Log.e("json", ar.toString())
        return RequestVerify(uniqueID, product!!.espacio_actual!!.id, product!!.producto!!.id, product!!.lote, aTagsForVerify)
    }

    fun cleanTagsForVerify(){
        aTagsForVerify.clear()
    }

    fun setColor(amount : Int, total : Int){
        if(from == "VERIFY_LOCAL"){
            if (amount == total) presenter!!.setColorComplete()
            else presenter!!.setColorIncomplete()
        }
        else{
            if (amount == total && amount == totalIntelisis) presenter!!.setColorComplete()
            else presenter!!.setColorIncomplete()
        }
    }
}
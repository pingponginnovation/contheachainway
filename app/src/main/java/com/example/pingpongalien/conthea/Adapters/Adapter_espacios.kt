package com.example.pingpongalien.conthea.Adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.pingpongalien.conthea.R

class Adapter_espacios: RecyclerView.Adapter<Adapter_espacios.ViewHolder>(){

    private var espacios: ArrayList<String>? = null
    private lateinit var listener: (String) -> Unit?

    fun Adapter_espacios(prods: ArrayList<String>, listener: (String) -> Unit){
        espacios = prods
        this.listener = listener
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val titulo = holder.itemView.findViewById(R.id.txt_nombre) as TextView

        titulo.text = espacios!![position]
        holder.itemView.setOnClickListener{
            listener(espacios!![position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_select_product, parent, false))
    }

    override fun getItemCount(): Int {
        return espacios!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}

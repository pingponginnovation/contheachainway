package com.example.pingpongalien.conthea.Fragments.InitialAdjustment

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

class InitialAdjustmentPresenter(myview: InitialAdjustment.View, context: Context, preferences: PreferencesController): InitialAdjustment.Presenter{

    val model: InitialAdjustment.Model = InitialAdjustmentModel(this, context, preferences)
    val view: InitialAdjustment.View = myview

    override fun AddTag(tag: String){
        model.AddTag(tag)
    }

    override fun SendToRetrofit(){
        model.SendToRetrofit()
    }

    override fun SetAmount(total: Int){
        view.SetAmount(total)
    }

    override fun Unsuccess(error: String) {
        view.Unsuccess(error)
    }

    override fun setModeRead(mode: Boolean) {
        view.setModeRead(mode)
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        view.setItemNameMenu(name, item)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        model.setModeRead(nameItem, item)
    }
}
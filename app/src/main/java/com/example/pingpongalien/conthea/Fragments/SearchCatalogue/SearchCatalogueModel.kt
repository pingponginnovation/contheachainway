package com.example.pingpongalien.conthea.Fragments.SearchCatalogue

import android.content.Context
import android.util.Log
import com.example.pingpongalien.conthea.Adapters.ExpandableCatalogue.ParentCatalogue
import com.example.pingpongalien.conthea.Helpers.HelperDate
import com.example.pingpongalien.conthea.Models.ProductChild
import com.example.pingpongalien.conthea.ModelsRetrofit.RequestDetailTag
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalog
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalogItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDetailTag
import com.example.pingpongalien.conthea.ModelsRoom.RoomCatalogItem
import com.example.pingpongalien.conthea.ModelsRoom.RoomCatalogPlace
import com.example.pingpongalien.conthea.ModelsRoom.RoomCatalogProduct
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.example.pingpongalien.conthea.Room.RoomController
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class SearchCatalogueModel(presenter : SearchCatalogue.Presenter, context : Context): SearchCatalogue.Model {
    var presenter: SearchCatalogue.Presenter? = null

    val TAG = "SearchCatalogueModel"
    var helperDate: HelperDate
    var appContext: Context
    private val parentArray: ArrayList<ParentCatalogue> = ArrayList()

    init {
        this.presenter = presenter
        this.helperDate = HelperDate()
        this.appContext = context
    }

    fun requestCataloge(catalogo: String, name: String, lote: String){
        val retrofit = RetrofitController(appContext)
        Log.e("lote", lote)
        retrofit.getCataloge(this, catalogo, name, lote)
    }

    override fun getCatalogue(catalogo: String, name: String, lote: String) {
        requestCataloge(catalogo, name, lote)
    }

    override fun successGetCataloge(responseCatalog: ResponseCatalog){
        if(responseCatalog.response!!.isNotEmpty()){

            responseCatalog.response.forEach{
                Log.e("response", it.toString())
            }

            doAsync{
                val database = RoomController.getInstance()
                database.daoController().deleteCatalog()
            }
            presenter!!.SuccessCatalogue()
            ConfigProducts(responseCatalog.response)
            presenter!!.setProductos(parentArray)
        }
        else presenter!!.setMessage("No se encontraron productos")
    }

    private fun ConfigProducts(productos: List<ResponseCatalogItem?>?){

        if(parentArray.isNotEmpty()) parentArray.clear()

        productos?.forEach{

            val arrayChilds: ArrayList<ProductChild> = arrayListOf() //Array de hijos

            val almacen = it?.almacen?.codigo!! + " " + it.almacen.nombre!!
            val datos = it.producto?.nombre!!.split(":").toTypedArray()
            val catalogo = datos[0]
            val description = datos[1]

            val child = ProductChild("", description, it.lote!!, it.caducidad, almacen, it.espacio_actual?.codigo, it.producto.fabricante, it.cantidad,
            catalogo, it.espacio_actual?.id, it.producto.id) //Creamos hijo
            arrayChilds.add(child) //Añadimos hijo al array

            if(parentArray.isNullOrEmpty()) parentArray.add(ParentCatalogue(arrayChilds, catalogo, it.lote)) //Se crea el primer padre
            else{

                var bandera = -1

                parentArray.forEach{parent ->

                    arrayChilds.forEach{child ->

                        if(parent.catalogoText == child.catalogue && parent.lote == child.lote){ //Agregamos nuevo hijo a padre existente en el array
                            val newChild = ProductChild("", child.description, child.lote, child.expiration, child.storage, child.ubication, child.maker,
                            child.cantidad, child.catalogue, child.espacio_id, child.producto_id)
                            parent.childList!!.add(newChild)
                            bandera = 1
                        }
                    }
                }

                if(bandera == -1) parentArray.add(ParentCatalogue(arrayChilds, catalogo, it.lote)) //Creamos nuevo padre, en caso de no existir
            }
        }

        /*parentArray.forEach {
            Log.e("Parent", it.catalogo)
            Log.e("Lote", it.lote)
            it.mChildrenList?.forEach { item ->
                item as ProductChild
                Log.e("hijos", item.toString())
            }
        }*/
    }

    override fun unsuccessGetCataloge(error: String) {
        presenter!!.unsuccessGetCataloge(error)
    }

    override fun successDetailTag(responseCatalog: ResponseDetailTag) {
        searchProduct(responseCatalog.response.id.toString(), 4)
    }

    override fun unsuccessDetailTag(error: String) {
        presenter!!.setMessage(error)
    }

    override fun searchProduct(search: String, typeSearch: Int) {
        if(search.isNotEmpty()) getProducts(search, typeSearch)
        else presenter!!.unsuccessGetCataloge("Agrega un producto a buscar")
    }

    override fun setComponentsCorrect(typeSearch: Int) {
        when (typeSearch) {
            3 -> {
                presenter!!.visibilityTextDate(true)
                presenter!!.visibilitySearch(false)
            }
            4 -> {
                presenter!!.visibilityRfid()
            }
            else -> {
                presenter!!.visibilityTextDate(false)
                presenter!!.visibilitySearch(true)
            }
        }

    }

    override fun searchProductByExpiration(startDate : String, finishDate : String){
        doAsync {
            val database = RoomController.getInstance()
            searchProduct(database, database.daoController().getProductsByExpiration(helperDate.convertStringToDate(startDate), helperDate.convertStringToDate(finishDate)))
        }
    }
    //FALSE startDate   TRUE finalDate
    override fun formatDate(year: Int, month: Int, day: Int, type: Boolean) {
        var monthInner = month
        monthInner ++
        lateinit var monthCorrect : String
        lateinit var dayCorrect : String
        if(day.toString().length == 1)
            dayCorrect = "0$day"
        else
            dayCorrect = day.toString()
        if(monthInner.toString().length == 1)
            monthCorrect = "0$monthInner"
        else
            monthCorrect = monthInner.toString()
        if(type)
            presenter!!.setFinishDate("$year-$monthCorrect-$dayCorrect")
        else
            presenter!!.setStartDate("$year-$monthCorrect-$dayCorrect")
    }

    override fun shouldSearch(uuid: String, typeSearch : Int) {
        if(typeSearch == 4){
            presenter!!.playSuccess()
            requestDetailTag(uuid)
        }
    }

    fun requestDetailTag(uuid: String){
        val retrofit = RetrofitController(appContext)
        retrofit.detailTag(RequestDetailTag(uuid), this)
    }

    fun getProducts(search: String, typeSearch: Int) {
        doAsync {
            val realSearch = "$search%"
            Log.e("realsearch", realSearch)
            val database = RoomController.getInstance()
            searchProduct(database, filterSearch(database, realSearch, typeSearch))
        }
    }

    fun searchProduct(database: RoomController, aNames : List<String>){
        /*doAsync {
            val aParent: ArrayList<ProductParent> = ArrayList()
            aNames.forEach { name ->
                val aCatalogue = database.daoController().getCatalogesByName(name)
                val aChild: ArrayList<ProductChild> = ArrayList()
                aCatalogue.forEach { innerProduct ->
                    val product = ProductChild(
                            innerProduct.espacioActual!!.codigoLugar!!,
                            helperDate.convertDateToString(innerProduct.caducidad!!),
                            innerProduct.producto!!.fabricante!!,
                            innerProduct.lote!!,
                            innerProduct.cantidad!!)
                    aChild.add(product)
                }
                aParent.add(ProductParent(name, filterSearchInDatabase(aChild)))
            }
            uiThread {
                aParent.forEach{
                    Log.e("parent", it.name)
                }
                if (aParent.size > 0) presenter!!.setProducts(aParent)
                else presenter!!.setMessage("Busqueda sin resultado")
            }
        }*/
    }

    fun filterSearchInDatabase(aChild : ArrayList<ProductChild>): ArrayList<ProductChild>{
        val aDataNoRepeat = HashSet<ProductChild>()
        aDataNoRepeat.addAll(aChild)
        aChild.clear()
        aChild.addAll(aDataNoRepeat)
        return aChild
    }

    fun filterSearch(database: RoomController, search: String, typeSearch: Int): List<String>{
        return when(typeSearch){
            0 -> getProductsByName(database, search)
            1 -> getProductsByMaker(database, search)
            2 -> getProductsByLote(database, search)
            else -> getProductsById(database, search)
        }
    }

    fun getProductsById(database: RoomController, search: String): List<String>{
        return database.daoController().getProductsById(search)
    }

    fun getProductsByName(database: RoomController, search: String): List<String>{
        return database.daoController().getProductsByName(search)
    }

    fun getProductsByMaker(database: RoomController, search: String): List<String>{
        return database.daoController().getProductsByMaker(search)
    }

    fun getProductsByLote(database: RoomController, search: String): List<String>{
        return database.daoController().getProductsByLote(search)
    }

    fun saveCatalogInDatabase(responseCatalog: ResponseCatalog){
        doAsync {
            val database = RoomController.getInstance()
            database.daoController().deleteCatalog()
            responseCatalog.response!!.forEach { catalog ->
                val roomCatalog = RoomCatalogItem(null, null, null,
                        null, null, null)

                if(catalog!!.espacio_actual != null)
                    roomCatalog.espacioActual = RoomCatalogPlace(catalog.espacio_actual!!.id,
                            catalog.espacio_actual.codigo)
                else roomCatalog.espacioActual = RoomCatalogPlace(0, "Sin ubicación")

                roomCatalog.cantidad = catalog.cantidad
                roomCatalog.lote = catalog.lote
                roomCatalog.caducidad = helperDate.convertStringToDate(catalog.caducidad)
                roomCatalog.producto = RoomCatalogProduct(catalog.producto!!.id, catalog.producto.codigo, catalog.producto.fabricante, catalog.producto.nombre)
                database.daoController().insertCatalog(roomCatalog)
            }
        }
    }
}
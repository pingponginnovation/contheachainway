package com.example.pingpongalien.conthea.Adapters.ExpandableUbications;

import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.example.pingpongalien.conthea.Models.ProductChild;
import java.io.Serializable;
import java.util.List;

public class ParentUbication implements Parent<ProductChild>{

    List<ProductChild> childList;
    String catalogoText;
    Integer active;

    public ParentUbication(List<ProductChild> childList, String catalogo, Integer active) {
        this.childList = childList;
        this.catalogoText = catalogo;
        this.active = active;
    }

    public String getCatalogoText() {
        return catalogoText;
    }

    @Override
    public List<ProductChild> getChildList() {
        return childList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
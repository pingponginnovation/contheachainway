package com.example.pingpongalien.conthea.Application

import android.app.Application
import android.util.Log
import com.example.pingpongalien.conthea.Room.RoomController

class Application: Application() {
    override fun onCreate() {
        super.onCreate()
        RoomController.getAppDatabase(applicationContext)
    }
}
package com.example.pingpongalien.conthea.Fragments.VerifyInventory

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.example.pingpongalien.conthea.R
import kotlinx.android.synthetic.main.fragment_verify_inventory.*
import org.jetbrains.anko.toast
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Window
import android.widget.SimpleAdapter
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Activities.Login.Login.LoginView
import com.example.pingpongalien.conthea.Adapters.Adapter_productos
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalogItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseProducts
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import kotlinx.android.synthetic.main.dialog_nomatch_products.*
import kotlinx.android.synthetic.main.dialog_select_product.*
import kotlinx.android.synthetic.main.dialog_select_product.autocomplete_prods
import kotlinx.android.synthetic.main.fragment_verify_inventory.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity

class VerifyInventoryView(val core: CoreView): Fragment(), VerifyInventory.View {

    var preferences: PreferencesController? = null
    private var presenter: VerifyInventory.Presenter? = null
    private var adaptador_dropdownList: ArrayAdapter<String>? = null
    private var productos: List<ResponseProducts>? = null
    private var mAdapter: Adapter_productos = Adapter_productos()
    private var dialog: Dialog? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        val view = inflater!!.inflate(R.layout.fragment_verify_inventory, container, false)

        preferences = PreferencesController(context)
        presenter = VerifyInventoryPresenter(this, context)
        presenter!!.getProducts()
        adaptador_dropdownList = ArrayAdapter(context!!, android.R.layout.simple_dropdown_item_1line)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        core.changeTitleToolbar("Inventario cíclico")
    }

    override fun setProducts(aProducts: List<ResponseProducts>){

        productos = aProducts

        aProducts.forEach{
            adaptador_dropdownList!!.add(it.nombre)
        }
        eventsView()
    }

    override fun unsuccessProducts(error: String) {
        if(error == "Unauthorized") showExpiredSession()
        visibilityProgressBar(false)
        activity.toast(error)
    }

    override fun setPlaces(aPlaces : List<Map<String, String>>) { //Carga datos al entrar en la opcion "Inventario ciclico"

        Log.e("LOTES", aPlaces.toString())
        visibilityListView(true)
        visibilityProgressBar(false)
        lv_places.adapter = SimpleAdapter(context, aPlaces, android.R.layout.simple_list_item_2, arrayOf("place", "lote"), intArrayOf(android.R.id.text1, android.R.id.text2));
    }

    override fun showDetailVerify(catalog: ResponseCatalogItem) {
        val intent = Intent(context, DetailVerifyView::class.java)
                .putExtra("catalogue", catalog)
                .putExtra("nameProduct", autocomplete_products.text.toString())
        core.startActivity(intent)
    }

    override fun visibilityWithoutProducts(){
        pb_search_order.visibility = View.GONE
        txt_message.visibility = View.VISIBLE
    }

    private fun showExpiredSession(){
        context.alert{
            title = "Advertencia"
            message = "Tú sesión ha expirado. Inicia sesión nuevamente"
            positiveButton("Aceptar"){
                ShowLoginView()
            }
            isCancelable = false
        }.show()
    }

    private fun ShowLoginView(){
        core.closeSession()
    }

    fun visibilityProgressBar(visibility : Boolean){
        if(visibility) pb_search_order.visibility = View.VISIBLE
        else pb_search_order.visibility = View.GONE
    }

    fun visibilityListView(visibility : Boolean){
        if(visibility) lv_places.visibility = View.VISIBLE
        else lv_places.visibility = View.GONE
    }

    fun eventsView(){

        autocomplete_products?.setOnClickListener{
            autocomplete_products?.isEnabled = false
            ShowProductsInRecycler()
        }

        UpdatePlaces(productos!![0].id!!, productos!![0].nombre!!)

        lv_places.setOnItemClickListener{ parent, view, position, id ->
                    //Click en un elemento de la lista
            presenter!!.getOnePlace(position)
        }
    }

    fun UpdatePlaces(id: Int, nameProduct: String){

        autocomplete_products?.isEnabled = true
        if(dialog != null) dialog!!.dismiss()

        autocomplete_products.setText(nameProduct)

        visibilityListView(false)
        visibilityProgressBar(true)
        presenter!!.getPlaces(id)
    }

    private fun ShowProductsInRecycler(){

        val filterList: ArrayList<ResponseProducts>?
        filterList = ArrayList()

        dialog = Dialog(activity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.dialog_select_product)

        dialog!!.rclv_productos.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        dialog!!.rclv_productos.setHasFixedSize(true)
        dialog!!.rclv_productos.addItemDecoration(SimpleDividerItemDecoration(context))
        mAdapter.Adapter_productos(productos!!, this@VerifyInventoryView)
        dialog!!.rclv_productos.adapter = mAdapter

        dialog!!.autocomplete_prods.threshold = 1

        dialog!!.autocomplete_prods.addTextChangedListener(object: TextWatcher {      //Filtrar RecyclerView

            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                val textlength = dialog!!.autocomplete_prods.text.toString().length
                filterList.clear()

                for (i in productos!!.indices){

                    if (textlength <= productos!![i].nombre!!.length) {

                        if (productos!![i].nombre!!.toLowerCase().trim().contains(dialog!!.autocomplete_prods.text.toString().toLowerCase().trim { it <= ' ' })){
                            filterList.add(productos!![i])
                        }
                    }
                }

                mAdapter.Adapter_productos(filterList, this@VerifyInventoryView)
                mAdapter.notifyDataSetChanged()
            }
        })

        dialog!!.btn_close_dialog!!.setOnClickListener{
            autocomplete_products?.isEnabled = true
            dialog!!.dismiss()
        }

        dialog!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog!!.show()
    }
}

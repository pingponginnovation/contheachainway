package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseAltaTagItems(
        @field:SerializedName("uuid")
        val uuid: String? = null,
        @field:SerializedName("lote")
        val lotee: String? = null,
        @field:SerializedName("articulo")
        val artic: String? = null,
        @field:SerializedName("almacenes")
        val almacen: String? = null
)
package com.example.pingpongalien.conthea.Fragments.InitialAdjustment

import android.view.MenuItem
import com.example.pingpongalien.conthea.Models.TagsForRegister
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponsRegisterTags
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseAltaTag

interface AltaTag{

    interface Model{
        fun AddTag(tag: String)
        fun SendToRetrofit()
        fun Success(response: ResponseAltaTag)
        fun Unsuccess(error: String)
        fun setModeRead(nameItem: String, item: MenuItem)
        fun SendTagsRegister(tags : ArrayList<TagsForRegister>)
        fun UnSuccessTagsRegister(resp: String)
        fun VerifyTagsInAnyOrder(resp: ResponsRegisterTags)
    }

    interface Presenter{
        fun AddTag(tag: String)
        fun SendToRetrofit()
        fun setModeRead(nameItem: String, item: MenuItem)
        fun SendTagsRegister(tags : ArrayList<TagsForRegister>)

        //For the view
        fun Unsuccess(error: String)
        fun SetAmount(total: Int)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun SetAdapterRegister(tags: ArrayList<TagsForRegister>, tagsOff: ArrayList<String>, inOrderOut: String)
        fun SuccessTagsRegister(resp: String)
        fun UnSuccessTagsRegister(resp: String)
    }

    interface View{
        fun SetAmount(amount: Int)
        fun Unsuccess(error: String)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun setModeRead(mode: Boolean)
        fun SetAdapterRegister(tags: ArrayList<TagsForRegister>, tagsOff: ArrayList<String>, inOrderOut: String)
        fun SuccessTagsRegister(resp: String)
        fun UnSuccessTagsRegister(resp: String)
    }
}
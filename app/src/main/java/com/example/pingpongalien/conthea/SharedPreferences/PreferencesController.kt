package com.example.pingpongalien.conthea.SharedPreferences

import android.content.Context
import android.content.SharedPreferences
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseLoginToken
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseLoginUser

class PreferencesController(context:Context){

    val TAG = "PreferencesController"

    val NAME_PREFERENCES = "CONTHEA"
    val TOKEN = "TOKEN"
    val TOKEN_TYPE = "TOKEN_TYPE"
    val TOKEN_REFRESH = "TOKEN_REFRESH"
    val KEEP_SESSION = "KEEP_SESSION"
    val URL = "URL"
    val NAME = "NAME"
    val LAST_NAME = "LAST_NAME"
    val ROLES_USER_ARRAY = hashSetOf<String>()
    val ROLES_KEY = "ROLES"
    val READ_TAGS = "READ_TAGS"
    val SAVED_TAGS = "SAVED_TAGS"
    private var ID_NOTIFICATION = "NOT"
    var hashSetOptions: HashSet<String>? = null
    private var ARRAY_EMAILS = "EMAILS"

    companion object {
        var preferences: SharedPreferences? = null
    }

    init {
        if(preferences == null)
            preferences = context.getSharedPreferences(NAME_PREFERENCES, Context.MODE_PRIVATE)
    }

    fun saveToken(responseToken: ResponseLoginToken){
        val editor = preferences!!.edit()
        editor.putString(TOKEN_TYPE, responseToken.tokenType)    //Original
        editor.putString(TOKEN, responseToken.accessToken)
        editor.putString(TOKEN_REFRESH, responseToken.refreshToken)
        editor.apply()
    }

    fun saveUser(responseUser: ResponseLoginUser){
        val editor = preferences!!.edit()
        editor.putString(NAME, responseUser.name)
        editor.putString(LAST_NAME, responseUser.lastName)
        responseUser.responsePermisos.forEach{
            ROLES_USER_ARRAY.add(it.tipo_permiso)
        }
        editor.putStringSet(ROLES_KEY, ROLES_USER_ARRAY)
        editor.apply()

        GetRoles()
    }

    fun SaveIDNotification(id: Int){
        val editor = preferences!!.edit()
        editor.putString(ID_NOTIFICATION, id.toString())
        editor.apply()
    }

    fun saveSession(sessionState: Boolean){
        val editor = preferences!!.edit()
        editor.putBoolean(KEEP_SESSION, sessionState)
        editor.apply()
    }

    fun saveRead(read: String){
        val editor = preferences!!.edit()
        editor.putString(READ_TAGS, read)
        editor.apply()
    }

    fun saveSaved(saved: String){
        val editor = preferences!!.edit()
        editor.putString(SAVED_TAGS, saved)
        editor.apply()
    }


    fun closeSession(){
        val editor = preferences!!.edit()
        editor.putString(TOKEN, "")
        editor.apply()
    }

    fun getStateSession():Boolean{
        if (preferences!!.getString(TOKEN, "") != "") return preferences!!.getBoolean(KEEP_SESSION, false)
        return false
    }

    fun saveUrl(url : String){

        val editor = preferences!!.edit()
        hashSetOptions = GetHashSet()

        if(hashSetOptions.isNullOrEmpty()){

            val firstHasSet = HashSet<String>()
            firstHasSet.add(url)
            editor.putStringSet("options", firstHasSet)
        }
        else{

            val newSet = HashSet<String>()
            newSet.add(url)
            newSet.addAll(hashSetOptions!!)
            editor.remove("options")
            editor.putStringSet("options", newSet)
        }

        editor.apply()
        SetUrl(url)
    }

    fun GetHashSet(): HashSet<String>?{
        val set = preferences!!.getStringSet("options", null)
        return set as? HashSet<String>
    }

    fun SetUrl(url: String){

        val editor = preferences!!.edit()
        editor.putString(URL, url)
        editor.apply()
    }

    fun getUrl(): String{
        return preferences!!.getString(URL, "")!!
    }

    fun SaveCodeForSetPlace(code: String, pos: Int){
        val edit = preferences!!.edit()
        edit.putString("Code", code)
        edit.putInt("Position", pos)
        edit.apply()
    }

    fun saveEmails(set: HashSet<String>){
        val editor = preferences!!.edit()
        editor.putStringSet(ARRAY_EMAILS, set)
        editor.apply()
    }

    fun GetName() = preferences!!.getString(NAME, "")
    fun GetApellido() = preferences!!.getString(LAST_NAME, "")
    fun getToken() = preferences!!.getString(TOKEN, "")
    fun getRead() = preferences!!.getString(READ_TAGS, "0")
    fun getSaved() = preferences!!.getString(SAVED_TAGS, "0")
    fun getEmails() = preferences!!.getStringSet(ARRAY_EMAILS, null)
    fun GetRoles(): HashSet<String>?{
        val set = preferences!!.getStringSet(ROLES_KEY, null)
        return set as? HashSet<String>
    }
    fun getIDnotification() = preferences!!.getString(ID_NOTIFICATION, "")
}
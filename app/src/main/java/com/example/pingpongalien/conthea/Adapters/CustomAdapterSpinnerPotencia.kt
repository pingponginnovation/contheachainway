package com.example.pingpongalien.conthea.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RadioButton
import android.widget.TextView
import com.example.pingpongalien.conthea.R
import kotlin.collections.ArrayList

class CustomAdapterSpinnerPotencia(context: Context, private val datos: ArrayList<Int>, private val listener: (Int) -> Unit): BaseAdapter(){

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return datos[position]
    }

    override fun getCount(): Int {
        return datos.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val rowView = inflater.inflate(R.layout.item_spinner_potency, parent, false)
        val potencia = rowView.findViewById(R.id.txt_potencia) as TextView

        val data = getItem(position) as Int

        rowView.setOnClickListener{
            listener(data)
        }
        potencia.text = data.toString()
        return rowView
    }
}
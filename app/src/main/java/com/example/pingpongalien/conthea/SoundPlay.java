package com.example.pingpongalien.conthea;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.IOException;

public class SoundPlay {
	private static final String TAG = SoundPlay.class.getSimpleName();

	private MediaPlayer mSucessMediaPlayer = null;
	private MediaPlayer mFailMediaPlayer = null;

	public SoundPlay(Context context) {
		mSucessMediaPlayer = MediaPlayer.create(context, R.raw.beep);
		mSucessMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mFailMediaPlayer = MediaPlayer.create(context, R.raw.fail);
		mFailMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
	}

	public void playSuccess() {

		if(null != mSucessMediaPlayer) {
			Log.e("Beep", "Beep");
			mSucessMediaPlayer.seekTo(0);
			mSucessMediaPlayer.start();
		}
	}

	public void Release(){
		if (mSucessMediaPlayer != null) {
			Log.e("Release", "mediaplayer");
			mSucessMediaPlayer.release();
			mSucessMediaPlayer = null;
		}
	}
}

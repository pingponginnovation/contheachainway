package com.example.pingpongalien.conthea.Adapters.ExpandableUbications;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.example.pingpongalien.conthea.R;

public class ParentUbicationViewHolder extends ParentViewHolder{

    private final TextView txt_catalogo;
    private final TextView txt_lote;

    public ParentUbicationViewHolder(@NonNull View itemView) {
        super(itemView);
        txt_catalogo = itemView.findViewById(R.id.txt_title);
        txt_lote= itemView.findViewById(R.id.txt_lote);
    }

    public void setCatalogo(String catalogo) {
        txt_catalogo.setText(catalogo);
    }

    public TextView getTxtview_catalogo() {
        return txt_catalogo;
    }

    public TextView getTxt_lote() {
        return txt_lote;
    }
}
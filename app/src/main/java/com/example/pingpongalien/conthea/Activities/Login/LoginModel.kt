package com.example.pingpongalien.conthea.Activities.Login.Login

import android.content.Context
import android.util.Log
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseErrorAuth
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseLogin
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

/**
 * Created by AOR on 17/11/17.
 */
class LoginModel(val presenterLogin: Login.Presenter, val sharedPreferences: PreferencesController, context: Context): Login.Model{

    var presenter: Login.Presenter? = null
    var preferences: PreferencesController? = null
    var appContext: Context? = null

    init {
        this.presenter = presenterLogin
        this.preferences = sharedPreferences
        this.appContext = context
    }

    override fun makeLogin(user: String, password: String, stateSession: Boolean, preferences: PreferencesController, token: String){
        if(validateUser(user) && validatePassword(password)) {
            requestLogin(user, password, stateSession, token)
        }
        else presenter!!.errorRequestLogin("Agregue un usuario y/o una contraseña mayor a 6 digitos")
    }

    override fun successRequestLogin(responseSuccess: ResponseLogin, stateSession: Boolean){
        saveOnPreferences(responseSuccess, stateSession)
        presenter!!.successRequestLogin()
    }

    override fun errorRequestLogin(responseError: ResponseErrorAuth){
        responseError.campos?.msj?.let { presenter!!.errorRequestLogin(it) }
    }

    override fun ErrorServer(msj: String) {
        presenter!!.ErrorServer(msj)
    }

    override fun saveKeepSession(keepSession: Boolean, preferences: PreferencesController) {
        this.preferences!!.saveSession(keepSession)
    }

    override fun verifyStateSession(preferences: PreferencesController){
        if(preferences.getStateSession())
            presenter!!.successRequestLogin()
    }

    fun validateUser(user: String):Boolean{
        if(user.isEmpty())
            return false
        return true
    }

    fun validatePassword(password: String):Boolean{
        if (password.length >= 6)
            return true
        return false
    }

    fun requestLogin(user: String, password: String, stateSession: Boolean, tokenFirebase: String){
        val retrofit = RetrofitController(appContext!!)
        retrofit.login(this, user, password, stateSession, tokenFirebase)
    }

    override fun ErrorResponseLogin(resp: String) {
        presenter!!.errorRequestLogin(resp)
    }

    fun saveOnPreferences(responseSuccess: ResponseLogin, stateSession: Boolean){
        Log.e("Roles", responseSuccess.user.responsePermisos.toString())
        this.preferences!!.saveToken(responseSuccess.token)
        this.preferences!!.saveUser(responseSuccess.user)
        this.preferences!!.saveSession(stateSession)
    }
}
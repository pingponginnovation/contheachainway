package com.example.pingpongalien.conthea.Adapters

import android.app.Activity
import android.content.Context
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import com.example.pingpongalien.conthea.Fragments.Unsuscribe.Unsuscribe
import com.example.pingpongalien.conthea.Fragments.Unsuscribe.UnsuscribeView
import com.example.pingpongalien.conthea.Models.TagsForDelete
import com.example.pingpongalien.conthea.R
import kotlinx.android.synthetic.main.fragment_unsuscribe.*

class AdapterTagsDelete(arayTagsDelete: ArrayList<TagsForDelete>, presenter: Unsuscribe.Presenter, vw:
Activity, unsuscribeAct: UnsuscribeView): RecyclerView.Adapter<AdapterTagsDelete.MyViewHolder>(){

    var presenter: Unsuscribe.Presenter? = null
    var arrayTags: ArrayList<TagsForDelete>
    private val inflater: LayoutInflater
    val tagsSelected = ArrayList<TagsForDelete>()
    var activiti: Activity? = null
    var actUnsuscribe: UnsuscribeView? = null
    var allTrue = false

    init {
        Log.e("ejecuciuon", "initt")
        activiti = vw
        actUnsuscribe = unsuscribeAct
        inflater = LayoutInflater.from(activiti)
        arrayTags = arayTagsDelete
        this.presenter = presenter
    }

    inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        var checkBox: CheckBox
        val infoArt: TextView
        val infoLote: TextView
        //val infoCad: TextView
        val infoAlmacen: TextView
        var btnEliminar: Button
        val btnAll: Button
        var totalTags: TextView? = null

        init {
            checkBox = itemView.findViewById(R.id.idCheckBox)
            infoArt = itemView.findViewById(R.id.idTxtArt)
            infoLote = itemView.findViewById(R.id.idTxtLote)
            //infoCad = itemView.findViewById(R.id.idTxtCad)
            infoAlmacen = itemView.findViewById(R.id.idTxtStorage)
            btnEliminar = activiti!!.findViewById(R.id.btnDelete)
            btnAll = activiti!!.findViewById(R.id.idBtnSelect)
            totalTags = activiti!!.findViewById(R.id.txt_total_tags)
        }
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int){

        //Toast.makeText(cntext, "BIND", Toast.LENGTH_LONG).show()

        holder!!.totalTags!!.text = "Tags leídos: ${arrayTags.size}"
        holder.infoArt.setText(arrayTags[position].article)
        holder.infoLote.setText(arrayTags[position].lote)
        //holder.infoCad.setText(arrayTags[position].caducidad)
        holder.infoAlmacen.setText(arrayTags[position].almacen)
        holder.checkBox.tag = position
        Log.e("position", position.toString())
        Log.e("tag checkbox", holder.checkBox.tag.toString())
        holder.checkBox.isChecked = arrayTags[position].selected

        holder.btnEliminar.setOnClickListener{

            Log.e("Selected", tagsSelected.toString())

            if(tagsSelected.isEmpty()) Toast.makeText(activiti, "Selecciona los tags que deseas eliminar", Toast.LENGTH_LONG).show()
            else{

                val builder = AlertDialog.Builder(activiti!!)
                builder.setTitle("Confirmación")
                builder.setMessage("¿Desea continuar con la eliminación de tags?")
                builder.setPositiveButton("Si"){dialog, which ->
                    actUnsuscribe!!.Layout_Recycler.visibility = View.GONE
                    actUnsuscribe!!.LY_progressBar_unsuscribe.visibility = View.VISIBLE
                    presenter!!.SendTagsDelete(tagsSelected)
                }
                builder.setNeutralButton("Cancelar"){dialog, which->
                    dialog.dismiss()
                }

                val dialog: AlertDialog = builder.create()
                dialog.show()
            }
        }

        holder.btnAll.setOnClickListener{

            if(!allTrue){

                arrayTags.forEachIndexed{index, tag->
                    tag.selected = true
                    if(!tagsSelected.contains(tag)) tagsSelected.add(arrayTags[index])
                }

                allTrue = true
            }
            else{

                arrayTags.forEachIndexed{index, tag->
                    tag.selected = false
                }

                tagsSelected.clear()
                allTrue = false
            }

            notifyDataSetChanged()
        }

        holder.checkBox.setOnClickListener{

            //Log.e("Selected", arrayTags[position].toString())

            if(holder.checkBox.isChecked){
                tagsSelected.add(arrayTags[position])
                arrayTags[position].selected = true //To mantain the checkbox selected if we scroll the view
            }
            else{
                tagsSelected.remove(arrayTags[position])
                arrayTags[position].selected = false //To unselect the checkbox
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder{
        val view = inflater.inflate(R.layout.cardview_fragment_unsuscribe, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int{
        Log.e("size", arrayTags.size.toString())
        return arrayTags.size
    }
}
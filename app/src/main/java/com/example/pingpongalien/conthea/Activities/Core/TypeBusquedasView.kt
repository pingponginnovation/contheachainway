package com.example.pingpongalien.conthea.Activities.Core

import android.os.Bundle
import android.view.*
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import kotlinx.android.synthetic.main.fragment_busquedas.view.*

class TypeBusquedasView(private val core: CoreView): ReaderFragment(){

    private lateinit var mvi: View

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mvi = inflater!!.inflate(R.layout.fragment_busquedas, container, false)
        HandleClicks()
        return mvi
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        core.changeTitleToolbar("Filtros de búsqueda")
    }

    private fun HandleClicks(){
        mvi.btn_catalogo.setOnClickListener{
            core.buscarEnCatalogo("", "", "")
        }
        mvi.btn_ubicacion.setOnClickListener{
            core.buscarPorUbicacion()
        }
        mvi.btn_etiqueta.setOnClickListener{
            core.buscarEtiquetas()
        }
    }

    override fun activateReader(){}
    override fun initReader(){}
}

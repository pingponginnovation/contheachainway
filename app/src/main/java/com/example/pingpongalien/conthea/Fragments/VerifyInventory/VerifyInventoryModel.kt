package com.example.pingpongalien.conthea.Fragments.VerifyInventory

import android.content.Context
import android.util.Log
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView.Companion.IDproducto_search
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView.Companion.almacen
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView.Companion.lotee
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalog
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseProducts
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

/**
 * Created by AOR on 7/12/17.
 */
class VerifyInventoryModel(presenter: VerifyInventoryPresenter, context: Context) : VerifyInventory.Model {


    var presenter: VerifyInventory.Presenter? = null
    var aProducts: List<ResponseProducts> = ArrayList()
    var aPlaces: ResponseCatalog? = null
    var appContext: Context

    init {
        this.presenter = presenter
        this.appContext = context
    }

    override fun getProducts() {
        requestProducts()
    }

    override fun successProducts(response: List<ResponseProducts>){
        aProducts = response
        if(aProducts.isNotEmpty()) presenter!!.setProducts(aProducts)
        else presenter!!.visibilityWithoutProducts()
    }

    override fun unsuccessProducts(error: String) {
        presenter!!.unsuccessProducts(error)
    }

    override fun successPlaces(response: ResponseCatalog) {
        aPlaces = response
        Log.e("Places", aPlaces.toString())
        createListPlaces()
    }

    override fun getPlaces(id: Int){
        //appContext.toast("ID: ${aProducts.get(position).id}")
        //IDproducto_search = aProducts.get(position).id
        IDproducto_search = id
        requestPlaces(id)
    }

    override fun getOnePlace(position: Int){
        //Log.e("ALMACEN: ", "${aPlaces!!.response!!.get(position)!!.espacio_actual!!.codigo}")
        //Log.e("LOTE: ", "${aPlaces!!.response!!.get(position)!!.lote}")
        almacen = aPlaces!!.response!![position]!!.espacio_actual!!.codigo
        lotee = aPlaces!!.response!!.get(position)!!.lote
        presenter!!.showDetailVerify(aPlaces!!.response!![position]!!)
    }

    fun requestProducts(){
        val retrofit = RetrofitController(appContext)
        retrofit.getProducts(this)
    }

    fun requestPlaces(idProduct : Int){
        val retrofit = RetrofitController(appContext)
        retrofit.getPlaces(idProduct,this)
    }

    fun createListProducts(){
        doAsync {
            val aInnerProducts = arrayListOf<String>()
            aProducts.forEach { product ->
                aInnerProducts.add(product.nombre!!)
            }
            uiThread {
                //presenter!!.setProducts(aInnerProducts)
            }
        }
    }

    fun createListPlaces(){

        doAsync {

            val aInnerPlaces = ArrayList<Map<String, String>>()

            aPlaces!!.response!!.forEach { place ->

                val innerPlace = HashMap<String, String>(2)

                if(place!!.espacio_actual != null) innerPlace.put("place", place.espacio_actual!!.codigo!!) //almacen
                else innerPlace.put("place","Sin lugar asignado")

                if(place.lote != null) innerPlace.put("lote", place.lote)
                else innerPlace.put("lote","Sin lote asignado")

                aInnerPlaces.add(innerPlace)
            }

            uiThread {
                presenter!!.setPlaces(aInnerPlaces)
                Log.e("Ainner", aInnerPlaces.toString())
            }
        }
    }
}
package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseDeletedTags(
        @SerializedName("response")
        var respuesta: String
)
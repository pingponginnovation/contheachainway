package com.example.pingpongalien.conthea.Activities.DetailVerify

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.Toast
import com.example.pingpongalien.conthea.Adapters.AdapterPotency
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalogItem
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderActivity
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.example.pingpongalien.conthea.Room.RoomController
import com.example.pingpongalien.conthea.SoundPlay
import com.google.gson.Gson
import com.rscja.deviceapi.RFIDWithUHFUART
import com.rscja.deviceapi.entity.UHFTAGInfo
import kotlinx.android.synthetic.main.activity_detail_verify.*
import kotlinx.android.synthetic.main.dialog_select_potencia.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import java.util.*

class DetailVerifyView: ReaderActivity(), DetailVerify.View{

    data class ajusteInventario2(val id: Int, val almacen: String, val lotee: String, val cantidad: Int, val tagsCorrectos: ArrayList<String>)
    data class ajusteInventario1(val id: Int, val almacen: String, val lotee: String)
    val TAG = "DetailVerifyView"
    var presenter: DetailVerify.Presenter? = null

    private var readSingle = false
    private var readTags = arrayListOf<String>() //Array para mandar por la ruta (/check/ajuste)
    private var correctTags = arrayListOf<String>() //Tags correctos que recibo para la verificacion
    private var dialog: Dialog? = null
    //private var am: AudioManager? = null
    //private var volumnRatio = 0f
    //private var soundMap = HashMap<Int, Int>()
    //private var soundPool: SoundPool? = null
    private var isOffline = false
    private var mSound: SoundPlay? = null
    companion object{
                //VARIABLES PARA MANDAR POR APIREST Y LOS RECIBA EL BACKEND. SE USARÁ PARA AJUSTE DE INVENTARIO
        var IDproducto_search: Int? = null
        var almacen: String? = null
        var catalogoo: String? = null
        var lotee: String? = null
        var total_leidos: Int? = null

        var loopFlag = false
        var mReader: RFIDWithUHFUART? = null
        var handler: Handler? = null

        private val POWER_LEVEL = 20
        var mContext: Context? = null
    }

    init{
        mView = R.layout.activity_detail_verify
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_verify)

        presenter = DetailVerifyPresenter(this, applicationContext, "VERIFY_VIEW")
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        setSupportActionBar(tb_detail)
        val catalogue = intent.getSerializableExtra("catalogue") as ResponseCatalogItem
        presenter!!.setCatalog(catalogue)
        txt_name_product.text = intent.getStringExtra("nameProduct")
        if(catalogue.lote != null) txt_lote.text = catalogue.lote
        else txt_lote.text = "Sin lote registrado"

        mSound = SoundPlay(this)
        iinit()
        SplitNameProduct()
        SendAndGetDatos()
        InitHandler()
        CheckDatabase()
    }

    private fun SplitNameProduct(){
        val name = intent.getStringExtra("nameProduct")
        val splitted = name.split(":").toTypedArray()
        catalogoo = splitted[0]
    }

    private fun SendAndGetDatos(){

        //val inventoryAdjust = ajusteInventario(IDproducto_search!!, almacen!!, lotee!!, total_leidos!!, correctTags)
        val inventoryAdjust = ajusteInventario1(IDproducto_search!!, almacen!!, lotee!!)
        val gson = Gson()
        val ajuste = gson.toJson(inventoryAdjust)
        presenter!!.GetAmountIntelisis(ajuste)
        //Log.e("JSON: ", ajuste)
        //Log.e("\nJSON: ", correctTags.toString())
    }

    private fun InitHandler(){
        handler = object : Handler() {
            override fun handleMessage(msg: Message) {
                val result = msg.obj.toString() + ""
                val strs = result.split("@").toTypedArray()
                val epc = strs[0].split(":").toTypedArray()

                Log.e("added", "added")
                AddTagReaded(epc[1])
            }
        }
    }

    private fun AddTagReaded(tag: String){
        presenter!!.addTag(tag)
        if(readTags.isEmpty()) readTags.add(tag)
        else if(!readTags.contains(tag)) readTags.add(tag)

        mSound!!.playSuccess()
    }

    private fun CheckDatabase(){

        Log.e("Checkdb", "si")
        Log.e("Almacen", almacen)
        Log.e("lote", lotee)
        Log.e("catalgo", catalogoo)

        var exist: Boolean?
        val arrayOfTags: ArrayList<String> = ArrayList()

        doAsync{
            val database = RoomController.getInstance()
            exist = database.daoInventario().isExists()
            if(exist!!){
                val order = database.daoInventario().GetTagsSaved(lotee!!, almacen!!, catalogoo!!)
                uiThread {
                    if(order.isNotEmpty()){
                        Log.e("Order", order.size.toString())
                        alert{
                            title = "Aviso"
                            message = "Existe un respaldo de esta órden, ¿Deseas cargar los datos?"
                            negativeButton("Eliminar respaldo"){deleteBackup()}
                            neutralPressed("Cancelar"){}
                            positiveButton("Si"){
                                order.forEach{
                                    arrayOfTags.add(it.uuidNumber!!)
                                }
                                if(arrayOfTags.isNotEmpty()) presenter!!.LoadLastStateOrder(arrayOfTags)
                            }
                            isCancelable = false
                        }.show()
                    }
                }
            }else Log.e("No existe", "la DB")
        }
    }

    fun deleteBackup(){
        Log.e("Deletedone", " done")
        doAsync {
            val database = RoomController.getInstance()
            database.daoInventario().deleteFromDB(lotee!!, almacen!!, catalogoo!!)
        }
    }

    fun iinit(){
        try {
           mReader = RFIDWithUHFUART.getInstance()
           if (mReader != null){

               if (mReader != null) {
                   someTask().MyCustomTask(this)
                   someTask().execute()
               }



               /*val result = mReader!!.init()
               if(!result) toast("Init fail RFID")
               else{
                   try {
                       mReader!!.power = POWER_LEVEL
                   }catch(e: Exception){}
               }*/
           }
       } catch (e: Exception) {
           toast(e.message!!)
       }
    }

    override fun initReader(){
        /*try {
            mReader = RFIDWithUHFUART.getInstance()
            if (mReader != null){

                /*if (mReader != null) {
                    someTask().MyCustomTask(this)
                    someTask().execute()
                }*/



                val result = mReader!!.init()
                if(!result) toast("Init fail RFID")
                else{
                    try {
                        mReader!!.power = POWER_LEVEL
                    }catch(e: Exception){}
                }
            }
        } catch (e: Exception) {
            toast(e.message!!)
        }*/
    }

    class someTask: AsyncTask<Void, Void, Boolean>() {

        var mypDialog: ProgressDialog? = null

        override fun doInBackground(vararg params: Void?): Boolean?{
            return mReader!!.init()
        }

        fun MyCustomTask(context: Context) {
            mContext = context
        }

        override fun onPreExecute() {
            super.onPreExecute()

            mypDialog = ProgressDialog(mContext)
            mypDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            mypDialog!!.setMessage("Iniciando RFID...")
            mypDialog!!.setCanceledOnTouchOutside(false)
            mypDialog!!.show()
            //mContext!!.progressDialog("Please wait a minute.", "Downloading…")
            //mContext!!.indeterminateProgressDialog("Fetching the data…")
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            mypDialog!!.cancel()
            if (!result!!) {
                Toast.makeText(mContext, "init fail", Toast.LENGTH_SHORT).show()
            }
            else{
                try {
                    mReader!!.power = POWER_LEVEL
                    Log.e("Power", mReader!!.power.toString())
                }catch(e: Exception){}
            }
        }
    }

    override fun activateReader(){
        //val iPower: Int = mReader!!.power
        //Log.e("potencia", iPower.toString())
    }

    override fun setModeRead(mode: Boolean){
        this.readSingle = mode
        Log.e("single", readSingle.toString())
    }

    override fun setItemNameMenu(name: String, item: MenuItem){
        item.title = name
    }

    override fun unsuccessVerify(error: String){
        toast(error)
    }

    override fun setAmount(verify: Int, total : Int){
        txt_cantidad.text = "Cantidad en conthea: $total"
        total_leidos = verify //Verify es la cantidad de tags leídos
    }

    override fun SetAmountIntelisis(cantidad: Int){
        im_circle.visibility = View.VISIBLE
        txt_total.visibility = View.VISIBLE
        txt_cantidad.visibility = View.VISIBLE
        txt_intelisis_canti.visibility = View.VISIBLE
        txt_name_product.visibility = View.VISIBLE
        txt_lote.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
        txt_intelisis_canti.text = "Cantidad en intelisis: $cantidad"
    }

    override fun UnsuccessAmountIntelisis(msj: String){
        toast(msj)
    }

    override fun setTotal(total: String){
        txt_total.setText(total) //Cantidad dentro del circulo
    }

    override fun setColorComplete() {
        txt_total.setTextColor(resources.getColor(R.color.colorPrimary))
        im_circle.setImageResource(R.drawable.bk_products_possitive)
    }

    override fun setColorIncomplete() {
        txt_total.setTextColor(resources.getColor(R.color.red))
        im_circle.setImageResource(R.drawable.bk_products_negative)
    }

    override fun setCorrectTags(misTags: ArrayList<String>){
        if(correctTags.isEmpty()){

            for (i in misTags){
                correctTags.add(i)
            }
            //correctTags.add(misTags.toString())
        }
        else{
            for (i in misTags){
                if(!correctTags.contains(i)) correctTags.add(i)
            }
        }
    }

    private fun showPotency() {
        val arrayPotency = arrayListOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                21,22,23,24,25,26,27,28,29,30)
        dialog = Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.dialog_select_potencia)

        dialog!!.rclv_potencias.addItemDecoration(SimpleDividerItemDecoration(this))
        dialog!!.rclv_potencias.apply{
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            setHasFixedSize(true)
            adapter = AdapterPotency(arrayPotency){Setpotencia(it)}
        }

        dialog!!.btn_close_dialog!!.setOnClickListener{
            dialog!!.dismiss()
        }
        dialog!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog!!.show()
    }

    private fun Setpotencia(num: Int){
        if(dialog != null) dialog!!.dismiss()
        if(mReader!!.setPower(num)) toast("Potencia ajustada correctamente a: $num")
        else toast("Error en establecer potencia")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail_order, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean{
        when(item!!.itemId){
            R.id.item_send ->{
                if(isOffline){
                        //ponemos este boton en modo validar tags
                    presenter!!.searchTag()
                }
                else{
                        //ponemos este boton en modo mandar ajuste
                    val inventoryAdjust = ajusteInventario2(IDproducto_search!!, almacen!!, lotee!!, total_leidos!!, correctTags)
                    val retrofit = RetrofitController(this)
                    val gson = Gson()
                    val ajuste = gson.toJson(inventoryAdjust)
                    retrofit.ajusteInventario2(ajuste, this)
                }
            }
            R.id.item_potency -> showPotency()
            R.id.item_type_read -> presenter!!.setModeRead(item.title.toString(), item)
            R.id.item_offline -> {
                if(item.title.toString() == "Modo offline"){
                    isOffline = true
                    item.title = "Modo online"
                }
                else{
                    isOffline = false
                    item.title = "Modo offline"
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean{
        if (keyCode == 139 || keyCode == 280 || keyCode == 293){
            if (event.repeatCount == 0) readTag()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        stopInventory()
        return super.onKeyUp(keyCode, event)
    }

    private fun readTag(){
        if(!readSingle){
            //TODO. Multiple read
            if (mReader!!.startInventoryTag()){
                loopFlag = true
                TagThread().start()
            }else{
                mReader!!.stopInventory()
                toast("Error de lectura")
            }
        }
        else{
            //todo. individual read
            val strUII: UHFTAGInfo? = mReader!!.inventorySingleTag()
            if(strUII != null){
                val strEPC = strUII.epc
                AddTagReaded(strEPC)
                mSound!!.playSuccess()
            }
            else toast("No se leyó ningun tag")
        }
    }

    internal class TagThread : Thread() {
        override fun run() {
            var strTid: String
            var strResult: String
            var res: UHFTAGInfo?
            while (loopFlag) {
                res = mReader!!.readTagFromBuffer()
                if (res != null){
                    strTid = res.tid
                    strResult = if (strTid.isNotEmpty() && strTid != "0000000" +
                            "000000000" && strTid != "000000000000000000000000"){
                        "TID:$strTid\n"
                    }else ""

                    val msg: Message = handler!!.obtainMessage()
                    msg.obj = strResult + "EPC:" + res.epc + "@" + res.rssi
                    handler!!.sendMessage(msg)
                }
            }
        }
    }

    private fun stopInventory(){
        if (loopFlag) {
            loopFlag = false
            if(!mReader!!.stopInventory()) toast("Fail stop operation")
        }

        if(!isOffline) presenter!!.searchTag()
    }

    /*private fun initSound() {
        soundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 5)
        soundMap.put(1, soundPool!!.load(this, R.raw.beep, 1))
        soundMap.put(2, soundPool!!.load(this, R.raw.fail, 1))
        am = getSystemService(AUDIO_SERVICE) as AudioManager
    }

    fun playSound(id: Int) {
        val audioMaxVolumn = am!!.getStreamMaxVolume(AudioManager.STREAM_MUSIC).toFloat()
        val audioCurrentVolumn = am!!.getStreamVolume(AudioManager.STREAM_MUSIC).toFloat()
        volumnRatio = audioCurrentVolumn / audioMaxVolumn
        try {
            soundPool!!.play(soundMap[id]!!, volumnRatio, volumnRatio, 1, 0, 1f)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }*/

    override fun initWidgets(){}
    override fun enableWidgets(enabled: Boolean){}
    protected fun playSuccess(){}

    override fun onResume(){
        if(mReader == null){
            iinit()
            mSound = SoundPlay(this)
        }
        super.onResume()
    }

    override fun onStop(){

        mSound!!.Release()

        if (mReader != null){
            if(mReader!!.free()){
                mReader = null
                Log.e("okfree", "free")
            }
            else Log.e("fuckfree", "Fuck free")
        }
        super.onStop()
    }
}

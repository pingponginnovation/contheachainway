package com.example.pingpongalien.conthea.Fragments.Unsuscribe

import android.content.Context
import android.view.MenuItem
import com.example.pingpongalien.conthea.Models.TagsForDelete

class UnsuscribePresenter(viewUnsuscribe : Unsuscribe.View, context: Context): Unsuscribe.Presenter {

    var view: Unsuscribe.View? = null
    var model: Unsuscribe.Model? = null

    init {
        this.view = viewUnsuscribe
        model = UnsuscribeModel(this, context)
    }

    //MODEL
    override fun sendTags() {
        model!!.sendTags()
    }

    override fun addTag(tag: String) {
        model!!.addTag(tag)
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        model!!.setModeRead(nameItem, item)
    }

    override fun SendTagsDelete(tags: ArrayList<TagsForDelete>){
        model!!.SendTagsDelete(tags)
    }

    //VIEW
    override fun SetAdapterDelete(arrayDelete: ArrayList<TagsForDelete>){
        view!!.SetAdapterDelete(arrayDelete)
    }

    override fun unSuccessRequestPlaces(error: String) {
        view!!.showErrorGetPlaces(error)
    }

    override fun setModeRead(mode: Boolean) {
        view!!.setModeRead(mode)
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        view!!.setItemNameMenu(name, item)
    }

    override fun SuccessDeletedTags(msj: String){
        view!!.SuccessDeletedTags(msj)
    }

    override fun UnSuccessDeletedTags(msj: String){
        view!!.UnSuccessDeletedTags(msj)
    }
}
package com.example.pingpongalien.conthea.Fragments.StartInventory

import android.view.MenuItem
import com.example.pingpongalien.conthea.ModelsRetrofit.RequestStartInventory
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseStartInventory

interface StartInventory{

    interface View{
        fun successStartInventory(responseSuccess: ResponseStartInventory?)
        fun unSuccessStartInventory(error: String)
        fun setData(read: String, saved: String)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
    }

    interface Presenter{
        //MODEL
        fun requestStartInventory()
        fun resetData()
        fun getData()
        fun addTag(tag : String)
        fun setModeRead(nameItem: String, item: MenuItem)

        //VIEW
        fun successStartInventory(responseSuccess: ResponseStartInventory?)
        fun unSuccessStartInventory(error: String)
        fun setData(read: String, saved: String)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
    }

    interface Model{
        fun requestStartInventory()
        fun successStartInventory(responseSuccess: ResponseStartInventory?)
        fun unSuccessStartInventory(error: String)
        fun resetData()
        fun getData()
        fun addTag(tag : String)
        fun setModeRead(nameItem: String, item: MenuItem)
    }
}
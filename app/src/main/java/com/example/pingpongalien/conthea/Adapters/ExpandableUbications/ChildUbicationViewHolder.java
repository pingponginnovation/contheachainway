package com.example.pingpongalien.conthea.Adapters.ExpandableUbications;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.example.pingpongalien.conthea.R;

public class ChildUbicationViewHolder extends ChildViewHolder{

    private TextView txt_ubicacion;
    private TextView txt_cantidad;
    private TextView txt_lote;
    private TextView txt_fabricante;
    private TextView txt_nombre;
    private TextView txt_caducidad;
    private TextView txt_state_tag;
    private TextView txt_almacen;
    private TextView txt_uid;
    private Button leer_etiquetas;

    public ChildUbicationViewHolder(@NonNull View itemView) {
        super(itemView);
        txt_caducidad = itemView.findViewById(R.id.txt_expiration);
        txt_almacen = itemView.findViewById(R.id.txt_almacen);
        txt_ubicacion = itemView.findViewById(R.id.txt_ubication);
        txt_cantidad = itemView.findViewById(R.id.txt_cantidadd);
        txt_lote = itemView.findViewById(R.id.txt_lote);
        txt_fabricante = itemView.findViewById(R.id.txt_maker);
        txt_state_tag = itemView.findViewById(R.id.txt_state_tag);
        txt_nombre = itemView.findViewById(R.id.txt_nombre);
        txt_uid = itemView.findViewById(R.id.txt_uid);
        leer_etiquetas = itemView.findViewById(R.id.btn_leer_etiquetas);
    }

    public Button getLeer_etiquetas(){
        return leer_etiquetas;
    }

    public TextView getTxt_caducidad() {
        return txt_caducidad;
    }

    public TextView getTxt_almacen() {
        return txt_almacen;
    }

    public TextView getTxt_ubicacion() {
        return txt_ubicacion;
    }

    public TextView getTxt_cantidad() {
        return txt_cantidad;
    }

    public TextView getTxt_lote() {
        return txt_lote;
    }

    public TextView getTxt_fabricante() {
        return txt_fabricante;
    }

    public TextView getTxt_nombre() {
        return txt_nombre;
    }

    public TextView getTxt_state_tag() {
        return txt_state_tag;
    }

    public TextView getTxt_uid() {
        return txt_uid;
    }
}
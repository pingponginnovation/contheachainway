package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseDeleteItems(

        @field:SerializedName("uuid")
        val uuid: String? = null,

        @field:SerializedName("articulo")
        val article: String? = null,

        @field:SerializedName("lote")
        val lote: String? = null,

        @field:SerializedName("caducidad")
        val caducidad: String? = null,

        @field:SerializedName("almacenes")
        val storage: String? = null
)
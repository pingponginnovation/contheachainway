package com.example.pingpongalien.conthea;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public abstract class ReaderFragment extends Fragment{

    // ------------------------------------------------------------------------
    // Member Variable
    // ------------------------------------------------------------------------

    //protected RFIDWithUHFUART mReader;
    protected int mView;

    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------

    public ReaderFragment() {
        super();
        mView = 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_set_place_xxx, container, false);
        mView = view.getId();
        getActivity().setContentView(mView);

        /*try {
            mReader = RFIDWithUHFUART.getInstance();
            if(mReader == null){

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setIcon(android.R.drawable.ic_dialog_alert);
                builder.setTitle(R.string.module_error);
                builder.setMessage(R.string.fail_check_module);
                builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }

                });
            }
        }catch(Exception e){
            Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }*/

        return view;
    }

    @Override
    public void onDestroy() {
        // Deinitalize RFID reader Instance
        //if (mReader != null) mReader.free();
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        onInitReader(false);
    }

    @Override
    public void onStop() {
        //if (mReader != null) mReader.free();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        //if (mReader != null) mReader.free();
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // ------------------------------------------------------------------------
    // Reader Event Handler
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Override Widgets Control Methods
    // ------------------------------------------------------------------------


    // Initialize Reader
    protected abstract void initReader();

    // Activated Reader
    protected abstract void activateReader();

    // Begin Initialize Reader
    private void onInitReader(final boolean enabled) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //initReader();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //activateReader();
                    }

                });
            }

        }).start();
    }
}

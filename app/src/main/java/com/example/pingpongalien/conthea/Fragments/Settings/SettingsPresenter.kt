package com.example.pingpongalien.conthea.Fragments.Settings

import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController

class SettingsPresenter(settingsView : Settings.View, val sharedPreferences: PreferencesController):
        Settings.Presenter{

    var view: Settings.View? = null
    var model: Settings.Model? = null

    init {
        this.view = settingsView
        model = SettingsModel(this, sharedPreferences)
    }

    //MODEL
    override fun saveUrl(url: String) {
       model!!.saveUrl(url)
    }

    //VIEW
    override fun showSaveUrl() {
        view!!.showSaveUrl()
    }

    override fun showErrorUrl() {
        view!!.showErrorUrl()
    }
}
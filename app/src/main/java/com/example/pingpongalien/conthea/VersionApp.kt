package com.example.pingpongalien.conthea


import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import kotlinx.android.synthetic.main.activity_login.btn_session_start
import kotlinx.android.synthetic.main.fragment_settings.*
import org.jetbrains.anko.longToast

class VersionApp: Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        val view = inflater.inflate(R.layout.version_app, container, false)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setVersionApp()
    }

    private fun setVersionApp() {
        try {
            val pInfo = context.packageManager.getPackageInfo(activity.packageName, 0)
            txt_version_number.text = "Versión actual: ${pInfo.versionName}"
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }
}

package com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta

import android.app.Dialog
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.*
import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.ListView
import android.widget.PopupWindow
import android.widget.Toast
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyLocalView
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Adapters.Adapter_potencia
import com.example.pingpongalien.conthea.Adapters.CustomAdapterSpinnerPotencia
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication
import com.example.pingpongalien.conthea.Adapters.TabsAdapter
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import com.example.pingpongalien.conthea.SoundPlay
import com.rscja.deviceapi.RFIDWithUHFUART
import com.rscja.deviceapi.entity.UHFTAGInfo
import kotlinx.android.synthetic.main.dialog_select_potencia.*
import kotlinx.android.synthetic.main.fragment_search_by_etiqueta.view.*
import kotlinx.android.synthetic.main.fragment_search_by_etiqueta.view.btn_continue_read
import kotlinx.android.synthetic.main.fragment_search_by_etiqueta.view.btn_limpiar
import kotlinx.android.synthetic.main.fragment_search_by_etiqueta.view.btn_single_read
import org.jetbrains.anko.toast

class SearchByEtiquetaView(val core: CoreView): ReaderFragment(), SearchByEtiqueta.View{

    private lateinit var mvi: View
    private var presenter: SearchByEtiqueta.Presenter? = null
    private var mSound: SoundPlay? = null
    private var readSingle = false
    private var intent = Intent("CON_UBICACION")
    private var intent2 = Intent("SIN_UBICACION")
    private var mAdapter: Adapter_potencia = Adapter_potencia()
    private var dialog: Dialog? = null

    companion object{
        private var POWER_LEVEL = 20
        var loopFlag = false
        var mReader: RFIDWithUHFUART? = null
        var handlerMSJ: Handler? = null
        private var mcontext: Context? = null
        var receiverSpace: BroadcastReceiver? = null
        var receiverNoSpace: BroadcastReceiver? = null
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mvi = inflater!!.inflate(R.layout.fragment_search_by_etiqueta, container, false)
        presenter = SearchByEtiquetaPresenter(this, context)
        return mvi
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        core.changeTitleToolbar("Búsqueda por etiqueta")
        mSound = SoundPlay(context)
        iinit()
        ConfigTabs()
        configPotency()
        configLectura()
        InitHandler()
    }

    fun iinit(){
        try {
            mReader = RFIDWithUHFUART.getInstance()
            if(mReader != null){

                if (mReader != null) {

                    mvi.autocomplete_potencia.setText(POWER_LEVEL.toString())

                    someTask().MyCustomTask(context)
                    someTask().execute()
                }
            }
        } catch (e: Exception) {
            context.toast(e.message!!)
        }
    }

    class someTask: AsyncTask<Void, Void, Boolean>() {

        var mypDialog: ProgressDialog? = null

        override fun doInBackground(vararg params: Void?): Boolean?{
            return mReader!!.init()
        }

        fun MyCustomTask(context: Context) {
            mcontext = context
        }

        override fun onPreExecute() {
            super.onPreExecute()

            mypDialog = ProgressDialog(mcontext)
            mypDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            mypDialog!!.setMessage("Iniciando RFID...")
            mypDialog!!.setCanceledOnTouchOutside(false)
            mypDialog!!.show()
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            mypDialog!!.cancel()
            if (!result!!) {
                Toast.makeText(mcontext, "init fail", Toast.LENGTH_SHORT).show()
            }
            else{
                try {
                    mReader!!.power = POWER_LEVEL
                }catch(e: Exception){}
            }
        }
    }

    private fun configLectura(){

        mvi.btn_continue_read.background.setColorFilter(ContextCompat.getColor(context, R.color.green_read), PorterDuff.Mode.MULTIPLY)
        mvi.btn_continue_read.setTextColor(Color.WHITE)

        mvi.btn_single_read.setOnClickListener{
            readSingle = true
            mvi.btn_continue_read.background.setColorFilter(ContextCompat.getColor(context, R.color.gray_read), PorterDuff.Mode.MULTIPLY)
            mvi.btn_continue_read.setTextColor(Color.parseColor("#2D2B2B"))

            mvi.btn_single_read.background.setColorFilter(ContextCompat.getColor(context, R.color.green_read), PorterDuff.Mode.MULTIPLY)
            mvi.btn_single_read.setTextColor(Color.WHITE)
        }

        mvi.btn_continue_read.setOnClickListener{
            readSingle = false
            mvi.btn_continue_read.background.setColorFilter(ContextCompat.getColor(context, R.color.green_read), PorterDuff.Mode.MULTIPLY)
            mvi.btn_continue_read.setTextColor(Color.WHITE)

            mvi.btn_single_read.background.setColorFilter(ContextCompat.getColor(context, R.color.gray_read), PorterDuff.Mode.MULTIPLY)
            mvi.btn_single_read.setTextColor(Color.parseColor("#2D2B2B"))
        }
    }

    private fun configPotency(){

        mvi.autocomplete_potencia.setOnClickListener{
            mvi.autocomplete_potencia.isEnabled = false
            val arrayPotency = arrayListOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                    21,22,23,24,25,26,27,28,29,30)
            LoadPotenciasRecycler(arrayPotency)
        }

        /*adapterPotencias = AdapterSpinnerPotencia(arrayPotency){Setpotencia(it)}

        mview.sp_potencia.setOnClickListener{
            popupWindow?.dismiss()
            if (popupWindow == null) providePopupWindow(mview.shower_listview)
            popupWindow!!.showAsDropDown(mview.shower_listview, 0, - mview.shower_listview.height)
        }*/
    }

    private fun LoadPotenciasRecycler(potencia: ArrayList<Int>){
        dialog = Dialog(activity!!)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.dialog_select_potencia)

        dialog!!.rclv_potencias.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        dialog!!.rclv_potencias.setHasFixedSize(true)
        dialog!!.rclv_potencias.addItemDecoration(SimpleDividerItemDecoration(context))
        mAdapter.Adapter_potencia(potencia){Setpotencia(it)}
        dialog!!.rclv_potencias.adapter = mAdapter

        dialog!!.btn_close_dialog!!.setOnClickListener{
            mvi.autocomplete_potencia.isEnabled = true
            dialog?.dismiss()
        }

        dialog!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog!!.show()
    }

    private fun providePopupWindow(vieww: View){
        /*popupWindow = PopupWindow(vieww.width, ViewGroup.LayoutParams.WRAP_CONTENT)
            .apply {
                val backgroundDrawable = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    activity!!.getDrawable(R.drawable.design_autocomplete).apply{}
                } else {
                    Log.e("Else", "menor lollipop")
                    TODO("VERSION.SDK_INT < LOLLIPOP")
                }
                setBackgroundDrawable(backgroundDrawable)
                isOutsideTouchable = true

                val listView = layoutInflater.inflate(R.layout.layout_potencia_dropdown, null,
                    false) as ListView
                listView.adapter = adapterPotencias
                contentView = listView
            }*/
    }

    private fun Setpotencia(num: Int){
        mvi.autocomplete_potencia.isEnabled = true
        dialog?.dismiss()
        if(mReader!!.setPower(num)){
            mvi.autocomplete_potencia.setText(num.toString())
            POWER_LEVEL = num
            context.toast("Potencia ajustada correctamente a: $num")
        }
        else context.toast("Error en establecer potencia")
    }

    fun setKeyDown(keyCode: Int, event: KeyEvent?){
        if (keyCode == 139 || keyCode == 280 || keyCode == 293){
            if (event!!.repeatCount == 0) readTag()
        }
    }

    fun setKeyUp(keyCode: Int, event: KeyEvent?){
        stopInventory()
    }

    private fun readTag(){
        if(!readSingle){
            //TODO. Multiple read
            if (mReader!!.startInventoryTag()){
                loopFlag = true
                TagThread().start()
            }else{
                mReader!!.stopInventory()
                context.toast("Error de lectura")
            }
        }
        else{
            //todo. individual read
            val strUII: UHFTAGInfo? = mReader!!.inventorySingleTag()
            if(strUII != null){
                val strEPC = strUII.epc
                AddTagReaded(strEPC)
                mSound!!.playSuccess()
            }
            else context.toast("No se leyó ningun tag")
        }
    }

    internal class TagThread : Thread() {
        override fun run() {
            var strTid: String
            var strResult: String
            var res: UHFTAGInfo?
            while (loopFlag) {
                res = mReader!!.readTagFromBuffer()
                if (res != null){
                    strTid = res.tid
                    strResult = if (strTid.isNotEmpty() && strTid != "0000000" +
                            "000000000" && strTid != "000000000000000000000000"){
                        "TID:$strTid\n"
                    }else ""

                    val msg: Message = handlerMSJ!!.obtainMessage()
                    msg.obj = strResult + "EPC:" + res.epc + "@" + res.rssi
                    handlerMSJ!!.sendMessage(msg)
                }
            }
        }
    }

    private fun InitHandler(){
        handlerMSJ = object : Handler() {
            override fun handleMessage(msg: Message) {
                val result = msg.obj.toString() + ""
                val strs = result.split("@").toTypedArray()
                val epc = strs[0].split(":").toTypedArray()

                AddTagReaded(epc[1])
                //playSound(1)
            }
        }
    }

    private fun showViews(){
        mvi.progress.visibility = View.VISIBLE
        mvi.imgetiquetas.visibility = View.GONE
        mvi.leer_etiquetas.visibility = View.GONE
    }

    private fun AddTagReaded(tag: String){
        presenter!!.addTag(tag)
        mSound!!.playSuccess()
    }

    private fun stopInventory(){
        if (loopFlag) {
            loopFlag = false
            if(!mReader!!.stopInventory()) context.toast("Fail stop operation")
        }
        showViews()
        presenter!!.sendTags()
    }

    override fun successTags(tagsSinUbicacion: ArrayList<ParentUbication>?, tagsConUbicacion: ArrayList<ParentUbication>?){
        stateViepager(true)
        clickLimpiar()
        notifyAdapter(tagsSinUbicacion, tagsConUbicacion)
    }

    private fun notifyAdapter(tagsSinUbicacion: ArrayList<ParentUbication>?, tagsConUbicacion: ArrayList<ParentUbication>?){

        Log.e("space", tagsConUbicacion?.size.toString())
        Log.e("space no", tagsSinUbicacion?.size.toString())

        //intent = Intent("CON_UBICACION")

        if(!tagsConUbicacion.isNullOrEmpty()){
            Log.e("entra", "bradcast con espacio")
            intent.putExtra("TAGS", tagsConUbicacion)
            LocalBroadcastManager.getInstance(activity).sendBroadcast(intent)
        }
        else{
            Log.e("clear", "bradcast con espacio")
            intent.putExtra("CLEAR", true)
            LocalBroadcastManager.getInstance(activity).sendBroadcast(intent)
        }

        //intent2 = Intent("SIN_UBICACION")

        if(!tagsSinUbicacion.isNullOrEmpty()){
            intent2.putExtra("TAGS", tagsSinUbicacion)
            LocalBroadcastManager.getInstance(activity).sendBroadcast(intent2)
        }
        else{
            intent2.putExtra("CLEAR", true)
            LocalBroadcastManager.getInstance(activity).sendBroadcast(intent2)
        }
    }

    private fun stateViepager(show: Boolean){
        if(show){
            mvi.tabs!!.visibility = View.VISIBLE
            mvi.viewpager!!.visibility = View.VISIBLE
            mvi.btn_limpiar!!.visibility = View.VISIBLE

            mvi.progress.visibility = View.GONE
            mvi.imgetiquetas!!.visibility = View.GONE
            mvi.leer_etiquetas!!.visibility = View.GONE
        }
        else{
            mvi.tabs!!.visibility = View.GONE
            mvi.viewpager!!.visibility = View.GONE
            mvi.btn_limpiar!!.visibility = View.GONE
            mvi.progress.visibility = View.GONE

            mvi.imgetiquetas!!.visibility = View.VISIBLE
            mvi.leer_etiquetas!!.visibility = View.VISIBLE
        }
    }

    private fun clickLimpiar(){
        mvi.btn_limpiar.setOnClickListener{
            stateViepager(false)
            presenter!!.cleanTags()
        }
    }

    private fun ConfigTabs(){
        mvi.tabs!!.addTab(mvi.tabs!!.newTab().setText("Con ubicación"))
        mvi.tabs!!.addTab(mvi.tabs!!.newTab().setText("Sin ubicación"))
        mvi.tabs!!.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = TabsAdapter(activity.supportFragmentManager, mvi.tabs!!.tabCount)
        mvi.viewpager!!.adapter = adapter

        mvi.viewpager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(mvi.tabs))

        mvi.tabs!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                mvi.viewpager!!.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab){}
            override fun onTabReselected(tab: TabLayout.Tab){}
        })
    }

    override fun errorTags(resp: String){
        stateViepager(false)
        context.toast(resp)
    }

    override fun onResume(){

        if(mReader == null){
            mSound = SoundPlay(context)
            iinit()
        }
        super.onResume()
    }

    override fun onStop(){

        mSound!!.Release()

        if (mReader != null){
            if(mReader!!.free()){
                mReader = null
            }
        }
        super.onStop()
    }

    override fun activateReader(){}
    override fun initReader(){}
    override fun cleanTags() {}
}

package com.example.pingpongalien.conthea.Fragments.SetPlace

import android.view.MenuItem
import com.example.pingpongalien.conthea.Models.TagsChangeLocation
import com.example.pingpongalien.conthea.Models.TagsChangeLocation2
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseGetPlaces
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponsePlaces
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponsePlacesItems

/**
 * Created by AOR on 5/12/17.
 */
interface SetPlace {

    interface View{
        fun setPlaces(aPlaces : ArrayList<String>)
        fun setNamePlace(name : String)
        fun showErrorGetPlaces(error : String)
        fun setTotalUpdate(totalUpdate : String)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun SetAdapterLocationTags(tags: ArrayList<TagsChangeLocation2>, tags2: ArrayList<ResponsePlacesItems>)
        fun SuccessChangeLocationTags(msj: String)
        fun UnSuccessChangeLocationTags(msj: String)
    }

    interface Presenter{
        //MODEL
        fun getPlaces(from: String)
        fun setidPlace(position : Int)
        fun getName(position : Int)
        fun sendTags()
        fun addTag(tag : String)
        fun setModeRead(nameItem : String, item: MenuItem)
        fun SendCahngeLocationTags(tags: ArrayList<String>, code: String)

        //VIEW
        fun setPlaces(aPlaces : ArrayList<String>)
        fun unSuccessRequestPlaces(error : String)
        fun setNamePlace(name : String)
        fun setTotalUpdate(totalUpdate : String)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun SetAdapterLocationTags(tags: ArrayList<TagsChangeLocation2>, tags2: ArrayList<ResponsePlacesItems>)
        fun SuccessChangeLocationTags(msj: String)
        fun UnSuccessChangeLocationTags(msj: String)
    }

    interface Model{
        fun requestPlaces(from: String)
        fun successRequestGetPlaces(response : ResponseGetPlaces)
        fun unSuccessRequestGetPlaces(error : String)
        fun successRequestPlaces(response : ResponsePlaces)
        fun setidPlace(position : Int)
        fun getName(position : Int)
        fun sendTags()
        fun addTag(tag : String)
        fun setModeRead(nameItem : String, item: MenuItem)
        fun SendCahngeLocationTags(tags: ArrayList<String>, code: String)
        fun SuccessChangeLocationTags(msj: String)
        fun UnSuccessChangeLocationTags(msj: String)
    }
}
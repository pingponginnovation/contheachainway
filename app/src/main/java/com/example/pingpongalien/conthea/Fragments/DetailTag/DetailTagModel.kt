package com.example.pingpongalien.conthea.Fragments.DetailTag

import android.content.Context
import android.util.Log
import com.example.pingpongalien.conthea.ModelsRetrofit.RequestDetailTag
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDetailTag
import com.example.pingpongalien.conthea.Retrofit.RetrofitController

class DetailTagModel(presenter : DetailTag.Presenter, context: Context): DetailTag.Model {

    var presenter: DetailTag.Presenter? = null
    var appContext : Context? = null
    var retrofitController : RetrofitController? = null

    init {
        this.presenter = presenter
        this.appContext = context
        this.retrofitController = RetrofitController(context)
    }

    override fun searchDetailTag(tag: String){
        retrofitController!!.detailTag(RequestDetailTag(tag), this)
    }

    override fun successRequestDetailTag(detailTag: ResponseDetailTag){
        if(detailTag.response.moneda == "") detailTag.response.moneda = "Sin moneda"
        presenter!!.setDetailTag(detailTag.response)
    }

    override fun unsuccessRequestDetailTag(error: String) {
        presenter!!.showErrorDetailTag(error)
    }
}
package com.example.pingpongalien.conthea.Adapters.SectionedRecyclerSetPlace

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.example.pingpongalien.conthea.R

class ParentViewHolderTagsLocation(itemView: View?): RecyclerView.ViewHolder(itemView) {

    var catalogo: TextView
    var cantidad: TextView

    init {
        catalogo = itemView!!.findViewById(R.id.txt_catalogo)
        cantidad = itemView.findViewById(R.id.txt_cantidad)
    }

}
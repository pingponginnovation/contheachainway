package com.example.pingpongalien.conthea.Fragments.SearchCatalogue

import android.content.Context
import com.example.pingpongalien.conthea.Adapters.ExpandableCatalogue.ParentCatalogue

/**
 * Created by AOR on 5/12/17.
 */
class SearchCataloguePresenter(viewCataloge : SearchCatalogue.View, context: Context) : SearchCatalogue.Presenter {

    var view: SearchCatalogue.View? = null
    var model: SearchCatalogue.Model? = null

    init {
        this.view = viewCataloge
        model = SearchCatalogueModel(this, context)
    }

    //MODEL
    override fun getCatalogue(catalogo: String, name: String, lote: String) {
        model!!.getCatalogue(catalogo, name, lote)
    }

    override fun searchProduct(search: String, typeSearch : Int) {
        model!!.searchProduct(search, typeSearch)
    }

    override fun setComponentsCorrect(typeSearch: Int) {
        model!!.setComponentsCorrect(typeSearch)
    }

    override fun searchProductByExpiration(startDate: String, finishDate: String) {
        model!!.searchProductByExpiration(startDate, finishDate)
    }

    override fun formatDate(year: Int, month: Int, day: Int, type: Boolean) {
        model!!.formatDate(year, month, day, type)
    }

    override fun shouldSearch(uuid: String, typeSearch : Int) {
        model!!.shouldSearch(uuid, typeSearch)
    }

    //VIEW
    override fun unsuccessGetCataloge(error: String) {
        view!!.showErrorUnsuccessCatalog(error)
    }

    override fun setMessage(message: String) {
        view!!.setMessage(message)
    }

    override fun visibilitySearch(state: Boolean) {
        view!!.visibilitySearch(state)
    }

    override fun visibilityTextDate(state: Boolean) {
        view!!.visibilityTextDate(state)
    }

    override fun setStartDate(date: String) {
        view!!.setStartDate(date)
    }

    override fun setFinishDate(date: String) {
        view!!.setFinishDate(date)
    }

    override fun playSuccess() {
        view!!.playSuccess()
    }

    override fun visibilityRfid() {
        view!!.visibilityRfid()
    }

    override fun SuccessCatalogue(){
        view!!.SuccessCatalogue()
    }

    override fun setProductos(aProducts: ArrayList<ParentCatalogue>) {
        view!!.setProductos(aProducts)
    }
}
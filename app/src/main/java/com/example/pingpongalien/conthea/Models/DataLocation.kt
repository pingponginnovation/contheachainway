package com.example.pingpongalien.conthea.Models

data class DataLocation(
        var state: Boolean,
        var data: String,
        var type: String
)
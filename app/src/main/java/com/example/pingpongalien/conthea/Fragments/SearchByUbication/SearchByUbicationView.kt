package com.example.pingpongalien.conthea.Fragments.SearchByUbication

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Adapters.Adapter_espacios
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.Adapter
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.Models.ProductChild
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseGetPlacesBody
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import kotlinx.android.synthetic.main.dialog_select_product.*
import kotlinx.android.synthetic.main.fragment_busqueda_ubicacion.view.*
import org.jetbrains.anko.toast
import kotlin.collections.ArrayList

class SearchByUbicationView(val core: CoreView): ReaderFragment(), SearchByUbication.View{

    private var presenter: SearchByUbication.Presenter? =null
    private lateinit var mvi: View
    private var preferences: PreferencesController? = null
    private var space = ""
    private var spaceID = -1
    private var responseEspacios: ArrayList<ResponseGetPlacesBody>? = null
    private var dialog2: Dialog? = null
    private var adapterProds: Adapter? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mvi = inflater!!.inflate(R.layout.fragment_busqueda_ubicacion, container, false)
        preferences = PreferencesController(context)
        presenter = SearchByUbicationPresenter(this, mvi.context)
        return mvi
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        core.changeTitleToolbar("Búsqueda por ubicación")
        presenter!!.getPlaces("SEARCH_BY_UBICATION")
    }

    override fun setPlaces(aPlaces: ArrayList<ResponseGetPlacesBody>){

        responseEspacios = aPlaces

        val first = aPlaces[0].codigo
        mvi.autocomplete_places.setText(first)
        spaceID = aPlaces[0].id?:-1
        getproductos()

        mvi.autocomplete_places.setOnClickListener{
            mvi.autocomplete_places.isEnabled = false
            LoadSpacesInRecycler(aPlaces)
        }
    }

    private fun getproductos(){
        stateProgressbar(true)
        presenter!!.getProducts(spaceID)
    }

    private fun stateProgressbar(show: Boolean){
        if(show){
            mvi.progressBar.visibility = View.VISIBLE
            mvi.rclv_productos.visibility = View.GONE
            mvi.ac_buscar.visibility = View.GONE
        }
        else{
            mvi.progressBar.visibility = View.GONE
            mvi.rclv_productos.visibility = View.VISIBLE
            mvi.ac_buscar.visibility = View.VISIBLE
            mvi.ac_buscar.isEnabled = true
        }
    }

    private fun prods(): ArrayList<ParentUbication>{
        val array: ArrayList<ParentUbication> = arrayListOf()

        /*val child = ProductChild("algo"," 1112", "s", "mty", "nohay", "nose", 10, "1234",
        0, 0)
        val child2 = ProductChild("arbol"," 5555", "s", "mty", "nohay", "nose", 10, "1",
                0, 0)
        val child3 = ProductChild("q"," 123", "s", "mty", "nohay", "nose", 10, "2",
                0, 0)
        val child4 = ProductChild("2"," 345", "s", "mty", "nohay", "nose", 10, "3",
                0, 0)

        val arrayChild1: ArrayList<ProductChild> = arrayListOf()
        val arrayChild2: ArrayList<ProductChild> = arrayListOf()
        val arrayChild3: ArrayList<ProductChild> = arrayListOf()
        val arrayChild4: ArrayList<ProductChild> = arrayListOf()
        arrayChild1.add(child)
        arrayChild2.add(child2)
        arrayChild2.add(child3)
        arrayChild4.add(child4)*/

        //array.add(ParentUbication(arrayChild1, "65824"))
        //array.add(ParentUbication(arrayChild2, "67721"))
        //array.add(ParentUbication(arrayChild3, "31123"))
        //array.add(ParentUbication(arrayChild4, "66888"))

        return array
    }

    private fun LoadSpacesInRecycler(spaces: ArrayList<ResponseGetPlacesBody>){

        val filterList: ArrayList<String>?
        filterList = ArrayList()
        val spacios = arrayListOf<String>()
        spaces.forEach{
            spacios.add(it.codigo!!)
        }

        val mAdapter = Adapter_espacios()

        dialog2 = Dialog(activity)
        dialog2!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog2!!.setCancelable(false)
        dialog2!!.setContentView(R.layout.dialog_select_product)

        dialog2!!.titulo_.text = "Selecciona un espacio"
        dialog2!!.rclv_productos.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        dialog2!!.rclv_productos.setHasFixedSize(true)
        dialog2!!.rclv_productos.addItemDecoration(SimpleDividerItemDecoration(context))
        mAdapter.Adapter_espacios(spacios){SelectedSpace(it)}
        dialog2!!.rclv_productos.adapter = mAdapter

        dialog2!!.autocomplete_prods.threshold = 1

        dialog2!!.autocomplete_prods.addTextChangedListener(object: TextWatcher {      //Filtrar RecyclerView

            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                val textlength = dialog2!!.autocomplete_prods.text.toString().length
                filterList.clear()

                for (i in spaces.indices){

                    if (textlength <= spaces[i].codigo?.length!!) {

                        if (spaces[i].codigo?.toLowerCase()!!.trim().contains(dialog2!!.autocomplete_prods.text.toString().toLowerCase().trim { it <= ' ' })){
                            filterList.add(spaces[i].codigo!!)
                        }
                    }
                }

                mAdapter.Adapter_espacios(filterList){SelectedSpace(it)}
                mAdapter.notifyDataSetChanged()
            }
        })

        dialog2!!.btn_close_dialog!!.setOnClickListener{
            mvi.autocomplete_places.isEnabled = true
            dialog2!!.dismiss()
        }

        dialog2!!.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog2!!.show()
    }

    fun SelectedSpace(spacee: String){
        mvi.autocomplete_places.isEnabled = true
        if(dialog2 != null) dialog2!!.dismiss()
        mvi.autocomplete_places.setText(spacee)
        space = spacee
        getSpaceID()
    }

    private fun getSpaceID(){
        responseEspacios?.forEach{
            if(it.codigo!! == space) spaceID = it.id!!
        }
        getproductos()
    }

    override fun unSuccessRequestPlaces(error: String) {
        context.toast(error)
    }

    private var prods: ArrayList<ParentUbication>? = null
    override fun setProducts(productos: ArrayList<ParentUbication>) {

        Log.e("productos", productos.toString())

        stateProgressbar(false )

        prods = productos
        adapterProds = Adapter(context, prods!!)

        mvi.rclv_productos.addItemDecoration(SimpleDividerItemDecoration(context))
        mvi.rclv_productos.apply{
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = adapterProds
        }

        configAutocomplete()
    }

    private fun configAutocomplete(){

        val filterList: ArrayList<ParentUbication> = ArrayList()
        var bandera = -1
        var lenghtActual = 0
        var isDeleting: Boolean

        mvi.ac_buscar.threshold = 1
        mvi.ac_buscar.addTextChangedListener(object: TextWatcher {     //Filtrar RecyclerView

            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if(filterList.isEmpty()){ //Primera vez que escriben
                    lenghtActual = mvi.ac_buscar.text.toString().length
                    isDeleting = false
                }
                else{
                    val lenght = mvi.ac_buscar.text.toString().length
                    if(lenght > lenghtActual){
                        isDeleting = false
                        lenghtActual += 1
                    }
                    else{
                        isDeleting = true
                        lenghtActual -= 1
                    }
                }

                val textlength = mvi.ac_buscar.text.toString().length

                if(isDeleting){

                    for (i in filterList.indices){

                        if (textlength <= filterList[i].catalogoText.length) {

                            if(filterList[i].catalogoText.toString().trim().contains(mvi.ac_buscar.text.toString().trim {it <= ' ' })){

                                if(!prods!!.contains(filterList[i]) || prods!!.isEmpty()){
                                    prods!!.add(filterList[i])
                                    bandera = 1
                                }
                            }
                        }
                    }
                }
                else{

                    if(mvi.ac_buscar.text.toString().length == lenghtActual){

                        for (i in prods!!.indices){

                            if (textlength <= prods!![i].catalogoText.length) {

                                if (prods!![i].catalogoText.toString().trim().contains(mvi.ac_buscar.text.toString().trim {it <= ' ' })){
                                    //filterList.add(prods!![i]) //aqui se va a agregar
                                    //prods = filterList
                                }
                                else{
                                    if(!filterList.contains(prods!![i]) || filterList.isEmpty()) {
                                        filterList.add(prods!![i])
                                        bandera = -1
                                    }
                                }
                            }
                            else{

                                if(!filterList.contains(prods!![i])) {
                                    filterList.add(prods!![i])
                                    bandera = -1
                                }
                            }
                        }
                    }
                }

                val elementArray: ArrayList<ParentUbication> = ArrayList()
                prods!!.forEach {
                    filterList.forEach{item->
                        if(it.catalogoText == item.catalogoText) elementArray.add(item)
                    }
                }

                if(bandera == -1){
                    elementArray.forEach {
                        prods!!.remove(it)
                    }
                }
                else{
                    elementArray.forEach {
                        filterList.remove(it)
                    }
                }

                adapterProds!!.notifyParentDataSetChanged(true)
            }
        })
    }

    override fun errorProductos(error: String){
        mvi.progressBar.visibility = View.GONE
        context.toast(error)
    }

    override fun initReader() {}
    override fun activateReader() {}
}
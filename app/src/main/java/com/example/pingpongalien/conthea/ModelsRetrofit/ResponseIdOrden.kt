package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseIdOrden(

        @SerializedName("message")
        val msj: String
)
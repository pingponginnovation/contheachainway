package com.example.pingpongalien.conthea.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ChildUbicationViewHolder;
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication;
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbicationViewHolder;
import com.example.pingpongalien.conthea.Models.ProductChild;
import com.example.pingpongalien.conthea.R;

import java.util.ArrayList;

public class AdapterByEtiquetas extends ExpandableRecyclerAdapter<ParentUbication, ProductChild, ParentUbicationViewHolder, ChildUbicationViewHolder>{

    private LayoutInflater mInflater;

    public AdapterByEtiquetas(Context context, @NonNull ArrayList<ParentUbication> listParent) {
        super(listParent);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public ParentUbicationViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View recipeView = mInflater.inflate(R.layout.item_catalogue_parent, parentViewGroup, false);
        return new ParentUbicationViewHolder(recipeView);
    }

    @Override
    public ChildUbicationViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View ingredientView = mInflater.inflate(R.layout.item_ubications_child, childViewGroup, false);
        return new ChildUbicationViewHolder(ingredientView);
    }

    // onBind ...
    @Override
    public void onBindParentViewHolder(@NonNull ParentUbicationViewHolder recipeViewHolder, int parentPosition, @NonNull ParentUbication product) {
        recipeViewHolder.getTxtview_catalogo().setText(product.getCatalogoText());
        recipeViewHolder.getTxt_lote().setVisibility(View.GONE);
    }

    @Override
    public void onBindChildViewHolder(@NonNull ChildUbicationViewHolder childUbicationViewHolder, int parentPosition, int childPosition, @NonNull ProductChild child) {

        if(child.getActivo() != null){
            if(child.getActivo() == 1) {
                childUbicationViewHolder.getTxt_state_tag().setTextColor(Color.parseColor("#3CCB7E"));
                childUbicationViewHolder.getTxt_state_tag().setText("Etiqueta activa");
            }
            else if(child.getActivo() == 0){
                childUbicationViewHolder.getTxt_state_tag().setTextColor(Color.parseColor("#ff1744"));
                childUbicationViewHolder.getTxt_state_tag().setText("Etiqueta Inactiva");
            }
        }

        if(childUbicationViewHolder.getTxt_lote() != null) childUbicationViewHolder.getTxt_lote().setText(child.getCatalogue());
        if(!child.getUid().isEmpty()) childUbicationViewHolder.getTxt_uid().setText(child.getUid());
        else childUbicationViewHolder.getTxt_uid().setVisibility(View.GONE);
        if(child.getExpiration() != null) childUbicationViewHolder.getTxt_caducidad().setText(child.getExpiration());
        if(child.getStorage() != null) childUbicationViewHolder.getTxt_almacen().setText(child.getStorage());
        if(child.getUbication() != null) childUbicationViewHolder.getTxt_ubicacion().setText(child.getUbication());
        else childUbicationViewHolder.getTxt_ubicacion().setText("Ubicacion no encontrada");
        if(child.getCantidad() != null) childUbicationViewHolder.getTxt_cantidad().setText(String.valueOf(child.getCantidad()));
        else childUbicationViewHolder.getTxt_cantidad().setText("Cantidad no encontrada");
        if(child.getLote() != null) childUbicationViewHolder.getTxt_lote().setText(child.getLote());
        if(child.getMaker() != null) childUbicationViewHolder.getTxt_fabricante().setText(child.getMaker());
        if(child.getDescription() != null) childUbicationViewHolder.getTxt_nombre().setText(child.getDescription());
    }
}

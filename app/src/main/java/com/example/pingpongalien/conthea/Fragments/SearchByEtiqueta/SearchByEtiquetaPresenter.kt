package com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta

import android.content.Context
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseEtiquetas

class SearchByEtiquetaPresenter(view : SearchByEtiqueta.View, context: Context) : SearchByEtiqueta.Presenter {

    private var view: SearchByEtiqueta.View? = null
    private var model: SearchByEtiqueta.Model? = null

    init {
        this.view = view
        model = SearchByEtiquetaModel(this, context)
    }

    override fun addTag(tag: String) {
        model!!.addTag(tag)
    }

    override fun cleanTags(){
        model!!.cleanTags()
    }

    override fun sendTags() {
        model!!.sendTags()
    }

    override fun successTags(tagsSinUbicacion: ArrayList<ParentUbication>?, tagsConUbicacion: ArrayList<ParentUbication>?) {
        view!!.successTags(tagsSinUbicacion, tagsConUbicacion)
    }

    override fun errorTags(resp: String) {
        view!!.errorTags(resp)
    }
}
package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseLoginRoles (
    @SerializedName("permission")
    val tipo_permiso: String
)

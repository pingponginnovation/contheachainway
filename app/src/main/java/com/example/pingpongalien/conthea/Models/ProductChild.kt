package com.example.pingpongalien.conthea.Models

data class ProductChild(
        var uid: String? = null,
        var description: String? = null,
        var lote : String? = null,
        var expiration : String? = null,
        var storage: String? = null,
        var ubication : String? = null,
        var maker : String? = null,
        var cantidad: Int? = null,
        var catalogue: String? = null,
        var espacio_id: Int? = null,
        var producto_id: Int? = null,
        var activo: Int? = null
)
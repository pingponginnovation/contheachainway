package com.example.pingpongalien.conthea.Adapters.ExpandableCatalogue;

import android.content.Context;
import android.media.DrmInitData;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ChildUbicationViewHolder;
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication;
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbicationViewHolder;
import com.example.pingpongalien.conthea.Fragments.SearchCatalogue.SearchCatalogue;
import com.example.pingpongalien.conthea.Fragments.SearchCatalogue.SearchCatalogueView;
import com.example.pingpongalien.conthea.Models.ProductChild;
import com.example.pingpongalien.conthea.R;

import java.util.ArrayList;

import kotlin.Unit;

public class AdapterCatalogue extends ExpandableRecyclerAdapter<ParentCatalogue, ProductChild, ParentCatalogueViewHolder, ChildUbicationViewHolder>{

    private LayoutInflater mInflater;
    private SearchCatalogueView activity;

    public AdapterCatalogue(Context context, @NonNull ArrayList<ParentCatalogue> listParent, SearchCatalogueView activity) {
        super(listParent);
        mInflater = LayoutInflater.from(context);
        this.activity = activity;
    }

    @Override
    public ParentCatalogueViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View recipeView = mInflater.inflate(R.layout.item_catalogue_parent, parentViewGroup, false);
        return new ParentCatalogueViewHolder(recipeView);
    }

    @Override
    public ChildUbicationViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View ingredientView = mInflater.inflate(R.layout.item_catalogue_child, childViewGroup, false);
        return new ChildUbicationViewHolder(ingredientView);
    }

    @Override
    public void onBindParentViewHolder(@NonNull ParentCatalogueViewHolder parentViewHolder, int parentPosition, @NonNull ParentCatalogue parent) {
        if(parent.catalogo != null) parentViewHolder.getTxtview_catalogo().setText(parent.catalogo);
        if(parent.lote != null) parentViewHolder.getTxt_lote().setText(parent.lote);
    }

    @Override
    public void onBindChildViewHolder(@NonNull ChildUbicationViewHolder childUbicationViewHolder, int parentPosition, final int childPosition, final ProductChild child){

        if(child.getExpiration() != null) childUbicationViewHolder.getTxt_caducidad().setText(child.getExpiration());
        if(child.getStorage() != null) childUbicationViewHolder.getTxt_almacen().setText(child.getStorage());
        if(child.getUbication() != null) childUbicationViewHolder.getTxt_ubicacion().setText(child.getUbication());
        if(child.getCantidad() != null) childUbicationViewHolder.getTxt_cantidad().setText(String.valueOf(child.getCantidad()));
        if(child.getLote() != null) childUbicationViewHolder.getTxt_lote().setText(child.getLote());
        if(child.getMaker() != null) childUbicationViewHolder.getTxt_fabricante().setText(child.getMaker());
        if(child.getDescription() != null) childUbicationViewHolder.getTxt_nombre().setText(child.getDescription());

        childUbicationViewHolder.getLeer_etiquetas().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.contarProductos(child);
            }
        });
    }
}

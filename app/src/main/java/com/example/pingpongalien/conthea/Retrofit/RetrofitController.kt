package com.example.pingpongalien.conthea.Retrofit

import android.app.Activity
import android.content.Context
import android.util.Log
import android.webkit.URLUtil
import android.widget.Toast
import com.example.pingpongalien.conthea.Activities.DetailOrder.DetailOrder
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerify
import com.example.pingpongalien.conthea.Activities.DetailVerify.DetailVerifyView
import com.example.pingpongalien.conthea.Activities.Login.Login.Login
import com.example.pingpongalien.conthea.Fragments.DetailTag.DetailTag
import com.example.pingpongalien.conthea.Fragments.InitialAdjustment.AltaTag
import com.example.pingpongalien.conthea.Fragments.InitialAdjustment.InitialAdjustment
import com.example.pingpongalien.conthea.Fragments.SearchByEtiqueta.SearchByEtiqueta
import com.example.pingpongalien.conthea.Fragments.SearchByUbication.SearchByUbication
import com.example.pingpongalien.conthea.Fragments.SearchCatalogue.SearchCatalogue
import com.example.pingpongalien.conthea.Fragments.SearchOrder.SearchOrder
import com.example.pingpongalien.conthea.Fragments.SetPlace.SetPlace
import com.example.pingpongalien.conthea.Fragments.StartInventory.StartInventory
import com.example.pingpongalien.conthea.Fragments.Unsuscribe.Unsuscribe
import com.example.pingpongalien.conthea.Fragments.VerifyInventory.VerifyInventory
import com.example.pingpongalien.conthea.ModelsRetrofit.*
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

class RetrofitController(context : Context){

    val TAG = "RetrofitController"
    var URL = ""
    var api: Api? = null
    var preferences: PreferencesController
    var actividadDetailOrderview: Activity? = null
    private lateinit var adapterRetrofit: Retrofit
    var miactividad: Context? = null
    private var errorType = -1
    companion object{
        var error: String? = null
        var responseSuccess: ResponseIdOrden? = null
    }

    init {
        Log.e("init", "init")
        URL = PreferencesController(context).getUrl()
        configurateRetrofit()
        preferences = PreferencesController(context)
        miactividad = context
    }

    fun configurateRetrofit(){

        val httpClient = configurateClient()
        val client = httpClient.build()

        if(URL.contains("https://") || URL.contains("http://")) {

            if(URL.endsWith("/")){
                adapterRetrofit = Retrofit.Builder()
                        .baseUrl(URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build()
                api = adapterRetrofit.create(Api::class.java)
            }
            else errorType = 2
        }
        else errorType = 1
    }

    private fun configurateClient(): OkHttpClient.Builder {

        val httpClient = OkHttpClient.Builder()

        httpClient.addInterceptor{ chain ->
            val original = chain.request()
            val request = original.newBuilder()
                    .header("Content-Type", "application/json")
                    .header("Accept", "application/json")
                    .method(original.method(), original.body())
                    .build()
            chain.proceed(request)
        }
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(logging)
        httpClient.callTimeout(50, TimeUnit.SECONDS)
        httpClient.readTimeout(50, TimeUnit.SECONDS)
        httpClient.connectTimeout(50, TimeUnit.SECONDS)
        return httpClient
    }

    fun login(model: Login.Model, user: String, password: String, stateSession: Boolean, tokenFirebase: String){

        if(api != null){
            val callLogin  = api!!.login(RequestLogin(user, password, tokenFirebase))
            var responseSuccess: ResponseLogin?

            callLogin.enqueue(object: Callback<ResponseLogin> {

                override fun onResponse(call: Call<ResponseLogin>, response: Response<ResponseLogin>){

                    if(response.isSuccessful){
                        responseSuccess = response.body()
                        model.successRequestLogin(responseSuccess!!, stateSession)
                    }
                    else model.ErrorResponseLogin(response.message())
                }
                override fun onFailure(call: Call<ResponseLogin>, t: Throwable){
                    model.ErrorServer(t.message!!)
                }
            })
        }
        else{
            when(errorType){
                1 -> model.ErrorResponseLogin("URL debe contener http:// o https://")
                2 -> model.ErrorResponseLogin("URL debe terminar en /")
            }
        }
    }

    fun getOrders(model: SearchOrder.Model, typeSearch:Boolean){

        val call: Call<ResponseOrder>?

        if (typeSearch) call = api!!.getOrdersOut("Bearer ${preferences.getToken()}")
        else call = api!!.getOrdersIn("Bearer ${preferences.getToken()}")

        call.enqueue(object: Callback<ResponseOrder> {

            override fun onResponse(call: Call<ResponseOrder>, response: Response<ResponseOrder>){

                if (response.isSuccessful){
                    val respSuccess: ResponseOrder? = response.body()
                    model.responseOrderSuccess(respSuccess!!)
                }
                else model.responseErrorOrders(response.message())
            }

            override fun onFailure(call: Call<ResponseOrder>, t: Throwable){
                model.responseErrorOrders(t.message!!)
                Log.e("Error", t.message!!)
            }
        })
    }

    fun verifyTags(requestVerifyTag: RequestVerifyTag, model : DetailOrder.Model, typeSearch:Boolean){ //Aqui se mandan los tags leídos de un pedido de orden de salida.

        val call: Call<ResponseVerifyTag>?

        if(typeSearch) call = api!!.verifyTagsOut("Bearer ${preferences.getToken()}", requestVerifyTag)
        else call = api!!.verifyTagsIn("Bearer ${preferences.getToken()}", requestVerifyTag)

        call.enqueue(object: Callback<ResponseVerifyTag> {

            override fun onResponse(call: Call<ResponseVerifyTag>, response: Response<ResponseVerifyTag>){

                if (response.isSuccessful){
                    val respSuccess: ResponseVerifyTag? = response.body()
                    model.successRequestVerify(respSuccess!!)
                }
                else model.UnsuccessVerifyTags(response.message())
            }

            override fun onFailure(call: Call<ResponseVerifyTag>, t: Throwable){
                model.ErrorPeticionNetwork()
            }
        })
    }

    fun getNameUuid(requestNameUuid: RequestNameUuid, model : DetailOrder.Model){
        var responseSuccess: ResponseNameUuid? = null
        doAsync {
            val callTag = api!!.getNameUuid("Bearer ${preferences.getToken()}", requestNameUuid)
            val response = callTag.execute()

            if ( response.isSuccessful) responseSuccess = response.body()

            uiThread {
                if(responseSuccess != null) model.successRequestNameUuid(responseSuccess!!)
                else model.ErrorRequestNameUuid("Error de petición", 0)
            }
        }
    }

    fun sendRemision(requestOrders: RequestRemision, model : DetailOrder.Model, typeSearch:Boolean){

        var responseSuccess: ResponseRemision? = null

        doAsync {
            val callRemision = if (typeSearch) {
                api!!.sendRemisionOut("Bearer ${preferences.getToken()}", requestOrders)
            } else {
                api!!.sendRemisionIn("Bearer ${preferences.getToken()}", requestOrders)
            }
            val response = callRemision.execute()

            if(response.isSuccessful) responseSuccess = response.body()

            uiThread {
                if(responseSuccess != null) model.successRequestRemision(responseSuccess!!)
                else{
                    Log.e("Error", response.message())
                    Log.e("Errorbody", response.errorBody().toString())
                    if(response.code() == 422){
                        model.unsuccessRequestRemision("Complete su pedido antes de darle salida a la orden")
                    } else {
                        model.unsuccessRequestRemision(response.message())
                    }
                }
            }
        }
    }

    fun sendStartInventory(requestStartInventory: RequestStartInventory, model : StartInventory.Model) {
        var responseSuccess: ResponseStartInventory? = null
        doAsync {
            val callStartInventory = api!!.startInventory("Bearer ${preferences.getToken()}", requestStartInventory)
            //Log.d(TAG + " Req", requestStartInventory.toString())
            val response = callStartInventory.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null) {
                    //Log.d(TAG + "Res", responseSuccess.toString())
                    model.successStartInventory(responseSuccess!!)
                }
                else {
                    model.unSuccessStartInventory("Sucedio un error al enviar los datos")
                }
            }
        }
    }

    fun getPlaces(model: Any, from: String){

        var responseSuccess: ResponseGetPlaces? = null
        doAsync {

            val callPlaces = api!!.getPlaces("Bearer ${preferences.getToken()}")
            val response = callPlaces.execute()

            if(response.isSuccessful) responseSuccess = response.body()

            uiThread {

                if(from == "SET_PLACE") {
                    model as SetPlace.Model
                    if(responseSuccess != null) model.successRequestGetPlaces(responseSuccess!!)
                    else model.unSuccessRequestGetPlaces("Error en el servidor, intentelo de nuevo")
                }
                else{
                    model as SearchByUbication.Model
                    if(responseSuccess != null) model.successRequestGetPlaces(responseSuccess!!)
                    else model.unSuccessRequestGetPlaces("Error al obtener espacios")
                }
            }
        }
    }

    fun sendTags(requestPlaces : RequestPlaces, model : SetPlace.Model){ //Recibo los tags para que seleccionen cual cambiar de lugar.

        var responseSuccess: ResponsePlaces? = null
        doAsync {

            val callPlaces = api!!.sendTags("Bearer ${preferences.getToken()}", requestPlaces)
            val response = callPlaces.execute()

            if(response.isSuccessful) responseSuccess = response.body()
            else Log.e("nosuc", "nossucceful")

            uiThread {
                if(responseSuccess != null) model.successRequestPlaces(responseSuccess!!)
                else model.unSuccessRequestGetPlaces("Error en el servidor, intentelo de nuevo")
            }
        }
    }

    fun SendTagsLocation(tags: String, model: SetPlace.Model){ //Envio los tags que seleccionaron para cambiar de ubicacion

        val call = api!!.SendTagsForLocation("Bearer ${preferences.getToken()}", tags)
        call.enqueue(object: Callback<ResponseTagsLocation> {

            override fun onResponse(call: Call<ResponseTagsLocation>, response: Response<ResponseTagsLocation>){

                if (response.isSuccessful){
                    val respSuccess: ResponseTagsLocation? = response.body()
                    model.SuccessChangeLocationTags("Asignación de lugar exitosa")
                }
                else model.UnSuccessChangeLocationTags(response.message())
            }

            override fun onFailure(call: Call<ResponseTagsLocation>, t: Throwable){
                Log.e("Error", t.message)
            }
        })
    }

    fun getCataloge(model : SearchCatalogue.Model, catalogo: String, name: String, lote: String){

        val callCataloge = api!!.getCataloge("Bearer ${preferences.getToken()}", catalogo, name, lote)
        callCataloge.enqueue(object: Callback<ResponseCatalog> {

            override fun onResponse(call: Call<ResponseCatalog>, response: Response<ResponseCatalog>){

                if (response.isSuccessful){
                    val respSuccess: ResponseCatalog? = response.body()
                    model.successGetCataloge(respSuccess!!)
                }
                else{
                    Log.e("code", response.code().toString())
                    model.unsuccessGetCataloge(response.message())
                }
            }

            override fun onFailure(call: Call<ResponseCatalog>, t: Throwable){
                model.unsuccessGetCataloge(t.message!!)
            }
        })
    }

    fun getProducts(model : VerifyInventory.Model){

        var responseSuccess: List<ResponseProducts>? = null

        doAsync {
            val callProducts = api!!.getProducts("Bearer ${preferences.getToken()}")
            val response = callProducts.execute()

            if(response.isSuccessful)
                responseSuccess = response.body()

            uiThread {

                if(responseSuccess != null)
                    model.successProducts(responseSuccess!!)
                else
                    model.unsuccessProducts("Error en el servidor, intentelo de nuevo")
            }
        }
    }

    fun getPlaces(idProduct : Int, model : VerifyInventory.Model){
        var responseSuccess: ResponseCatalog? = null
        var error = ""

        doAsync {
            val callPlaces = api!!.getPlaces("Bearer ${preferences.getToken()}", idProduct)
            val response = callPlaces.execute()

            if(response.isSuccessful) responseSuccess = response.body()
            else {
                Log.e("error", response.message())
                error = response.message()
            }

            uiThread {
                if(responseSuccess != null)
                    model.successPlaces(responseSuccess!!)
                else
                    model.unsuccessProducts(error)
            }
        }
    }

    fun verifyUbication(verify: RequestVerify, model: DetailVerify.Model){ //Funcion que me devuelve los tags que van a ser verificados con los tags que yo lea.

        var responseSuccess: ResponseVerify? = null

        doAsync {

            val callVerify = api!!.verifyUbication("Bearer ${preferences.getToken()}", verify)
            val response = callVerify.execute()

            if(response.isSuccessful) responseSuccess = response.body() //Recibo el array de tags vàlidos que se van a verificar , desde el backend

            uiThread {
                if(responseSuccess != null){
                    //miactividad!!.toast(responseSuccess!!.response.size.toString())
                    model.successVerify(responseSuccess!!)
                }
                else model.unsuccessVerify(response.message())
            }
        }
    }

    fun verifyUbicationLocal(verify: RequestVerify, model: DetailVerify.Model, from: String){

        var responseSuccess: ResponseVerifyLocal? = null

        doAsync {

            val callVerify = api!!.verifyUbicationLocal("Bearer ${preferences.getToken()}", verify)
            val response = callVerify.execute()

            if(response.isSuccessful) responseSuccess = response.body() //Recibo el array de tags vàlidos que se van a verificar , desde el backend

            uiThread {
                if(responseSuccess != null){
                    //miactividad!!.toast(responseSuccess!!.response.size.toString())
                    model.successVerifyLocal(responseSuccess!!)
                }
                else model.unsuccessVerify(response.message())
            }
        }
    }

    fun GetTagsForDelete(requestDelete : RequestDelete, model : Unsuscribe.Model){

        val callDelete = api!!.deleteProduct("Bearer ${preferences.getToken()}", requestDelete)
        callDelete.enqueue(object: Callback<ResponseDelete> {

            override fun onResponse(call: Call<ResponseDelete>, response: Response<ResponseDelete>){

                if (response.isSuccessful){

                    val respSuccess: ResponseDelete? = response.body()
                    model.successRequestPlaces(respSuccess!!)
                }
                else model.unSuccessRequestGetPlaces(response.message())
            }

            override fun onFailure(call: Call<ResponseDelete>, t: Throwable){
                Log.e("Error", t.message)
                model.unSuccessRequestGetPlaces("No se completó la petición, error de conexión a internet")
            }
        })
    }

    fun DeleteTagsSelected(tags: String, model: Unsuscribe.Model){

        val callDelete = api!!.DeleteTags("Bearer ${preferences.getToken()}", tags)
        callDelete.enqueue(object: Callback<ResponseDeletedTags> {

            override fun onResponse(call: Call<ResponseDeletedTags>, response: Response<ResponseDeletedTags>){

                if (response.isSuccessful){

                    val respSuccess: ResponseDeletedTags? = response.body()
                    model.SuccessDeletedTags("Los tags han sido eliminados correctamente")
                }
                else model.UnSuccessDeletedTags("Error en el servidor, intentelo de nuevo")
            }

            override fun onFailure(call: Call<ResponseDeletedTags>, t: Throwable){
                Log.e("Error", t.message)
                model.UnSuccessDeletedTags("No se completó la petición, error de conexión a internet")
            }
        })
    }

    fun detailTag(requestDetailTag: RequestDetailTag, model : DetailTag.Model){

        val callDetail = api!!.detailTag("Bearer ${preferences.getToken()}", requestDetailTag)
        callDetail.enqueue(object: Callback<ResponseDetailTag> {

            override fun onResponse(call: Call<ResponseDetailTag>, response: Response<ResponseDetailTag>){

                if (response.isSuccessful){
                    val respSuccess: ResponseDetailTag? = response.body()
                    model.successRequestDetailTag(respSuccess!!)
                }
                else{
                    if(response.code() == 401) model.unsuccessRequestDetailTag(response.message())
                    else model.unsuccessRequestDetailTag("El item no esta registrado en el sistema")
                }
            }

            override fun onFailure(call: Call<ResponseDetailTag>, t: Throwable){
                Log.e("Error", t.message)
                model.unsuccessRequestDetailTag("No se completó la petición, error de conexión a internet")
            }
        })
    }

    fun detailTag(requestDetailTag: RequestDetailTag, model : SearchCatalogue.Model){

        var responseSuccess: ResponseDetailTag? = null

        doAsync {

            val callDetail = api!!.detailTag("Bearer ${preferences.getToken()}", requestDetailTag)
            val response = callDetail.execute()
            if(response.isSuccessful)
                responseSuccess = response.body()
            uiThread {
                if(responseSuccess != null)
                    model.successDetailTag(responseSuccess!!)
                else
                    model.unsuccessDetailTag("El item no esta registrado en el sistema")
            }
        }
    }

    fun sendOrderID(ordenID: String, actividad: Activity){

        actividadDetailOrderview = actividad

        doAsync{

            val callDetail = api!!.sendIDOrden("Bearer ${preferences.getToken()}", ordenID)
            val response = callDetail.execute()

            if(response.isSuccessful) responseSuccess = response.body()
            else error = response.message()

            uiThread {

                if(responseSuccess == null) Toast.makeText(actividadDetailOrderview, error.toString(), Toast.LENGTH_LONG).show()
                else{

                    //Toast.makeText(actividadDetailOrderview, responseSuccess!!.msj, Toast.LENGTH_LONG).show()
                    if(responseSuccess!!.msj.equals("false", true)){

                        actividadDetailOrderview!!.finish()
                        Toast.makeText(actividadDetailOrderview, "Esta orden está ocupada", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    fun ajusteInventario(ajuste: String, model: DetailVerify.Model){

        var success: ResponseAjuste? = null

        doAsync{

            val call = api!!.checkAjuste("Bearer ${preferences.getToken()}", ajuste)
            val resp = call.execute()

            if(resp.isSuccessful) success = resp.body()
            else error = resp.message()

            uiThread {
                if(success == null) model.UnsuccessAmountIntelisis(error.toString())
                else model.SuccessAmountIntelisis(success!!)
            }
        }
    }

    fun ajusteInventario2(ajuste: String, actividad: DetailVerifyView){

        val activi = actividad
        var success: ResponseAjuste2? = null

        doAsync{

            val call = api!!.checkAjuste2("Bearer ${preferences.getToken()}", ajuste)
            val resp = call.execute()

            if(resp.isSuccessful) success = resp.body()
            else error = resp.message()

            uiThread {
                if(success == null){
                    activi.deleteBackup()
                    Toast.makeText(activi, error.toString(), Toast.LENGTH_LONG).show()
                }
                else Toast.makeText(activi, success!!.msj, Toast.LENGTH_LONG).show()
            }
        }
    }

    fun SendTagsRead(adjustment: String, model: InitialAdjustment.Model){

        var success: ResponseInitialAdjustment? = null

        doAsync{

            val call = api!!.SendInitialAdjustment("Bearer ${preferences.getToken()}", adjustment)
            val resp = call.execute()

            if(resp.isSuccessful) success = resp.body()
            else error = resp.message()

            uiThread {

                if(success == null) model.Unsuccess(error.toString())
                else model.Success(success!!)
            }
        }
    }

    fun GetTagAlta(tag: String, model: AltaTag.Model){

        val call = api!!.DarAltaTag("Bearer ${preferences.getToken()}", tag)
        call.enqueue(object: Callback<ResponseAltaTag> {

            override fun onResponse(call: Call<ResponseAltaTag>, response: Response<ResponseAltaTag>){

                if (response.isSuccessful){

                    val respSuccess: ResponseAltaTag? = response.body()
                    model.Success(respSuccess!!)
                }
                else model.Unsuccess(error.toString())
            }

            override fun onFailure(call: Call<ResponseAltaTag>, t: Throwable){
                Log.e("Error", t.message)
                model.Unsuccess("No se completó la petición, error de conexión a internet")
            }
        })
    }

    fun RegisterTags(tag: String, model: AltaTag.Model){

        val call = api!!.RegisterTags("Bearer ${preferences.getToken()}", tag)
        call.enqueue(object: Callback<ResponsRegisterTags> {

            override fun onResponse(call: Call<ResponsRegisterTags>, response: Response<ResponsRegisterTags>){

                if (response.isSuccessful){

                    val respSuccess: ResponsRegisterTags? = response.body()
                    model.VerifyTagsInAnyOrder(respSuccess!!)
                }
                else model.UnSuccessTagsRegister(response.message())
            }

            override fun onFailure(call: Call<ResponsRegisterTags>, t: Throwable){
                Log.e("Error", t.message)
                model.UnSuccessTagsRegister("No se completó la petición, error de conexión a internet")
            }
        })
    }

    fun getProductos(space_id: Int, model: SearchByUbication.Model){

        val call = api!!.getproductos("Bearer ${preferences.getToken()}", space_id)
        call.enqueue(object: Callback<ResponseCatalog> {

            override fun onResponse(call: Call<ResponseCatalog>, response: Response<ResponseCatalog>){

                if (response.isSuccessful){

                    val respSuccess: ResponseCatalog? = response.body()
                    model.successProducts(respSuccess!!)
                }
                else model.unSuccessproducts(error.toString())
            }

            override fun onFailure(call: Call<ResponseCatalog>, t: Throwable){
                model.unSuccessproducts("No se completó la petición, error de conexión a internet")
            }
        })
    }

    fun search_by_etiquetas(model: SearchByEtiqueta.Model, req: RequestEtiquetas){

        val call = api!!.search_by_etiqueta("Bearer ${preferences.getToken()}", req)
        call.enqueue(object: Callback<ResponseEtiquetas> {

            override fun onResponse(call: Call<ResponseEtiquetas>, response: Response<ResponseEtiquetas>){

                if (response.isSuccessful){
                    val respSuccess: ResponseEtiquetas? = response.body()
                    model.successTags(respSuccess)
                }
                else model.errorTags(error.toString())
            }

            override fun onFailure(call: Call<ResponseEtiquetas>, t: Throwable){
                model.errorTags("No se completó la petición, error de conexión a internet")
            }
        })
    }
}
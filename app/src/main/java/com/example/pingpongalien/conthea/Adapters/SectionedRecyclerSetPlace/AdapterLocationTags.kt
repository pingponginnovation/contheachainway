package com.example.pingpongalien.conthea.Adapters.SectionedRecyclerSetPlace

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pingpongalien.conthea.Models.ChildLocationTags
import com.example.pingpongalien.conthea.Models.DataLocation
import com.example.pingpongalien.conthea.R
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_asign_place_child.view.*
import kotlinx.android.synthetic.main.item_asign_place_parent.view.*

class AdapterLocationTags(context: Context, arrayTags: ArrayList<ParentLocationTags>, listener: (DataLocation) -> Unit):
        SectionRecyclerViewAdapter<ParentLocationTags, ChildLocationTags, ParentViewHolderTagsLocation, ChildViewHolderTagsLocation>(context, arrayTags) {

    private var inflater: LayoutInflater? = null
    private var listener: (DataLocation) -> Unit

    init {
        inflater = LayoutInflater.from(context)
        this.listener = listener
    }

    override fun onCreateSectionViewHolder(sectionViewGroup: ViewGroup?, viewType: Int): ParentViewHolderTagsLocation{
        val view: View = inflater!!.inflate(R.layout.item_asign_place_parent, sectionViewGroup, false)
        return ParentViewHolderTagsLocation(view)
    }

    override fun onBindSectionViewHolder(parent: ParentViewHolderTagsLocation?, sectionPosition: Int, section: ParentLocationTags?) {
        parent!!.catalogo.text = section!!.getCatalogoText()
        parent.cantidad.text = section.getCantidadText()

        parent.itemView.checkbox_all.setOnClickListener{
            if(parent.itemView.checkbox_all.isChecked){
                val data = DataLocation(true, section.catalogoText, "CATALOGO")
                listener(data)
            }
            else{
                val data = DataLocation(false, section.catalogoText, "CATALOGO")
                listener(data)
            }
        }
    }

    override fun onCreateChildViewHolder(childViewGroup: ViewGroup?, viewType: Int): ChildViewHolderTagsLocation {
        val view: View = inflater!!.inflate(R.layout.item_asign_place_child, childViewGroup, false)
        return ChildViewHolderTagsLocation(view)
    }

    override fun onBindChildViewHolder(child: ChildViewHolderTagsLocation?, sectionPosition: Int, childPosition: Int, childSection: ChildLocationTags?) {
        child!!.lote.text = childSection!!.lote
        child.almacen.text = childSection.almacen

        child.itemView.checkbox_select.isChecked = childSection.selected
        child.itemView.checkbox_select.setOnClickListener{
            if(child.itemView.checkbox_select.isChecked){
                val data = DataLocation(true, childSection.uuid!!, "UID")
                listener(data)
            }
            else{
                val data = DataLocation(false, childSection.uuid!!, "UID")
                listener(data)
            }
        }
    }
}
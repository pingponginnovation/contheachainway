package com.example.pingpongalien.conthea.Fragments.StartInventory

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseStartInventory
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import org.jetbrains.anko.toast

class StartInventoryView : ReaderFragment(), StartInventory.View {

    val TAG = "StartInventoryView"

    var presenter: StartInventory.Presenter? = null

    private var mOperationTime = 0
    private var mPowerLevel: Int = 0
    private var mIsReportRssi = false
    private var mElapsedTick: Long = 0
    private var mTick: Long = 0
    private val SKIP_KEY_EVENT_TIME: Long = 1000
    private var mThread: Thread? = null
    private var mIsAliveThread = false
    private val UPDATE_TIME = 500
    private var m_timeFlag = 0
    private var m_timeSec: Long = 1
    private val MAX_POWER_LEVEL = 300
    //TRUE CONTINUE, FALSE SINGLE
    private var modeRead = true
    private val POWER_LEVEL = 110

    lateinit var txt_total: TextView
    lateinit var txt_cantidad: TextView
    lateinit var btn_reset: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):View?{
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_start_inventory, container, false)
        presenter = StartInventoryPresenter(this, context, PreferencesController(context))
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPowerLevel = MAX_POWER_LEVEL
        mOperationTime = 0
        mTick = 0
        mElapsedTick = 0
        setHasOptionsMenu(true)
        txt_total = activity.findViewById(R.id.txt_total)
        txt_cantidad = activity.findViewById(R.id.txt_cantidad)
        btn_reset = activity.findViewById(R.id.btn_reset)
        presenter!!.getData()
        eventsView()
    }

    override fun successStartInventory(responseSuccess: ResponseStartInventory?) {
        setData(responseSuccess!!.response!![0].read!!, responseSuccess.response!![1].saved!!)
    }

    override fun unSuccessStartInventory(error: String) {
        context.toast(error)
    }

    override fun setData(read: String, saved: String) {
        txt_total.text = read
        txt_cantidad.text = saved
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_detail_verify, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.item_potency -> showPotency()
            R.id.item_type_read -> presenter!!.setModeRead(item.title.toString(), item)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setModeRead(mode: Boolean){
        this.modeRead = mode
    }

    override fun setItemNameMenu(name: String, item: MenuItem) {
        item.title = name
    }

    private fun eventsView() {
        btn_reset.setOnClickListener { presenter!!.resetData() }
    }

    private fun showPotency() {
    }

    override fun initReader() {
    }

    override fun activateReader() {
        // Set Power Level
        setPowerLevel(mPowerLevel)

        // Set Operation Time
        setOperationTime(mOperationTime)
    }

    protected fun setPowerLevel(power: Int) {
        //POTENCIA
        mPowerLevel = power
    }

    protected fun setOperationTime(time: Int) {
        mOperationTime = time
    }

    fun setKeyDown(keyCode : Int, event: KeyEvent){
    }

    fun setKeyUp(keyCode : Int, event: KeyEvent){
    }


    protected fun playSuccess() {
    }
}

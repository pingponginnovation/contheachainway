package com.example.pingpongalien.conthea.ModelsRetrofit
import com.google.gson.annotations.SerializedName

data class ResponseTagsLocation(
        @SerializedName("response")
        val response: String? = null
)
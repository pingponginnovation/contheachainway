package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseAltaTag(

        @SerializedName("response")
        val respuesta: ArrayList<ResponseAltaTagItems>
)
package com.example.pingpongalien.conthea.Fragments.SearchByUbication

import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalog
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseGetPlaces
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseGetPlacesBody

interface SearchByUbication {

    interface View{
        fun setProducts(productos: ArrayList<ParentUbication>)
        fun errorProductos(error: String)
        fun setPlaces(aPlaces : ArrayList<ResponseGetPlacesBody>)
        fun unSuccessRequestPlaces(error : String)
    }

    interface Presenter{
        //MODEL
        fun getProducts(space: Int)
        fun getPlaces(from: String)

        //VIEW
        fun setProducts(productos: ArrayList<ParentUbication>)
        fun errorProductos(error: String)
        fun setPlaces(aPlaces : ArrayList<ResponseGetPlacesBody>)
        fun unSuccessRequestPlaces(error : String)
    }

    interface Model{
        fun getProducts(space: Int)
        fun successProducts(response : ResponseCatalog)
        fun unSuccessproducts(error : String)
        fun requestPlaces(from: String)
        fun successRequestGetPlaces(response : ResponseGetPlaces)
        fun unSuccessRequestGetPlaces(error : String)
    }
}
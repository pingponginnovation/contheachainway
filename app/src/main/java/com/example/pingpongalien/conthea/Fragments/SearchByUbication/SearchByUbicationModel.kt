package com.example.pingpongalien.conthea.Fragments.SearchByUbication

import android.content.Context
import android.util.Log
import com.example.pingpongalien.conthea.Adapters.ExpandableCatalogue.ParentCatalogue
import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication
import com.example.pingpongalien.conthea.Helpers.HelperDate
import com.example.pingpongalien.conthea.Models.ProductChild
import com.example.pingpongalien.conthea.ModelsRetrofit.*
import com.example.pingpongalien.conthea.Retrofit.RetrofitController

class SearchByUbicationModel(presenter : SearchByUbication.Presenter, context : Context): SearchByUbication.Model{

    private var presenter: SearchByUbication.Presenter? = null
    private var appContext: Context
    private val parentArray: ArrayList<ParentUbication> = ArrayList()
    private var aPlaces: ArrayList<ResponseGetPlacesBody>? = null
    private var idPlace = 0

    init {
        this.presenter = presenter
        this.appContext = context
    }

    override fun requestPlaces(from: String) {
        val retrofit = RetrofitController(appContext)
        retrofit.getPlaces(this, from)
    }

    override fun successRequestGetPlaces(response: ResponseGetPlaces) {
        if(response.aPlaces.size > 0) {
            aPlaces = response.aPlaces
            setPlaces()
            setId()
        }
        else presenter!!.unSuccessRequestPlaces("No existen ubicaciones activas")
    }

    fun setPlaces(){
        presenter!!.setPlaces(aPlaces!!)
    }

    fun setId(){
        idPlace = aPlaces!![0].id!!
    }

    override fun unSuccessRequestGetPlaces(error: String) {
        presenter!!.unSuccessRequestPlaces(error)
    }

    override fun getProducts(space: Int){
        val retrofit = RetrofitController(appContext)
        retrofit.getProductos(space, this)
    }

    override fun successProducts(response: ResponseCatalog){
        Log.e("byUbication", response.toString())
        if(response.response!!.isNotEmpty()){
            ConfigProducts(response.response)
            presenter!!.setProducts(parentArray)
        }
        else presenter!!.errorProductos("No se encontraron productos")
    }

    override fun unSuccessproducts(error: String) {
        presenter!!.errorProductos(error)
    }

    private fun ConfigProducts(productos: List<ResponseCatalogItem?>?){

        if(parentArray.isNotEmpty()) parentArray.clear()

        productos?.forEach{

            val arrayChilds: ArrayList<ProductChild> = arrayListOf() //Array de hijos

            val almacen = it?.almacen?.codigo!! + " " + it.almacen.nombre!!
            val datos = it.producto?.nombre!!.split(":").toTypedArray()
            val catalogo = datos[0]
            val description = datos[1]

            val child = ProductChild("", description, it.lote!!, it.caducidad, almacen, it.espacio?.codigo?:"", it.producto.fabricante, it.cantidad,
                    catalogo, it.espacio_actual?.id, it.producto.id) //Creamos hijo
            arrayChilds.add(child) //Añadimos hijo al array

            if(parentArray.isNullOrEmpty()) parentArray.add(ParentUbication(arrayChilds, catalogo, -1)) //Se crea el primer padre
            else{

                var bandera = -1

                parentArray.forEach{parent ->

                    arrayChilds.forEach{child ->

                        if(parent.catalogoText == child.catalogue){ //Agregamos nuevo hijo a padre existente en el array
                            val newChild = ProductChild("", child.description, child.lote, child.expiration, child.storage, child.ubication, child.maker,
                                    child.cantidad, child.catalogue, child.espacio_id, child.producto_id)
                            parent.childList!!.add(newChild)
                            bandera = 1
                        }
                    }
                }

                if(bandera == -1) parentArray.add(ParentUbication(arrayChilds, catalogo, -1)) //Creamos nuevo padre, en caso de no existir
            }
        }
    }
}
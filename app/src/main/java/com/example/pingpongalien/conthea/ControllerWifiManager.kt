package com.example.pingpongalien.conthea

import android.annotation.TargetApi
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.util.Log

class ControllerWifiManager(context: Context){

    private var contex: Context? = null
    private var cm: ConnectivityManager
    private var wifiManager: WifiManager
    init {
        if(this.contex == null) this.contex = context
        cm = contex!!.getSystemService(ReaderActivity.CONNECTIVITY_SERVICE) as ConnectivityManager
        wifiManager = contex!!.getSystemService(ReaderActivity.WIFI_SERVICE) as WifiManager
    }

    fun IsNetworkConnected(): Boolean{
        //Log.e("ES", "netowr")
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        return isConnected
    }

    fun GetStrenghtWIFI(): Int{
        //Log.e("ES", "wifistrenght")
        val numberOfLevels = 5
        val wifiInfo: WifiInfo = wifiManager.connectionInfo
        return WifiManager.calculateSignalLevel(wifiInfo.rssi, numberOfLevels)
    }

    fun GetNetworkSpeed(): Int{
        val wifiInfo: WifiInfo = wifiManager.connectionInfo
        Log.e("SPEED", wifiInfo.linkSpeed.toString())
        return wifiInfo.linkSpeed
    }
}
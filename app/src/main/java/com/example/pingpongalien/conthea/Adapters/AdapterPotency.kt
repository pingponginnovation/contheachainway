package com.example.pingpongalien.conthea.Adapters

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.pingpongalien.conthea.R
import kotlinx.android.synthetic.main.item_potency.view.*

class AdapterPotency(private val array: ArrayList<Int>, val listener: (Int) -> Unit):
    RecyclerView.Adapter<AdapterPotency.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(array[position], listener)

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){

        fun bind(num: Int, listener: (Int) -> Unit) = with(itemView){
            this.txt_potency.text = num.toString()
            setOnClickListener{
                listener(num)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_potency, parent, false))
    }

    override fun getItemCount(): Int {
        return array.size
    }
}
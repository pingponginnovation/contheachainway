package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseAjuste2(
        @SerializedName("message")
        val msj: String
)
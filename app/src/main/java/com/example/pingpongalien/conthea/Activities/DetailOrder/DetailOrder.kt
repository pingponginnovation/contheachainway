package com.example.pingpongalien.conthea.Activities.DetailOrder

import android.view.MenuItem
import com.example.pingpongalien.conthea.Adapters.AdapterProducts
import com.example.pingpongalien.conthea.Models.TagNoMatch
import com.example.pingpongalien.conthea.ModelsRetrofit.*

/**
 * Created by AOR on 28/11/17.
 */
interface DetailOrder{

    interface View{
        fun successRequestRemision(response : String)
        fun unsuccessRequestRemision(error : String)
        fun setModeRead(mode: Boolean)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun tagsNoMatch(aTagsNoMatch: ArrayList<TagNoMatch>)
        fun setTotal(total : Int, msj: String)
        fun showNoProductsToSend()
        fun showNoCompleteOrder(status: Int)
        fun SetProgressBar(state: Boolean)
        fun ErrorRequestNameUuid(msj: String, total: Int)
        fun ErrorPeticionNetwork()
        fun UnsuccessVerifyTags(resp: String)
    }

    interface Presenter{
        fun addTag(tag: String)
        fun setLotes(aProducts: List<ResponseOrderProduct>)
        fun setProducts(aProducts: List<ResponseOrderProduct>, movimientoType: String)
        fun successRequestRemision(response : String)
        fun unsuccessRequestRemision(error : String)
        fun setAdapter(adapter: AdapterProducts)
        fun sendData(totalProducts : Int)
        fun setModeRead(mode: Boolean)
        fun setModeRead(nameItem : String, item: MenuItem)
        fun setItemNameMenu(name : String, item: MenuItem)
        fun searchTag()
        fun tagsNoMatch(aTagsNoMatch: ArrayList<TagNoMatch>)
        fun setTotal(total : Int, msj: String)
        fun showNoProductsToSend()
        fun showNoCompleteOrder(status: Int)
        fun sendData()
        fun setTypeSearch(typeSearch: Boolean)
        fun SetProgressBar(state: Boolean)
        fun ErrorRequestNameUuid(msj: String, total: Int)
        fun ErrorPeticionNetwork()
        fun LoadLastStateOrder(tags: ArrayList<String>)
        fun UnsuccessVerifyTags(resp: String)
    }

    interface Model{
        fun setLotes(aProducts: List<ResponseOrderProduct>)
        fun setProducts(aProducts: List<ResponseOrderProduct>, movimientoType: String)
        fun addTagForVerify(tag: String)
        fun successRequestVerify(responseVerifyTag: ResponseVerifyTag)
        fun successRequestRemision(response : ResponseRemision)
        fun successRequestNameUuid(response : ResponseNameUuid)
        fun ErrorRequestNameUuid(msj: String, total: Int)
        fun unsuccessRequestRemision(error : String)
        fun setAdapter(adapter: AdapterProducts)
        fun setModeRead(nameItem : String, item: MenuItem)
        fun searchTag()
        fun sendData(totalProducts : Int)
        fun sendData()
        fun setTypeSearch(typeSearch: Boolean)
        fun ErrorPeticionNetwork()
        fun UnsuccessVerifyTags(resp: String)
        fun LoadLastStateOrder(tags: ArrayList<String>)
    }
}
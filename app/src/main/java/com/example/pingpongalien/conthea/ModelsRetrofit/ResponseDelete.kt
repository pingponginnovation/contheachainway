package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseDelete (
    @SerializedName("response")
    var respuesta: ArrayList<ResponseDeleteItems>
)

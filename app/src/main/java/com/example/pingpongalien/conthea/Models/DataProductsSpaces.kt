package com.example.pingpongalien.conthea.Models

import com.example.pingpongalien.conthea.Adapters.ExpandableUbications.ParentUbication

data class DataProductsSpaces(
        val con_espacio: ArrayList<ParentUbication>? = null,
        val sin_espacio: ArrayList<ParentUbication>? = null
)
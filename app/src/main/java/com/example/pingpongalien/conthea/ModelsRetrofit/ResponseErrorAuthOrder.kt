package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

data class ResponseErrorAuthOrder(
        @SerializedName("error")
        val error: String,

        @SerializedName("message")
        val msj: String
)
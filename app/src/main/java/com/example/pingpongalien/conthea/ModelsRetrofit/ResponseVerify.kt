package com.example.pingpongalien.conthea.ModelsRetrofit

import com.google.gson.annotations.SerializedName

/**
 * Created by AOR on 8/12/17.
 */
data class ResponseVerify(
        @SerializedName("lectura_dispositivo")
        val lectura_hecha: Int? = null,

        @SerializedName("parcial")
        val total_parcial: Int? = null,

        @SerializedName("total")
        val total_productos: Int? = null,

        @SerializedName("response")
        val response: ArrayList<String>? = null
)
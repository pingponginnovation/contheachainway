package com.example.pingpongalien.conthea.ModelsRetrofit

/**
 * Created by AOR on 22/11/17.
 */
data class RequestLogin(val email:String, val password: String, val tokenFirebase: String)
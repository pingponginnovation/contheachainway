package com.example.pingpongalien.conthea.Fragments.Unsuscribe

import android.content.Context
import android.util.Log
import android.view.MenuItem
import com.example.pingpongalien.conthea.Models.TagsForDelete
import com.example.pingpongalien.conthea.ModelsRetrofit.RequestDelete
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDelete
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseDeleteItems
import com.example.pingpongalien.conthea.Retrofit.RetrofitController
import com.google.gson.Gson

class UnsuscribeModel(presenter : Unsuscribe.Presenter, context: Context): Unsuscribe.Model {

    var presenter: Unsuscribe.Presenter? = null
    val TAG = "UnsuscribeModel"
    var aTags = arrayListOf<String>()
    var appContext: Context
    val arrayTest = arrayListOf("Tubos B15", "Banco de sangre", "Coronavirus Wuhan") //test

    init {
        this.presenter = presenter
        this.appContext = context
    }

    override fun addTag(tag: String){
        aTags.add(tag)
    }

    override fun sendTags(){
        val retrofit = RetrofitController(appContext)
        retrofit.GetTagsForDelete(RequestDelete(aTags), this)
    }

    override fun successRequestPlaces(response: ResponseDelete){

        Log.e("Es", response.toString())

        ClearArrayDelete()
        FillTagsForDelete(response.respuesta, false)
        presenter!!.SetAdapterDelete(arrayTagsForDelete)
        aTags.clear()
    }

    override fun unSuccessRequestGetPlaces(error : String){
        presenter!!.unSuccessRequestPlaces(error)
        aTags.clear()
    }

    override fun setModeRead(nameItem: String, item: MenuItem) {
        if(nameItem.equals("Lectura unica")) {
            presenter!!.setModeRead(true)
            presenter!!.setItemNameMenu("Lectura continua", item)
        } else{
            presenter!!.setModeRead(false)
            presenter!!.setItemNameMenu("Lectura unica", item)
        }
    }

    override fun SendTagsDelete(tags: ArrayList<TagsForDelete>){
        val retrofit = RetrofitController(appContext)
        val gson = Gson()
        val tagss = gson.toJson(tags)
        retrofit.DeleteTagsSelected(tagss, this)
    }

    override fun SuccessDeletedTags(msj: String){
        presenter!!.SuccessDeletedTags(msj)
    }

    override fun UnSuccessDeletedTags(msj: String){
        presenter!!.UnSuccessDeletedTags(msj)
    }

    fun ClearArrayDelete(){
        if(!arrayTagsForDelete.isEmpty()) arrayTagsForDelete.clear()
    }

    companion object{

        val arrayTagsForDelete = ArrayList<TagsForDelete>()

        fun FillTagsForDelete(productos: ArrayList<ResponseDeleteItems>, select: Boolean){

            productos.forEach{produc->
                arrayTagsForDelete.add(TagsForDelete(produc.uuid, produc.article, produc.lote, produc.caducidad, produc.storage, select))
            }
        }
    }
}
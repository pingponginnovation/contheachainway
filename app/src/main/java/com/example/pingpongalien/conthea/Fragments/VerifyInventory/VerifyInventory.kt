package com.example.pingpongalien.conthea.Fragments.VerifyInventory

import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalog
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseCatalogItem
import com.example.pingpongalien.conthea.ModelsRetrofit.ResponseProducts

/**
 * Created by AOR on 7/12/17.
 */
interface VerifyInventory {

    interface View{
        fun setProducts(aProducts : List<ResponseProducts>)
        fun unsuccessProducts(error : String)
        fun setPlaces(aPlaces : List<Map<String, String>>)
        fun showDetailVerify(catalog: ResponseCatalogItem)
        fun visibilityWithoutProducts()
    }

    interface Presenter {
        //MODEL
        fun getProducts()
        fun getPlaces(position : Int)
        fun getOnePlace(position : Int)

        //VIEW
        fun setProducts(aProducts : List<ResponseProducts>)
        fun unsuccessProducts(error : String)
        fun setPlaces(aPlaces : List<Map<String, String>>)
        fun showDetailVerify(catalog: ResponseCatalogItem)
        fun visibilityWithoutProducts()
    }

    interface Model{
        fun getProducts()
        fun successProducts(response : List<ResponseProducts>)
        fun successPlaces(response : ResponseCatalog)
        fun unsuccessProducts(error : String)
        fun getPlaces(position : Int)
        fun getOnePlace(position : Int)
    }

}
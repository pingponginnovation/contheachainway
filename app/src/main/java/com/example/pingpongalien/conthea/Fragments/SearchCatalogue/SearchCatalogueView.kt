package com.example.pingpongalien.conthea.Fragments.SearchCatalogue

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import com.example.pingpongalien.conthea.Activities.Login.Login.Core.CoreView
import com.example.pingpongalien.conthea.Adapters.ExpandableCatalogue.AdapterCatalogue
import com.example.pingpongalien.conthea.Adapters.ExpandableCatalogue.ParentCatalogue
import com.example.pingpongalien.conthea.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.pingpongalien.conthea.Models.ProductChild
import com.example.pingpongalien.conthea.R
import com.example.pingpongalien.conthea.ReaderFragment
import com.example.pingpongalien.conthea.SharedPreferences.PreferencesController
import kotlinx.android.synthetic.main.fragment_busqueda_catalogo.view.*
import kotlinx.android.synthetic.main.fragment_leer_etiquetas.view.*
import kotlinx.android.synthetic.main.item_asign_place_parent.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import java.util.*

class SearchCatalogueView(val core: CoreView, var catalogo: String, var nombre: String, var lote: String): ReaderFragment(), SearchCatalogue.View{

    val TAG = "SearchCatalogueView"
    var presenter: SearchCatalogue.Presenter? =null
    lateinit var datePickerStart: DatePickerDialog
    lateinit var datePickerFinal: DatePickerDialog
    lateinit var mvi: View
    private var adapterCatalogue: AdapterCatalogue? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mvi = inflater!!.inflate(R.layout.fragment_busqueda_catalogo, container, false)
        return mvi
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = SearchCataloguePresenter(this, mvi.context)
        core.changeTitleToolbar("Búsqueda por catálogo")

        if(catalogo.isNotEmpty() || nombre.isNotEmpty() || lote.isNotEmpty()){ //Cuando regresamos de la vista de verifyLocal para buscar otro

            when{
                catalogo.isNotEmpty() -> mvi.edt_buscar.setText(catalogo)
                nombre.isNotEmpty() -> mvi.edt_buscar.setText(nombre)
                else -> mvi.edt_buscar.setText(lote)
            }

            HideViews(true)
            presenter!!.getCatalogue(catalogo, nombre, lote)
        }

        mvi.btn_buscar.setOnClickListener{
            if(mvi.edt_buscar.text.toString().isEmpty()) mvi.context.toast("Campo vacío")
            else{

                HideViews(true)

                when{
                    mvi.radio_catalogo.isChecked -> {
                        catalogo = mvi.edt_buscar.text.toString()
                        nombre = ""
                        lote = ""
                    }
                    mvi.radio_nombre.isChecked -> {
                        nombre = mvi.edt_buscar.text.toString()
                        catalogo = ""
                        lote = ""
                    }
                    else -> {
                        lote = mvi.edt_buscar.text.toString()
                        nombre = ""
                        catalogo = ""
                    }
                }

                presenter!!.getCatalogue(catalogo, nombre, lote)
            }
        }
    }

    private fun HideViews(state: Boolean){
        if(state){
            mvi.rclv_catalogos.visibility = View.GONE
            mvi.ac_buscar_catalogo.visibility = View.GONE
            mvi.progress_bar.visibility = View.VISIBLE
        }
        else{
            mvi.progress_bar.visibility = View.GONE
            mvi.ac_buscar_catalogo.visibility = View.VISIBLE
            mvi.rclv_catalogos.visibility = View.VISIBLE
        }
    }

    override fun showErrorUnsuccessCatalog(error: String){
        if(error == "Unauthorized") showExpiredSession()
        mvi.progress_bar.visibility = View.GONE
        mvi.context.toast(error)
    }

    override fun SuccessCatalogue(){
        HideViews(false)
    }

    override fun setProductos(aProducts: ArrayList<ParentCatalogue>) {
        adapterCatalogue = AdapterCatalogue(context, aProducts, this)
        mvi.rclv_catalogos.addItemDecoration(SimpleDividerItemDecoration(context))
        mvi.rclv_catalogos.apply{
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = adapterCatalogue
        }
        configAutocomplete(aProducts)
    }

    fun contarProductos(childSelected: ProductChild){
        core.contarProductos(childSelected, catalogo, nombre, lote)
    }

    private var prods: ArrayList<ParentCatalogue>? = null
    private fun configAutocomplete(productos: ArrayList<ParentCatalogue>){

        prods = productos

        val filterList: ArrayList<ParentCatalogue> = ArrayList()
        var bandera = -1
        var lenghtActual = 0
        var isDeleting: Boolean

        mvi.ac_buscar_catalogo.threshold = 1
        mvi.ac_buscar_catalogo.addTextChangedListener(object: TextWatcher {     //Filtrar RecyclerView

            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if(filterList.isEmpty()){ //Primera vez que escriben
                    lenghtActual = mvi.ac_buscar_catalogo.text.toString().length
                    isDeleting = false
                }
                else{
                    val lenght = mvi.ac_buscar_catalogo.text.toString().length
                    if(lenght > lenghtActual){
                        isDeleting = false
                        lenghtActual += 1
                    }
                    else{
                        isDeleting = true
                        lenghtActual -= 1
                    }
                }

                val textlength = mvi.ac_buscar_catalogo.text.toString().length

                if(isDeleting){

                    for (i in filterList.indices){

                        if (textlength <= filterList[i].catalogoText.length) {

                            if(filterList[i].catalogoText.toString().trim().contains(mvi.ac_buscar_catalogo.text.toString().trim {it <= ' ' })){

                                if(!prods!!.contains(filterList[i]) || prods!!.isEmpty()){
                                    prods!!.add(filterList[i])
                                    bandera = 1
                                }
                            }
                        }
                    }
                }
                else{

                    if(mvi.ac_buscar_catalogo.text.toString().length == lenghtActual){

                        for (i in prods!!.indices){

                            if (textlength <= prods!![i].catalogoText.length) {

                                if (prods!![i].catalogoText.toString().trim().contains(mvi.ac_buscar_catalogo.text.toString().trim {it <= ' ' })){
                                    //filterList.add(prods!![i]) //aqui se va a agregar
                                    //prods = filterList
                                }
                                else{
                                    if(!filterList.contains(prods!![i]) || filterList.isEmpty()) {
                                        filterList.add(prods!![i])
                                        bandera = -1
                                        Log.e("nolo", "continee")
                                    }
                                }
                            }
                            else{
                                if(!filterList.contains(prods!![i])) {
                                    filterList.add(prods!![i])
                                    bandera = -1
                                    Log.e("nolo", "continee")
                                }
                            }
                        }
                    }
                }

                val elementArray: ArrayList<ParentCatalogue> = ArrayList()
                prods!!.forEach {
                    filterList.forEach{item->
                        if(it.catalogoText == item.catalogoText) elementArray.add(item)
                    }
                }

                if(bandera == -1){
                    elementArray.forEach {
                        prods!!.remove(it)
                    }
                }
                else{
                    elementArray.forEach {
                        filterList.remove(it)
                    }
                }
                adapterCatalogue!!.notifyParentDataSetChanged(true)
            }
        })
    }

    override fun setMessage(message: String){
        mvi.progress_bar.visibility = View.GONE
        context.toast(message)
    }

    private fun showExpiredSession(){
        context.alert{
            title = "Advertencia"
            message = "Tú sesión ha expirado. Inicia sesión nuevamente"
            positiveButton("Aceptar"){
                ShowLoginView()
            }
            isCancelable = false
        }.show()
    }

    private fun ShowLoginView(){
        core.closeSession()
    }

    override fun visibilitySearch(state: Boolean) {}
    override fun visibilityTextDate(state: Boolean) {}
    override fun visibilityRfid() {}
    override fun setStartDate(date: String) {}
    override fun setFinishDate(date: String) {}
    fun setKeyDown(keyCode : Int, event: KeyEvent){}
    fun setKeyUp(keyCode : Int, event: KeyEvent){}
    private fun showPotency() {}
    override fun initReader(){}
    override fun activateReader() {}
    override fun playSuccess() {}
}

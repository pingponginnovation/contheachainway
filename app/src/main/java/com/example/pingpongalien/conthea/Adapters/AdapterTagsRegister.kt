package com.example.pingpongalien.conthea.Adapters

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import com.example.pingpongalien.conthea.Fragments.InitialAdjustment.AltaTag
import com.example.pingpongalien.conthea.Fragments.InitialAdjustment.AltaTagView
import com.example.pingpongalien.conthea.Models.TagsForRegister
import com.example.pingpongalien.conthea.R
import kotlinx.android.synthetic.main.fragment_alta_tag.*

class AdapterTagsRegister(arayTagsRegist: ArrayList<TagsForRegister>, arrayOff: ArrayList<String>, inOrderOut: String,  presenter: AltaTag.Presenter, vw: Activity, altaAct: AltaTagView):
        RecyclerView.Adapter<AdapterTagsRegister.MyViewHolder>(){

    var presenter: AltaTag.Presenter? = null
    var arrayTags: ArrayList<TagsForRegister>
    var arrayOff: ArrayList<String>? = null
    private val inflater: LayoutInflater
    val tagsSelected = ArrayList<TagsForRegister>()
    var activiti: Activity? = null
    var actAltaTag: AltaTagView? = null
    var allTrue = false
    var inOrderOut: String? = null

    init {
        activiti = vw
        actAltaTag = altaAct
        inflater = LayoutInflater.from(activiti)
        arrayTags = arayTagsRegist
        this.arrayOff = arrayOff
        this.presenter = presenter
        this.inOrderOut = inOrderOut
    }

    inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        var checkBox: CheckBox
        val infoArticulo: TextView
        val infoLote: TextView
        val infoAlmacn: TextView
        val textoFragment: TextView
        var btnRegistar: Button
        val btnAll: Button

        init {
            checkBox = itemView.findViewById(R.id.idCheckBox)
            infoArticulo = itemView.findViewById(R.id.idTxtArt)
            infoLote = itemView.findViewById(R.id.idTxtLote)
            infoAlmacn = itemView.findViewById(R.id.idTxtStorage)
            textoFragment = activiti!!.findViewById(R.id.txto_fragment)
            btnRegistar = activiti!!.findViewById(R.id.btnRegister)
            btnAll = activiti!!.findViewById(R.id.idBtnSelect)
        }
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int){

        //Toast.makeText(cntext, "BIND", Toast.LENGTH_LONG).show()
        //Log.e("Array", arrayTags.size.toString())

        holder!!.infoArticulo.setText(arrayTags[position].article)
        holder.infoLote.setText(arrayTags[position].lote)
        holder.infoAlmacn.setText(arrayTags[position].almacen)
        holder.checkBox.tag = position
        holder.checkBox.isChecked = arrayTags[position].selected

        if(inOrderOut.equals("Si", true)){

            holder.textoFragment.setText("Productos en rojo pertenecen a una orden de salida")
            holder.checkBox.isEnabled = false
            holder.checkBox.isChecked = false

            if(arrayOff!!.contains(arrayTags[position].uuid)){

                holder.infoArticulo.setTextColor(Color.parseColor("#ba000d"))
                holder.infoLote.setTextColor(Color.parseColor("#ba000d"))
                holder.infoAlmacn.setTextColor(Color.parseColor("#ba000d"))
            }
            else{
                holder.infoArticulo.setTextColor(Color.parseColor("#32cb00"))
                holder.infoLote.setTextColor(Color.parseColor("#32cb00"))
                holder.infoAlmacn.setTextColor(Color.parseColor("#32cb00"))
            }
        }

        holder.btnRegistar.setOnClickListener{

            if(tagsSelected.isEmpty()) Toast.makeText(activiti, "Selecciona los tags que deseas agregar", Toast.LENGTH_LONG).show()
            else{

                val builder = AlertDialog.Builder(activiti!!)
                builder.setTitle("Confirmación")
                builder.setMessage("¿Desea continuar con el registro de tags?")
                builder.setPositiveButton("Si"){dialog, which ->
                    actAltaTag!!.LY_Recycler2.visibility = View.GONE
                    actAltaTag!!.LY_progressBar_altatag.visibility = View.VISIBLE
                    presenter!!.SendTagsRegister(tagsSelected)
                }
                builder.setNeutralButton("Cancelar"){dialog, which->
                    dialog.dismiss()
                }

                val dialog: AlertDialog = builder.create()
                dialog.show()
            }
        }

        holder.btnAll.setOnClickListener{

            if(!allTrue){

                arrayTags.forEachIndexed{index, tag->
                    tag.selected = true
                    if(!tagsSelected.contains(tag)) tagsSelected.add(arrayTags[index])
                }

                allTrue = true
            }
            else{

                arrayTags.forEachIndexed{index, tag->
                    tag.selected = false
                }

                tagsSelected.clear()
                allTrue = false
            }

            notifyDataSetChanged()
        }

        holder.checkBox.setOnClickListener{

            //val pos = holder.checkBox.tag as Int
            //Toast.makeText(cntext, arrayTags[position].lote, Toast.LENGTH_LONG).show()

            if(holder.checkBox.isChecked) tagsSelected.add(arrayTags[position])
            else tagsSelected.remove(arrayTags[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder{

        //Log.e("CREATEEE", arrayTags.size.toString())

        val view = inflater.inflate(R.layout.cardview_fragment_register, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int{
        return arrayTags.size
    }
}
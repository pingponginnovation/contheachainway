package com.example.pingpongalien.conthea.ModelsRoom

import android.arch.persistence.room.*
import com.example.pingpongalien.conthea.Helpers.HelperDate
import org.jetbrains.annotations.NotNull
import java.util.Date

@Entity(tableName = "inventario_ciclico")
data class RoomInventario(
        @PrimaryKey(autoGenerate = true)
        var id: Int = 0,
        @ColumnInfo(name = "lote_number") var lote: String?,
        @ColumnInfo(name = "almacen_number") var almacen: String?,
        @ColumnInfo(name = "catalogo_number") var catalogo: String?,
        @ColumnInfo(name = "uuid_number") var uuidNumber: String?
)